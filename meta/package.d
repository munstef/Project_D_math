module meta;

public {
	import meta.list;
	import meta.match;
	import meta.predicate;
	import meta.traits;
	import meta.transform;
	import meta.comparable;
}
private {
	import std.typecons : tuple;
}

public template Select(bool condition, T...) if (T.length == 2)
{
    import std.meta : Alias;
    alias Select = Alias!(T[!condition]);
}

public template Select(uint n, T ...) if (T.length > n) {
	import std.meta : Alias;
	alias Select = Alias!(T[n]);
}

public template adjoin(F...) {
	static if (F.length == 0) alias adjoint = identity;
	else static if(F.length == 1) alias adjoin = F[0];
	else {
		auto adjoin(V...)(auto ref a) {
			return tuple(F[0](a), adjoin!(F[1 .. $])(a));
		}
	}
}

public template compose(F...) {
	static if (F.length) alias compose = std.fonctional.compose;
	else alias compose = identity;
}

public T forceCast(T, U)(U u) if (T.sizeof == U.sizeof) {
	union {
		T t;
		U u;
	} tmp;
	tmp.u = u;
	return tmp.t;
}




