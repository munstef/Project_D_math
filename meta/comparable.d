module meta.comparable;
import meta.traits : is_comparable, is_equalable;

bool is_LT(F)(const F x, const F y) if(is_comparable!F) { return x < y; }

bool is_LE(F)(const F x, const F y) if(is_comparable!F) { return x <= y; }

bool is_GT(F)(const F x, const F y) if(is_comparable!F) { return x > y; }

bool is_GE(F)(const F x, const F y) if(is_comparable!F) { return x >= y; }

bool is_EQ(F)(const F x, const F y) if(is_equalable!F) { return x == y; }

bool is_NE(F)(const F x, const F y) if(is_equalable!F) { return x != y; }

bool is_between(T, string[2] ext = "[]")(T x, T a, T b) if (is_comparable!T) {
	bool result = (ext[0] == '[') ? (x >= a) : (x > a);
	result &= (ext[1] == ']') ? (x <= b) : (x < b);
	return result;
}

T clampHight(T)(T x, T a) if (is_comparable!T) {
	return min(x, a);
}
	
T clampLow(T)(T x, T a) if (is_comparable!T) {
	return max(x, a);
}

T clamp(T)(T x, T a, T b) if (is_comparable!T) {
	return min(max(x, a), b);
}

T max(T)(T[] a...) if(is_comparable!T) {
	if(a.length) {T result = a[0]; for(size_t i = 1; i < a.length; ++i) result = (a[i] > result) ? a[i] : result; return result;}
	else throw new Exception("Argument Error : Expected argument");
}

T min(T)(T[] a...) if(is_comparable!T) {
	if(a.length) {T result = a[0]; for(size_t i = 1; i < a.length; ++i) result = (a[i] < result) ? a[i] : result; return result;}
	else throw new Exception("Argument Error : Expected argument");
}

Tuple!(T, "min", T, "max") minmax(T)(T[] a...) if(is_comparable!T) {
	if(a.length) { T min, max = a[0]; for(size_t i = 1; i < a.length; ++i) {if (a[i] < min)  min = a[i]; else if (a[i] > max) max = a[i];} return tuple(min, max);}
	else throw new Exception("Argument Error : Expected argument");
}
