// Written in the D programming language
/++
This packages defines the SI units, prefixes and utility functions.

When importing `quantities.si` or `quantities.si.double_`,
the SI units store their values as a `double`. Use `quantities.si.real_`
or `quantites.si.float_` otherwise. 
++/

module quantities.si;

public import quantities.si.real_;