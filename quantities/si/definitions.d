/++
This module only contains a template mixin used to generate
SI units, prefixes and utility functions.
+/
module quantities.si.definitions;
public {
	import std.typecons : Tuple, tuple;
	import std.ascii : toUpper;
}

/// 
mixin template SI(Numeric)
{
    /++
    Predefined SI units.
    +/
    enum meter = unit!(Numeric, "L");			// 1 m   ≝ c0 / 299792458
    enum kilogram = unit!(Numeric, "M");		// 1 kg  ≝ ℎ / 6.626070040e-34
    enum second = unit!(Numeric, "T");			// 1 s   ≝ 9192631770 / ΔVCs
    enum ampere = unit!(Numeric, "I");			// 1 A   ≝ ℯ / 1.6021766208e-27
    enum kelvin = unit!(Numeric, "Θ");			// 1 K   ≝ 1.38064852e-23  / kB (kg m² s⁻²)
    enum mole = unit!(Numeric, "N");			// 1 mol ≝ 6.022140857e23 / NA
    enum candela = unit!(Numeric, "J");			// 1 cd  ≝ Kcd / (683 kg m² s⁻³ sr⁻¹)
	enum bit = unit!(Numeric, "D");				// data

	enum one = Quantity!(Numeric, Dimensions.init)(1); /// The dimensionless unit 'one'

	alias Dimensionless = typeof(meter/meter); /// The type of dimensionless quantities
/*    enum UnitTemperature { Kelvin, Celsius, Fahrenheit, Rankin, Delisle, Newton, Réaumur, Rømer, Leyden };
	Numeric[UnitTemperature][2] Θ = [
		Kelvin : [Numeric(1), Numeric(0)],
		Celsius : [Numeric(1), Numeric(-273.15)],
		Fahrenheit : [Numeric(1.8), Numeric(-459.67)], 
		Rankin : [Numeric(1.8), Numeric(0)],
		Delisle : [Numeric(-2.0/3), Numeric(559.725)],
		Newton : [Numeric(100.0/33), Numeric(-90.14)],
		Réaumur : [Numeric(1.25), Numeric(-218.52)],
		Rømer : [Numeric(40.0/33), Numeric(-135.90)],
		Leyden : [Numeric(1), Numeric(-20.15)]
	];

	Numeric from(UnitTemperature r)(Numeric t) {
		return (t * Θ[r][0] + Θ[r][1]);
	}
	Numeric to(UnitTemperature r)(Numeric k) {
		return (k - Θ[r][1]) / Θ[r][0];
	}
	Numeric from(UnitTemperature f, UnitTemperature t)(Numeric n) if(f != t){
		return to!t(from!f(n));
	}
	Numeric from(UnitTemperature f, UnitTemperature t)(Numeric n) if(f == t){
		return n;
	}

	static ThermodynamicTemperature delta(UnitTemperature r)() @property {return Θ[r][0] * kelvin;}
*/
	auto DHMS(in Numeric d, in Numeric m = Numeric(0), in Numeric s = Numeric(0), in Numeric ms = Numeric(0)) {
		return d * day + m * minute + (s + ms * 1e-3) * second;
	}
	auto DMS(in Numeric degre, in Numeric m = Numeric(0), in Numeric s = Numeric(0)) {
		return degre * arcdegree + m * arcminute + s * arcsecond;
	}

    enum radian = meter / meter; // ditto
    enum steradian = square(meter) / square(meter); /// ditto
    enum hertz = 1 / second; /// ditto
    enum newton = kilogram * meter / square(second); /// ditto
    enum pascal = newton / square(meter); /// ditto
    enum joule = newton * meter; /// ditto
    enum watt = joule / second; /// ditto
    enum coulomb = second * ampere; /// ditto
    enum volt = watt / ampere; /// ditto
    enum farad = coulomb / volt; /// ditto
    enum ohm = volt / ampere; /// ditto
    enum siemens = ampere / volt; /// ditto
    enum weber = volt * second; /// ditto
    enum tesla = weber / square(meter); /// ditto
    enum henry = weber / ampere; /// ditto
	enum celsius = kelvin;
	enum fahrenheit = 1.8 * kelvin;
	enum rankin = 1.8 * kelvin;
	enum delisle = -2.0 / 3 * kelvin;
	enum newton_t = 100.0 / 33 * kelvin;
	enum réaumur = 1.25 * kelvin;
	enum rømer = 40.0 / 33 * kelvin;
	enum leyden = kelvin;

/*	enum fahrenheit = Δ!Fahrenheit;  
	enum rankine = Δ!Rankine;
	enum delisle = Δ!Delisle;
	enum deg_newton = Δ!Newton;
	enum réaumur = Δ!Réaumur;
	enum rømer = Δ!Rømer;
	enum leyden = Δ!Leyden; */
    enum lumen = candela / steradian; /// ditto
    enum lux = lumen / square(meter); /// ditto
    enum becquerel = 1 / second; /// ditto
    enum gray = joule / kilogram; /// ditto
    enum sievert = joule / kilogram; /// ditto
    enum katal = mole / second; /// ditto

	enum poiseuille = pascal * second; /// dito

// time
    enum minute = 60 * second; /// ditto
    enum hour = 60 * minute; /// ditto
    enum day = 24 * hour; /// ditto	
	enum week = 7 * day;
	enum julianYear = 365.25*day;
	enum gregorianYear = 365.2425*day;

// Angle plan
	alias PlaneAngle = typeof(radian); /// ditto
    enum arcdegree = PI / 180 * radian; /// ditto
    enum arcminute = arcdegree / 60; /// ditto
    enum arcsecond = arcminute / 60; /// ditto
	enum turn = 2 * PI * radian;
	alias tour = turn;
	enum gradian = PI / 200 * radian;

//
	alias SolidAngle = typeof(steradian); /// ditto

// Superficie
	enum hectare = 1.0e4 * square(meter);

// Volume
	enum liter = 1.0e-3 * cubic(meter);

// masse
	enum ton = 1.0e3 * kilogram;

// Length
	enum astronomicUnit = 149_597_870_700.0 * meter;
	enum parsec = 648000 / PI * astronomicUnit;
	enum lightYear = c0 * julianYear; 	

// Hors SI usage evec unités SI
	enum electronVolt = 1.602_176_565e-19 * joule;
	enum dalton = 1.660_538_921e-27 * kilogram;
	enum uma = dalton;

/// Unités Naturels
	enum c0 = 299_792_458.0 * meter / second;
	enum ℏ = 1.054_571_726e-34 * joule * second;
	enum me = 9.109_3826e-31 * kilogram; 
	enum ℏ_mec02 = 1.288_088_6677e-21 * second; 

/// Unités atomiques
	enum ℯ = 1.602_176_53e-19 * coulomb;							// element charge
//	enum me = 9.109_3826e−31 kilogram; 
//	enum ℏ = 1.054_571_726e-34 * joule * second;	
	enum a0 = 0.529_177_210_92e-10 * meter;
	enum Eh = 4.359_744_34e-18 * joule;
	enum ℏ_Eh = 2.418_884_326_502e-17 * second;

/// universal constants
	enum ΔVCs = 9192631770.0 * hertz ;                              // hyperfine transition frequency of Cs
	enum ℎ = 6.626070040e-34 * joule * second;						// Planck constant								
	enum kB = 1.38064852e-23 * joule / kelvin;						// Boltzmann constant
	enum NA = 6.022140857 / mole;									// Avogadro's number
	enum Kcd = 683 * lumen / watt;									// luminous efficacity
/// Autres unités en dehors du SI
// Unités de pression
	enum bar = 1.0e5 * pascal;
	enum mmHg = 133.322 * pascal;
// Unités de longueur
	enum ångström = 1.0e-10 * meter;
	enum barn = 1.0e-28 * meter;
	enum micron = 1.0e-9 * meter;
// Unité calorimétrique
	enum calorie = 4.18 * joule;
	enum Calorie = 4.18e3 * joule;
	enum kcal = Calorie;
	enum thermie = 1e6 * calorie;
	enum frigorie = -1.0 * calorie;
// Unités nautiques
	enum knot = 1852.0/3600.0 * meter / second;
	enum mile = 1852.0 * meter;

// Unités en dehors du SI associées au systèmes d'unités CGS et CGS de Gauss
	enum gram = 1e-3 * kilogram; /// ditto
	enum erg = 1.0e-7 * joule;
	enum dyne = 1.0e-5 * newton;
	enum poise = 0.1 * poiseuille;
	enum stokes = 1.0e-4 * square(meter) / second;
	enum stilb = 1.0e-4 * candela / square(meter);
	enum phot = 1e4 * lux;
	enum gal = 1e-2 * meter / square(second);
	enum maxwell = 1.0e-8 * weber;
	enum gauss = 1.0e4 * tesla;
	enum œrsted = (1.0e3/(4 * PI)) * ampere / meter;

// Unité aéronautique
	enum mach = 340 * meter / second;

/// Unités informatique
	enum Byte =  8 * bit;

	// constantes mathematiques
	enum π = PI;

	enum µ0 = 4e-7 * π * tesla * meter / ampere;					// vacuum permeability	
	enum ε0 = 1 / µ0 / square(c0);									// vacuum permittivity
	enum Z0 = µ0 * c0;												// characteristic impedance of vacuum

	// electromanetisme
	
	enum kC = 1 /(4 * π * ε0);										// Coulomb's constant
	// gravitation
	enum G = 6.67408e-11 * cubic(meter)/kilogram/(square(second));	// Newtonian constant of gravitation
	enum gn = 9.80665 * meter / square(second);						// standard acceleration of gravity (gee, free-fall on Earth)	
	alias g0 = gn;													// "
	// phisico-chimiques	
	enum T0 = 273.16 * kelvin;										// Température du point triple de l'eau

	enum atm = 101325 * pascal;										// standars atmosphere

	enum R = 8.3144598 * joule / mole / kelvin;						// gas constant

	enum F = NA * ℯ;												// Constante de Faraday
	enum V0 = 22.413962e-3 * cubic(meter) / mole;					// Volume molaire d'un gaz parfait (p == 100. kPa, T = T0)
	enum atomicMassUnit = 1.660539040e-27 * kilogram;				// Atomic mass constant
	enum c1 = 2 * π * ℎ * c0 * c0;									// First radiation constant
	enum c1L = 2 * ℎ * c0 * c0 / steradian;							//	'' for spectral radiance
	enum c2 = ℎ * c0 / kC;											// second radiation constant
	enum σ = 5.670367e-8 * watt / square(meter) * pow!(-4)(kelvin); // Constante de Stefan-Boltzmann
	enum NL = NA / V0;												// Constante de Loschmidt
	// constantes atomiques et nucléaires
	enum Mu = 1e-3 * kilogram / mole;								// Atomic mass constant
	enum M12C = 1.2e-2 * kilogram / mole;
	enum α = kC * square(ℯ) / (ℏ * c0);								// fine-structure constant
	enum Rinf = 10973731.568508 / meter;							// Rydberg constant
	enum G0 = 7.7480917310e_5 * siemens;							// Quantum de conductance
	enum Φ0 = 2.067833831e-15 * weber;								// Quantum de flux magnétique
	enum λC = 2.4263e-12 * meter;									// Longueur d'onde de Compton pour l'électron
	enum RC = 3.861159e-13 * meter;									// Rayon de Compton pour l'électron
	enum rℯ = 2.817940325e-15 * meter;								// Rayon classique de l'électron
	enum µB = 9.274009994e-24 * ampere / meter;						// Magnéton de Bohr
	enum µN = 5.050783699e-27 * ampere * square(meter);				// Magnéton nucléaire
	enum mℯ = 9.10938356e-31 * kilogram;							// Masse de l'électron
	enum mp = 1.672621898e-27 * kilogram;							// proton mass
	enum mn = 1.6741927471e-27 * kilogram;							// Masse du neutron
	enum mµ = 1.883531594e-28 * kilogram;							// Masse du muon
	enum mτ = 3.16747e-27 * kilogram;								// Masse du tauon
	enum mZ0 = 1.6556e-25 * kilogram;								// Masse du boson Z°
	enum mW = 1.4334e-25 * kilogram;								// Masse du boson W
	enum RK_90 = 4.835979e14 * hertz / volt;						// constantes de von Klitzing
	enum KJ_90 = 25812.80 * ohm;									// constantes de Josephson
/// Unités SI de base
    alias Length = typeof(meter); /// Predefined quantity type templates for SI quantities
    alias Mass = typeof(kilogram); /// ditto
    alias Time = typeof(second); /// ditto
    alias ElectricCurrent = typeof(ampere); /// ditto
    alias ThermodynamicTemperature = typeof(kelvin); /// ditto
    alias AmountOfSubstance = typeof(mole); /// ditto
    alias LuminousIntensity = typeof(candela); /// ditto
	alias Data = typeof(bit);
/// Unités SI dérivées à partir des unités de base
	alias Area = typeof(square(meter)); /// ditto
    alias Volume = typeof(cubic(meter)); /// ditto
    alias Speed = typeof(meter/second); /// ditto
	alias Velocity = Speed;
    alias Acceleration = typeof(meter/square(second)); /// ditto
	alias WaveNumber = typeof(1/ meter);
    alias MassDensity = typeof(kilogram/cubic(meter)); /// ditto
	alias Density = MassDensity;
	alias SurfaceDensity = typeof(kilogram/square(meter));
	alias SpecificVolume = typeof(cubic(meter)/kilogram);
	alias CurrentDensity = typeof(ampere/square(meter));
	alias MagneticFieldStrength = typeof(ampere/meter);
	alias MassConcentration = typeof(kilogram/cubic(meter));
/// Unités ayant des noms spéciaux et des symboles particuliers
	alias Frequency = typeof(hertz); /// ditto
    alias Force = typeof(newton); /// ditto
    alias Pressure = typeof(pascal); /// ditto
    alias Energy = typeof(joule); /// ditto
    alias Work = Energy; /// ditto
    alias AmountHeat = Energy; /// ditto
    alias Power = typeof(watt); /// ditto
	alias radiantFlux = Power;
    alias ElectricCharge = typeof(coulomb); /// ditto
    alias ElectricPotentialDifference = typeof(volt); /// ditto
    alias Capacitance = typeof(farad); /// ditto
    alias Resistance = typeof(ohm); /// ditto
    alias Conductance = typeof(siemens); /// ditto
    alias MagneticFlux = typeof(weber); /// ditto
    alias MagneticFluxDensity = typeof(tesla); /// ditto
    alias Inductance = typeof(henry); /// ditto	
    alias LuminousFlux = typeof(lumen); /// ditto
    alias Illuminance = typeof(lux); /// ditto
    alias ActivityRefferedToNuclide = typeof(becquerel);
    alias AbsorbedDose = typeof(gray); /// ditto
	alias kerma = AbsorbedDose;
	alias DoseEquivalent = typeof(sievert);
	alias CataliticActivity = typeof(katal);

/// Unités SI dérivées cohérentes dont le nom et le symbole comprennent des unités SI dérivées 
/// cohérentes ayant des noms spéciaux et des symboles particuliers
	alias DynamicViscosity = typeof(pascal * second);
	alias MomentOfForce = typeof(newton * meter);
	alias surfaceTension = typeof(newton * meter);
    alias AngularVelocity = typeof(radian/second);
	alias AngularFrequency = AngularVelocity;
	alias AngularAcceleration = typeof(radian/square(second));
	alias HeatFluxDensity = typeof(watt/square(meter));
	alias Irradiance = HeatFluxDensity;
	alias HeatCapacity = typeof(joule/kelvin);
	alias SpecificHeatCapacity = typeof(joule/(kilogram * kelvin));
	alias SpecificEntropy = SpecificHeatCapacity;
	alias SpecificeEnergy = typeof(joule/kilogram);
	alias ThermalConductivity = typeof(watt / (meter * kelvin));
	alias EnergyDensity = typeof(joule / cubic(meter));
	alias ElectricFieldStrength = typeof(volt/meter);
	alias ElectricChargeDensity = typeof(coulomb /cubic(meter));
	alias SurfaceChargeDensity = typeof(coulomb / square(meter));
	alias ElectricFluxDensity = typeof(coulomb / square(meter));
	alias ElectricDisplacement = ElectricFluxDensity;
	alias Permittivity = typeof(farad/meter);
	alias Permeability = typeof(henry/meter);
	alias MolarEnergy = typeof(joule/mole);
	alias MolarEntropy = typeof(joule/(mole * kelvin));
	alias MolarHeatCapacity = MolarEntropy;
	alias Exposure = typeof(coulomb/kilogram);
	alias AbsorbedDoseRate = typeof(gray /second);
	alias RadiantIntensity = typeof(watt /steradian);
	alias Radiance = typeof(watt /(steradian * square(meter)));
	alias CatalyticActivityConcontration = typeof(katal/cubic(meter));

    /// SI prefixes.
    alias yotta = prefix!1e24;
    alias zetta = prefix!1e21; /// ditto
    alias exa = prefix!1e18; /// ditto
    alias peta = prefix!1e15; /// ditto
    alias tera = prefix!1e12; /// ditto
    alias giga = prefix!1e9; /// ditto
    alias mega = prefix!1e6; /// ditto
    alias kilo = prefix!1e3; /// ditto
    alias hecto = prefix!1e2; /// ditto
    alias deca = prefix!1e1; /// ditto
    alias deci = prefix!1e-1; /// ditto
    alias centi = prefix!1e-2; /// ditto
    alias milli = prefix!1e-3; /// ditto
    alias micro = prefix!1e-6; /// ditto
    alias nano = prefix!1e-9; /// ditto
    alias pico = prefix!1e-12; /// ditto
    alias femto = prefix!1e-15; /// ditto
    alias atto = prefix!1e-18; /// ditto
    alias zepto = prefix!1e-21; /// ditto
    alias yocto = prefix!1e-24; /// ditto

    /// A list of common SI symbols and prefixes
    enum siSymbols = SymbolList!Numeric()
        .addUnit("m", meter)
        .addUnit("kg", kilogram)
        .addUnit("s", second)
        .addUnit("A", ampere)
        .addUnit("K", kelvin)
        .addUnit("mol", mole)
        .addUnit("cd", candela)
        .addUnit("rad", radian)
        .addUnit("sr", steradian)
        .addUnit("Hz", hertz)
        .addUnit("N", newton)
        .addUnit("Pa", pascal)
		.addUnit("Pl", poiseuille)
        .addUnit("J", joule)
        .addUnit("W", watt)
        .addUnit("C", coulomb)
        .addUnit("V", volt)
        .addUnit("F", farad)
        .addUnit("Ω", ohm)
        .addUnit("S", siemens)
        .addUnit("Wb", weber)
        .addUnit("T", tesla)
        .addUnit("H", henry)
        .addUnit("lm", lumen)
        .addUnit("lx", lux)
        .addUnit("Bq", becquerel)
        .addUnit("Gy", gray)
        .addUnit("Sv", sievert)
        .addUnit("kat", katal)
		.addUnit("P", poise)
        .addUnit("g", gram)
		.addUnit("°C", celsius)
		.addUnit("°F", fahrenheit)
		.addUnit("°Ra", rankin)
		.addUnit("°Ré", réaumur)
		.addUnit("°Rø", rømer)
		.addUnit("°L", leyden)
		.addUnit("°D", delisle) 
		.addUnit("j", day)
		.addUnit("d", day)
        .addUnit("min", minute)
        .addUnit("h", hour)
		.addUnit("w", week)
		.addUnit("y", gregorianYear)
		.addUnit("°", arcdegree)
		.addUnit("'", arcminute)
		.addUnit("\"", arcsecond)
		.addUnit("tr", turn)
		.addUnit("grad", gradian)
		.addUnit("gon", gradian)
		.addUnit("ha", hectare)
        .addUnit("l", liter)
        .addUnit("L", liter)
        .addUnit("t", ton)

		.addUnit("ua", astronomicUnit)
		.addUnit("pc", parsec)
		.addUnit("ly", lightYear)

        .addUnit("eV", electronVolt)
        .addUnit("Da", dalton)
		.addUnit("u", dalton)

		.addUnit("c₀", c0)
		.addUnit("ℏ", ℎ)
		.addUnit("mₑ", me)


		.addUnit("ℯ", ℯ)
		.addUnit("NA", NA)
		.addUnit("Kcd", Kcd)

		.addUnit("bar", bar)
		.addUnit("mmHg", mmHg)
		.addUnit("Å", ångström)
		.addUnit("M", mile)
		.addUnit("kn", knot)
		.addUnit("atm", atm)

		.addUnit("erg", erg)
		.addUnit("dyn", dyne)
		.addUnit("Pl", poiseuille)
		.addUnit("P", poise)
		.addUnit("St", stokes)
		.addUnit("sb", stilb)
		.addUnit("ph", phot)
		.addUnit("Gal", gal)
		.addUnit("Mx", maxwell)
		.addUnit("G", gauss)
		.addUnit("Oe", œrsted)

		.addUnit("cal", calorie)
		.addUnit("Cal", Calorie)
		.addUnit("th", thermie)
		.addUnit("fg", frigorie)

		.addUnit("g", gram)

		.addUnit("mach", mach)

		.addUnit("b", bit)
		.addUnit("o", Byte)
		.addUnit("B", Byte)

        .addPrefix("Y", 1e24)
        .addPrefix("Z", 1e21)
        .addPrefix("E", 1e18)
        .addPrefix("P", 1e15)
        .addPrefix("T", 1e12)
        .addPrefix("G", 1e9)
        .addPrefix("M", 1e6)
        .addPrefix("k", 1e3)
        .addPrefix("h", 1e2)
        .addPrefix("da", 1e1)
        .addPrefix("d", 1e-1)
        .addPrefix("c", 1e-2)
        .addPrefix("m", 1e-3)
        .addPrefix("µ", 1e-6)
        .addPrefix("n", 1e-9)
        .addPrefix("p", 1e-12)
        .addPrefix("f", 1e-15)
        .addPrefix("a", 1e-18)
        .addPrefix("z", 1e-21)
        .addPrefix("y", 1e-24)
        .addPrefix("Yi", 1024.0^^8)
        .addPrefix("Zi", 1024.0^^7)
        .addPrefix("Ei", 1024.0^^6)
        .addPrefix("Pi", 1024.0^^5)
        .addPrefix("Ti", 1024.0^^4)
        .addPrefix("Gi", 1024.0^^3)
        .addPrefix("Mi", 1024.0^^2)
        .addPrefix("Ki", 1024.0);

    private static SymbolList!Numeric _siSymbols;
    private static Parser!Numeric _siParser;
    static this()
    {
        _siSymbols = siSymbols;
        _siParser = Parser!Numeric(_siSymbols, &std.conv.parse!(Numeric, const(char)[]));
    } 

    /// Parses a string for a quantity of type Q at runtime
    Q parseSI(Q)(const(char)[] str)
        if (isQuantity!Q)
    {
        return _siParser.parse!Q(str);
    }
    ///
    unittest
    {
        auto t = parseSI!Time("90 min");
        assert(t == 90 * minute);
        t = parseSI!Time("h");
        assert(t == 1 * hour);

        auto v = parseSI!Dimensionless("2");
        assert(v == (2 * meter) / meter);
    }

    /// A compile-time parser with automatic type deduction for SI quantities.
    alias si = compileTimeParser!(Numeric, siSymbols, std.conv.parse!(Numeric, const(char)[]));
    ///
    unittest
    {
        enum min = si!"min";
        enum inch = si!"2.54 cm";
        auto conc = si!"1 µmol/L";
        auto speed = si!"m s^-1";
        auto value = si!"0.5";
        
        static assert(is(typeof(inch) == Length));
        static assert(is(typeof(conc) == Concentration));
        static assert(is(typeof(speed) == Speed));
        static assert(is(typeof(value) == Dimensionless));
    }
}