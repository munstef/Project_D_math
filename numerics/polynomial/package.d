module numerics.polynomial;

import std.range;
import std.bigint;
import std.format;
import meta : Signed;
import numerics;
//public import numerics.polynomial.special;

struct Polynomial(T, uint var : 0 = 0) {

	T[] coeffs;

	protected void reduce() {
		while((length>0) && !coeffs[length - 1]) --coeffs.length;
	}

	this(this) {coeffs = coeffs.dup;}

	this(T x, size_t n) {coeffs = new T[n + 1]; coeffs[n]=x;}

	this(T[] c...) {coeffs = c.dup; reduce();}

	this(const Polynomial P) { coeffs = P.coeffs.dup;}

	Signed!size_t deg() const @property {return coeffs.length - 1;}

	Signed!size_t ord() const @property {foreach(i, e; coeffs) if(e) return i; return -1;}

	T opIndex(size_t index) const {if (index >= coeffs.length) return T(0); else return coeffs[index];}

	T opIndexAssign(T value, size_t index) {
		if (!value) {
			if (length) {
				if (index == length - 1) {-- coeffs.length; reduce();}
				if (index < length - 1) coeffs[index] = value;
			}
			return value;
		} else {
			while(index >= length) coeffs ~= T(0); 
			return coeffs[index] = value;
		}
	}

	ref Polynomial opAssign(const T x) {coeffs = [x]; return this;}

	ref Polynomial opAssign(const T[] array) {coeffs = array.dup; reduce(); return this;}

	ref Polynomial opAssign(const Polynomial P) { coeffs = P.coeffs.dup; return this;}

	Polynomial opUnary(string op)() const if(op == "+" || op =="-") {
		if (op == "+") return this;
		if (op == "-") return Polynomial(-coeffs[]);
	}

	ref Polynomial opUnary(string op)() if(op == "++" || op == "--") {
		this[0] += (op == "++") ? T(1) : T(-1); 
		return this;
	}

	ref Polynomial opOpAssign(string op)(const Polynomial Q) if(op == "+" || op == "-") {
		if(op == "+") foreach(i, q; Q.coeffs) this[i] += q;
		else foreach(i, q; Q.coeffs) this[i] -= q;
		return this;
	}

	ref Polynomial opOpAssign(string op)(const Polynomial Q) if(op == "*") {
		return this = this * Q;
	}

	ref Polynomial opOpAssign(string op)(const Polynomial Q) if(op=="/" || op == "%") {
		return this = divrem(this, Q)[(op == "/") ? 0 : 1];
	}

	ref Polynomial opOpAssign(string op)(const T a) if(op == "+" || op == "-") {
		if (op == "+") this[0] += a; else this[0] -= a;
		return this;
	}

	ref Polynomial opOpAssign(string op)(const T a) if(op == "*" || op=="/" || op == "%") {
		if(op == "%") {coeffs = []; return this;}
		if (op=="*") foreach(q; coeffs) q *= a;
		if (op == "/") foreach(q; coeffs) q /= a;
		reduce();
		return this;
	}
	ref Polynomial  opOpAssign(string op)(const T a) if(op == "+" || op == "-") {
		if (op == "+") return this[0] += a; else this[0] -= a;
		return this;
	}

	Polynomial opOpAssign(U, string op)(U n) if(op == "^^" && isIntegral(U) && !isSigned(U)) {
		return this = pow(this, n);
	}

	Polynomial opBinary(string op)(const T a) const if(op == "+" || op == "-") {
		Polynomial P = Polynomial(this);
		if(op == "+") P[0]+=a; P[0]-=a;
		return P;
	}

	Polynomial opBinary(string op)(const T a) const if(op == "*" || op == "/") {
		Polynomial P = Polynomial(this);
		return (op == "*") ? P *= a : P /= a;
	}

	Polynomial opBinaryRight(string op)(const T a) const if(op == "+" || op == "-") {
		Polynomial P = Polynomial((op == "+") ? this : -this);
		P[0]=a + P[0];
		return P;
	}

	Polynomial opBinaryRight(string op)(const T a) const if(op == "*") {
		Polynomial P = Polynomial(this);
		foreach(q; coeffs) q = a * q;
		return P;
	}

	Polynomial opBinany(string op)(const Polynomial P) const if(op == "+" || op == "-") {
		if(length == 0) return -P;
		if(P.length == 0) return this;
		uint M = max(deg, P.deg);
		Pölynomial R = Polynomial(T(0), M);
		if(op == "+") for(uint i = 0; i < M; ++i) R[i] = this[i] + P[i];
		else for(uint i = 0; i < M; ++i) R[i] = this[i] - P[i];
		R.reduce();
		return R;
	}

	Polynomial opBinary(string op)(const Polynomial P) const if(op == "*") {
		if(!length) return Polynomial();
		if(!P.length) return Polynomial();
		Polynomial R = Polynomial(T(0), this.deg + P.deg);
		foreach(i, e; coeffs) foreach(j, p; P.coeffs) R.coeffs[i+j] += e * p;
		return R;
	}

	Polynomial opBinary(string op, U)(U n) const if((op == "^^") && isIntegral!U && !isSigned!U) 
	{
		return pow(this, n);
	}

	U opCall(U)(U u) {
		U m = U(1);
		U r;
		foreach(a; coeffs) { r += a * m; m *= u;}
		return r;
	}

	Polynomial opBinary(string op) (const Polynomial Q) const if(op == "/" || op == "%") {
		return divrem(this, Q)[(op == "/") ? 0 : 1];
	}

	Polynomial div(Polynomial P, size_t order) {
		Polynomial R = this;
		Polynomial Q;
		size_t m = P.ord;
		T a = coeffs[m];
		while(R && R.ord >= m && Q.deg <= order) {
			Polynomial Q1; Q1[R.ord - m] = R.coeffs[ord] / a;
			Q += Q1;
			Polynomial R1 = Q1 * P;
			R.coeffs[R.ord] = 0;
			R1.coeffs[R1.ord] = 0;
			R -= R1;
		}
		return Q;
	}

	Polynomial X(size_t n) { Polynomial P = Polynomial(zero!T, n); P[n] = one!T; return P;}

	string dllToString() {
		bool first = true;
		string r = "";
		if (!this) return "0";
		foreach(size_t i, T e; coeffs[ord .. $]) {
			if(first) r ~= std.format.format("%n", e); else {
				if (e > 0) r ~= " + "; r ~= " - ";
				r ~= std.format.format("%n", abs!T(e));
			}
			if (i >= 1) r ~= "x";
			if (i > 1) r~=std.format.format("^%n", i);
			first = false;
		}
		return r;
	}

	string toString() {
		return format("%d", this);
	}

	string dllToString() {
		return formatDll("%d", this);
	}

	bool opCast(Q : bool)() const { return coeffs.length > 0;}

	bool opEquals(const Polynomial P) const {
		if(deg != P.deg) return false;
		foreach(i, e; coeffs) if (P.coeffs[i] != e) return false;
		return true;
	}

	bool opEquals(const T t) const {
		return (isZero && t == T(0)) || (coeffs.length == 1) && (coeffs[0] == t);
	}

	bool isZero() const @property {
		return coeffs.length == 0;
	}

	size_t opDollar() const {return coeffs.length;}
	size_t length() const @property {return coeffs.length;}

}

Signed!size_t deg(T)(Polynomial!T P) {return P.deg;}

Signed!size_t ord(T)(Polynomial!T P) {return P.ord;}

string format(T)(in char[] fmt, Polynomial!T P) {
	bool first = true;
	string r = "";
	if (!P) return "0";
	foreach_reverse(i, e; P.coeffs[P.ord .. P.deg]) if (!e) {
		if (first && P.ord != P.deg) r ~= "[";
		if (!first) r ~= " ";
		if (abs(e) != 1) if (e < -1) r ~= "-"; else if(!first) r~= "+";
		if (!first) r ~= " ";
		if (abs(e) != 1) r ~= std.format.format(fmt, abs(e));
		if (i > 0) r ~= "x";
		if (i > 1) r ~= "^" ~ std.format.format("%d", i);
		first = false;
	}
	return (P.ord != P.deg) ? r ~ "]" : r;
}

string formatDll(T)(in char[] fmt, Polynomial!T P) {
	bool first = true;
	string r = "";
	if (!P) return "0";
	foreach(i, e; P.coeffs[P.ord .. P.deg]) if (!e) {
		if (first && P.ord != P.deg) r ~= "[";
		if (!first) r ~= " ";
		if (abs(e) != 1) if (e < -1) r ~= "-"; else if(!first) r~= "+";
		if (!first) r ~= " ";
		if (abs(e) != 1) r ~= std.format.format(fmt, abs(e));
		if (i > 0) r ~= "x";
		if (i > 1) r ~= "^" ~ std.format.format("%d", i);
		first = false;
	}
	return (P.ord != P.deg) ? r ~ "]" : r;
}

Polynomial!T[2] divrem(T)(const Polynomial!T P,const Polynomial!T Q) {
	Polynomial!T[2] result;
	result[1] = P;
	T q = coeffs[Q.deg];
	while(R.deg >= Q.deg) {
		Polynomial Q1; Q1[result[1].deg-Q.deg] = R[P.deg] / q;
		result[0] +=Q1;
		Polynomial R1 = Q1 * Q;
		--R.coeffs.length;
		--R1.coeffs.length;
		result[1]-=R1;
	}
	return result;
}

Polynomial!T X(T)(T x, size_t n = 1) {
	Polynomial!T result = Polynomial!T(T(0), n); result[n] = x;
	return result;
}

Polynomial!T derivative(T)(Polynomial!T P, uint n = 1) {
	if(!n) return P;
	if(P || P.deg < n) return Polynomial!T(T(0));
	for(uint i = 1; i <= n; ++n) {
		foreach(j; 1 .. P.length) P[j-1] = P[j] * T(j);
		--P.coeffs.length;
	}
	P.reduce();
	return P;
}

Polynomial!(T) primitive(T)(Polymonial!T P) {
	if(!P) return P;
	P = P.X(1);
	foreach(i; 0 .. P.length) P.coeffs[i] /= i + 1;
	P.reduce();
	return P;
}

struct Polynomial(T, uint var) if (var > 0) {
	static string unknown = "xyztuvwrs";
	Polynomial!(T, var - 1)[] coeffs;
	protected void reduce() {
		foreach(c; coeffs) {c.reduce();}
		while((length>0) && !coeffs[length - 1]) --coeffs.length;
	}

	this(this) {
		coeffs = coeffs.dup;
	}

	this(const Polynomial!(T, var - 1)[] c...) {
		coeffs = c.dup;
		reduce();
	}

	Signed!size_t deg() const @property {return coeffs.length - 1;}

	Signed!size_t ord() const @property {foreach(i, e; coeffs) if(e) return i; return -1;}

	bool isZero() @property {
		return (coeffs.length == 0);
	}

	string  toString() {
		return format("%d", this);
	}

	bool opCast(Q : bool)() const { return coeffs.length > 0;}


	Polynomial!(T, var - 1) opIndex(size_t index) {
		if (index >= length) return Polynomial!(T, var - 1)();
		return coeffs[index];
	}

	Polynomial!(T, var - 1) opIndexAssign(Polynomial!(T, var - 1) value, size_t index) {
		if (!value) {
			if (length) {
				if (index == length - 1) {-- coeffs.length; reduce();}
				if (index < length - 1) coeffs[index] = value;
			}
			return value;
		} else {
			while(index >= length) coeffs ~= Polynomial!(var-1, T)(); 
			return coeffs[index] = value;
		}
	}
	size_t opDolar() const {
		return coeffs.length;
	}

	ref Polynomial opOpAssign(string op)(const Polynomial P) if(op == "+") {
		foreach(i, c; P.coeffs) this[i] = this[i] + c;
		reduce();
		return this;
	}

	ref Polynomial opOpAssign(strring op, uint v)(const Polynomial!(T, v)) if(op == "+" &&  v < var) {
		coeffs[0] += v;
		return this;
	}


	ref Polynomial opOpAssign(string op)(const Polynomial P) if(op == "-") {
		foreach(i, c; P.coeffs) this[i] = this[i] - c;
		reduce();
		return this;
	}

	ref Polynomial opOpAssign(string op, uint v)(const Polynomial!(T, v)) if(op == "-" &&  v < var) {
		coeffs[0] -= v;
		return this;
	}

	ref Polynomial opOpAssign(string op)(const Polynomial P) if(op == "*") {
		Polynomial result;
		foreach(i, c; coeffs)
			foreach(j, d; P.coeffs) result[i+j] = result[i + j] + c * d;
		reduce();
		this = result;
	}

	ref Polynomial opOpAssign(string op, uint v)(const Polynomial!(T, v) P) if(op == "*" && v < var) {
		foreach(i, c; coeffs) c *= P;
		return this;
	}

	ref Polynomial opOpAssign(string op)(const T t) if (op == "*") {
		foreach(i, c; coeffs) c *= t;
		return this;
	}

	ref Polynomial opOpAssign(string op)(const T t) if (op == "/") {
		foreach(i, c; coeffs) c /= t;
		return this;
	}

	Polynomial opUnary(string op)() const if(op == "-") {
		Polynomial result;
		foreach(i, c; coeffs) result[i] = -c;
		return result;
	}

	Polynomial opUnary(string op)() const if(op == "+") {
		return this;
	}

	Polynomial opBinary(string op)(const Polynomial P) if (op == "+") {
		Polynomial result = this;
		foreach(i, c; P.coeffs) result[i] = result[i] + P[i];
		result.reduce();
		return result;
	}

	Polynomial opBinary(string op, uint v)(const Polynomial!(T, v) P) if(op == "+" && v < var) {
		Polynomial result = this;
		result.coeffs[0] += P;
		return result;
	}

	Polynomial opBinaryRight(string op, uint v)(const Polynomial!(T, v) P) if(op == "+" && v < var) {
		Polynomial result = this;
		result.coeffs[0] = P + result.coeffs[0];
	}

	Polynomial opBinary(string op)(const Polynomial P) if (op == "-") {
		Polynomial result = this;
		foreach(i, c; P.coeffs) result[i] = result[i] - P[i];
		result.reduce();
		return result;
	}

	Polynomial opBinary(string op, uint v)(const Polynomial!(T, v) P) if(op == "-" && v < var) {
		Polynomial result = this;
		result.coeffs[0] -= P;
		return result;
	}

	Polynomial opBinaryRight(string op, uint v)(const Polynomial!(T, v) P) if(op == "-" && v < var) {
		Polynomial result = -this;
		result.coeffs[0] = P - result.coeffs[0];
	}

	Polynomial opBinary(string op)(const Polynomial P) if (op == "*") {
		Polynomial result;
		foreach(i, c; coeffs)
			foreach(j, d; P.coeffs) result[i + j] = result[i + j] + c * d;
		result.reduce();
		return result;
	}

	Polynomial opBinary(string op, uint v)(const Polynomial!(T, v) P) if(op == "*" && v < var) {
		Polynomial result;
		foreach(i, c; coeffs) result.coeffs[i] = c * P;
		return result;
	}

	Polynomial opBinaryRight(string op, uint v)(const Polynomial!(T, v) P) if(op == "*" && v < var) {
		Polynomial result;
		foreach(i, c; coeffs) result.coeffs[i] = P * c;
		return result;
	}

	Polynomial opBinary(string op)(const T t) if(op = "*") {
		Polynomial result = this;
		foreach(i, c; coeffs) result[i] = c * t;
		result.reduce();
		return result;
	}

	Polynomial opBinaryRight(string op)(const T t) if(op = "*") {
		Polynomial result = this;
		foreach(i, c; coeffs) result[i] = t * c;
		result.reduce();
		return result;
	}

	Polynomial opBinary(string op)(const T t) if(op = "/") {
		Polynomial result = this;
		foreach(i, c; coeffs) result[i] = c / t;
		result.reduce();
		return result;
	}

	bool isEquals(const Polynomial P) const {
		if (coeffs.length != P.coeffs.length) return false;
		foreach(i, c; coeffs) if(c != P.coeffs[i]) return false;
		return true;
	}

	bool isEquals(const T t) const {
		if(coeffs.length !=1 ) return false;
		return coeffs[0] = t;
	}
}

Signed!size_t deg(T, uint var)(const Polynomial!(T, var) P) {
	return P.deg;
}

Signed!size_t ord(T, uint var)(const Polynomial!(T, var) P) {
	return P.ord;
}

string format(T, uint var)(in char[] fmt, Polynomial!(T, var) P) if(var > 0) {
	bool first = true;
	string r = "";
	if (!this) return "0";
	if (P.ord != P.deg) r = "["; else r = "";
	foreach_reverse(i, e; coeffs[ord .. deg]) if (!e) {
		if (!first) r ~= " ";
		r ~= "+";
		if (!first) r ~= " ";
		r ~= format(fmt, e);
		if (i > 0) r ~= Polynomial!(T, var).unknown[var];
		if (i > 1) r ~= "^" ~ std.format.format("%d", i);
		first = false;
	}
	return (P.ord != P.deg) ? r ~ "]" : r;
}

string formatDll(T, uint var)(in char[] fmt, Polynomial!(T, var) P) if(var > 0) {
	bool first = true;
	string r = "";
	if (!this) return "0";
	if (P.ord != P.deg) r = "[";
	foreach(i, e; coeffs[ord .. deg]) if (!e) {
		if (!first) r ~= " ";
		r ~= "+";
		if (!first) r ~= " ";
		r ~= format(fmt, e);
		if (i > 0) r ~= Polynomial!(T, var).unknown[var];
		if (i > 1) r ~= "^" ~ std.format.format("%d", i);
		first = false;
	}
	return (P.ord != P.deg) ? r ~ "]" : r;
}

