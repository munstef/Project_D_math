module numerics.polynomial.special;
import numerics.polynomial;
import numerics.integral;
import numerics.algorithm : Recurrence, recurrence;

Polynomial!T exp(T)(size_t n) {
	Polynomial!T result; result[0] = T(1);
	T k = T(1);
	for(int i =  1; i <= n; ++i) {k/=i; result[i] = k;}
	return result;
}

Polynomial!T cosh(T)(size_t n) {
	Polynomial!T result; result[0] = T(1);
	T k = T(1);
	for(int i =  1; i <= n; ++i) {k /= i; if(!(i % 2)) result.coeffs[i] = k;}
	return result;
}

Polynomial!T sinh(T)(size_t n) {
	Polynomial!T result;
	T k = T(1);
	for(int i = 1; i <= n; ++i) { if((i % 2)) result[i] = (k /= i); else k /= i;}
	return result;
}

Polynomial!T cos(T)(size_t n) {
	Polynomial!T result = Polynomial!T();
	T k = -1;
	for(int i =  1; i <= n; ++i) if(i % 2) k /= i; else result[i] = (k /= -i);
	return result;
}

Polynomial!T sin(T)(size_t n) {
	Polynomial!T result;
	T k = -1;
	foreach(i; 1 .. n) if(!(i % 2)) k /= i; else result[i] = (k /= -i);
	return result;
}

Polynomial!T tanh(T)(size_t n) {
	return sinh!T(n + 1).div(cosh!T(n + 1), n);
}

Polynomial!T tan(T)(size_t n) {
	return sin!T(n+1).div(cos!T(n+1), n);
}

@property Polynomial!T smoothStep(T, T a)() if(is_SignedIntegral!T && a > 0) {
	Polynomial!T result = T(0);
	for(T n = T(0); n < a - T(1); ++ n) 
		result[a + n] = pow_1(n) * binomial!T(a + n - 1, n) * binomial!T(2 * a - 1, a - n - 1);
	return result;
}

@property Polynomial!T smoothStep(T, T a: T(1))() if(is_SignedIntegral!T){
	return Polynomial!T = X!T(T(1));
}

@property Polynomial!T smoothStep(T, T a : T(2) = T(2))() if(isSignedIntegral!T) {
	return Polynomial!T(T(0), T(-2), T(3));
}

Polynomial!T ln1px(T)(size_t n) {
	T[] R; R~=[T(0)];
	for(size_t k = 1; k <= n; ++k) R ~= R[k-1] / T(-k);
	return Polynomial!T(R);
}

Polynomial!T F(T)(T[] num, T[] den, size_t order) {
	size_t m = num.length; size_t n = den.length;
	bool f = true;
	foreach(e; den) f = f && (e > 0);
	assert(f);
	T s = 0;
	s  = sum(num, T(0));
	s -= sum(den, T(0));
	Polymonial!(T, var) r = 1;
	size_t k = 0;
	Polymonial c = T(1);
	while(k <= order) {
		p1 = product(num, T(1));
		p1 /= product(den, T(1));
		c *= X(1) * p1 / k; 
		r = r + c;
		++k;
		if(k < order) { ++num; ++den;}
	}
	return r;
}

Polynomial!T sec(T)(size_t n) { return Polynomial!T(T(1)).div(cos!T(n+1), n);}

Polynomial!T  sech(T)(size_t n) { return Polynomial!T(T(1)).div(cosh!T(n+1), n);}

auto W(T)(Polynomial!T P, Polynomial!T Q) {
	return Recurrence!((a, n) => P * a[n-1] + Q * a[n-2], Polynomial!T, 2)([Polynomial!T(T(0)), Polynomial!T(T(1))]);
}

auto w(T)(Polynomial!T P, Polynomial!T Q) {
	return Recurrence!((a, n) => P * a[n-1] + Q * a[n-2], Polynomial!T, 2)([Polynomial!T(T(0)), P]);
}

static struct Laguerre(T) {
	auto L = recurrence!((a, n) => (Polynomial!T(T(2 * n - 1), T(-1)) * a[n - 1] - T(n - 1) * a[n - 2])/T(n))([Polynomial!T(T(1)), Polynomial!T(T(1), T(-1))]);
	auto L2(size_t n, size_t m) { return isOdd(m) ? -L[n + m].derivative(m) : L[n + m].derivative(m);}
}

static struct Bernstein(T) {
	auto B = sequence!((a, n) => binomial(n, i) * X!T(1, i) * Polynomial!T([1, -1]) ^^ (n - i))();
}

static struct Chebyshev(V) {
	alias _t = Recurrence!((a, b) => Polynomial!V(V(0), V(2)) * a[n - 1] - a[n - 2], Polynomial!V, 2);
	auto T = _t([Polynomial!V(V(1)), Polynomial!V(V(0), V(1))]);
	auto U = _t([Polynomial!V(V(1)), Polynomial!V(V(0), V(2))]);
}

static struct Fibonacci(T) {
	auto F = W!T(Polynomial!T(T(0), T(1)), Polynomial(T(1)));
}

static struct Lucas(T) {
	auto L = w!T(Polynomial!T(T(0), T(1)), Polynomial!T(T(1)));
}

static struct Fermat(T) {
	auto F =  W!T(Polynomial!T([T(0), T(3)]), Polynomial!T(T(-2)));
	static struct Lucas {
		auto F = w!T(Polynomial!T(T(0), T(3)), Polynomial!T(T(-2)));
	}
}

static struct Jacobsthal(T) {
	auto J = W!T(Polynomial!T(T(1)), Polynomial!T(T(0), T(2)));
	static struct Lucas {
		auto J = w!T(Polynomial!T(T(1)), Polynomial!T(T(0), T(2)));
	}
}

static struct Gegenbauer(T) {
	auto C = recurrence!((a, n) => (Polynomial!T(T(0), T(2*(n + λ - 1)), T(1)) * a[n - 1] - T(n + 2 * λ - 2) * a[n - 2]) / T(n))(Polynomial!T(T(1)), Polynomial!T(T(0), T(2 * λ)));
}

static struct Hermite(T) {
	auto H = recurrence!((a, n) => Polynomial!T(T(0), T(2)) * a[n - 1] + T(2 * (n - 1)) * a[n - 2])(Polynomial!T(T(1)), Polynomial!T(T(0), T(2)));
}

static struct Legendre(T) {
	auto P = recurrence!((a, n) => (Polynomial!T(T(0), T(2 * n - 1)) * a[n - 1] - T(n - 1) *  a[n - 2]) / n)(Polynomial!T(T(1)), Polynomial!T(T(0), T(1)));
}

static struct Mittag {
	static struct Leffer(T) {
		auto M = reccurence!((a, n) => (a[n - 1](Polynomial!T(T(1), T(1))) + T(2) * a[n -1] + a[n - 1](Polynomial!T(T(-1), T(1)))) / T(2))(Polynomial!T(T(1)));
	}
}

static struct Morgan {
	struct Voice(T) {
		alias _b = Recurrence!((a, n) => Polynomial!T(T(2), T(1)) * a[n - 1] - a[n - 2], Polynomial!T, 2);
		auto b = _b([Polynomial!T(T(1)), Polynomial!T(T(1), T(1))]);
		auto B = _b([Polynomial!T(T(1)), Polynomial!T(T(2), T(1))]);	
	}
}


static struct Pell(T) {
	auto P = w!(Polynomial!T(T(0), T(2)), Polynomial!(T(1)));
	struct Lucas {
		auto Q = W!(Polynomial!T(T(0), T(2)), Polynomial!T(T(1)));
	}

}


static struct Pollaczek(T, T b, Tc) {
	auto P = reccurence!((a, n) => (Polynomial!T(T(2 * b),T(2 * n - 1 + 2 * b)) * a[n - 1] - T(n - 1) * a[n - 2]) / T(n))(Polynomial!T(T(1), Polynomial!T(T(2 * c), T(2 * b + 1))));
}



static struct Eulerian(T) {
	auto A = recurrence!((a, n) => Polynomial!T([T(0), T(1), T(1)]) * derivative(a[n - 1]) + a[n - 1] * Polynomial!T([T(1), T(n - 1)]))(Polynomial!T([T(1)]));
	auto P = recurrence!((a, n) => Polynomial!T([T(1), T(2 * (n - 1))]) * a[n - 1] - Polynomial!T([T(0), T(-1), T(1)]) * derivative(a[n - 1]))(Polynomial!T([T(1)]));
}
