module numerics.imaginary;
import numerics;
import meta;

struct Imaginary(T) if(is_floating_point!T) {

	static if (is(T == float)) ifloat val;
	static if (is(T == double)) idouble val;
	static if (is(T == real)) ireal val;

	alias val this;

	T re() @property {return zero!T;}	

	this(T t) { im = t;}

	Imaginary opUnary(string op : "+")() const {
		return this;
	}

	Imaginary opUnary(string op : "-")() const {
		return Imaginary(-im);
	}

	Imaginary opBinary(string op : "+")(const Imaginary im) const {
		return Imaginary(this.im +im);
	}

	Imaginary opBinary(string op : "-")(const Imaginary im) const {
		return Imaginary(this.im - im);
	}

	T opBinary(string op : "*")(const Imaginary im) const {
		return -this.im * im;
	}

	T opBinary(string op : "/")(const Imaginary im) const {
		return this.im / im;
	}

	Complex opBinary(string op : "+")(const T re) const {
		return Complex(re, im);
	}

	Complex opBinary(string op : "-")(const T re) const {
		return Complex(-re, im);
	}

	Imaginary opBinary(string op : "*")(const T re) const {
		return Imaginary(re * im);
	}

	Imaginary opBinary(string op : "/")(const T re) const {
		return Imaginary(im / re);
	}

	Complex opBinaryRight(string op : "+")(const T re) const {
		return Complex(re, im);
	}

	Complex opBinaryRight(string op : "-")(const T re) const {
		return Complex(re, -im);
	}

	Imaginary opBinaryRight(string op : "*")(const T re) const {
		return Imaginary(re * im);
	}

	Imaginary opBinaryRight(string op : "/")(const T re) const {
		return Imaginary(-re / im);
	}

	T i() const @property { return -im;}

	enum  I = Imaginary(T(1));
}

T arg(T)(const Imaginary!T x) { return x.arg;}

Imaginary!T conj(T)(const Imaginary!T im) {
	return Imaginary!T(-im.im);
}

T abs(T)(const Imaginary!T im) {
	return abs(im.im);
}

Imaginary!T inv(T)(const Imaginary!T x) {
	return U(-1/x.im);
}

T sqr(T)(const Imaginary!T x) {
	return -x.im * x.im;
}

Complex!T sqrt(T)(const Imaginary!T x) {
	return polar(sqrt(abs(x)), sign(x.im) * Pi / 4);
}

Complex!T exp(T)(const Imaginary!T x) {
	return Complex!T(cos(x.im), sin(x.im));
}

Complex!T log(T)(const Imaginary!T x) {
	return Complex!T(log(abs(x.im)), arg(x));
}

Imaginary!T sin(T)(const Imaginary!T x) {
	return sinh(x.im).i;
}

T cos(T)(Imaginary!T x) {
	return cosh(x.im);
}

Imaginary!T tan(T)(const Imaginary!T x) {
	return tanh(x.im).i;
}

Imaginary!T cot(T)(const Imaginary!T x) {
	return -coth(x.im).i;
}

Imaginary!T sinh(T)(const Imaginary!T x) {
	return sin(x.im).i;
}

T cosh(T)(Imaginary!T x) {
	return cos(x.im);
}

Imaginary!T tanh(T)(const Imaginary!T x) {
	return tan(x.im).i;
}

Imaginary!T asin(T)(const Imaginary!T x) {
	return asinh(x.im).i;
}

Complex!T acos(T)(Imaginary!T x) {
	return Complex!T(Pi/2, - asinh(x.im));
}

Imaginary!T atan(T)(const Imaginary!T x) {
	return atanh(x.im).i/2;
}

Imaginary!T acot(T)(const Imaginary!T x) {
	return atanh(1.0/x.im).i/2;
}

Imaginary!T asinh(T)(const Imaginary!T x) {
	return sin(x.im).i;
}

Complex!T acosh(T)(Imaginary!T x) {
	return Complex!T(-asinh(x.im), Pi/2);
}

Imaginary!T atanh(T)(const Imaginary!T x) {
	return atan(x.im).i;
}

Imaginary!T acoth(T)(const Imaginary!T x) {
	return -acot(x.im).i;
}

mixin Functions!(ifloat);
mixin Functions!(idouble);
mixin Functions!(idouble);


