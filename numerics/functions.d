module numerics.functions;

static import std.math;
import std.mathspecial;
import std.conv : to, parse;
import std.range;
import std.algorithm.iteration : sum;
import numerics;
import numerics.complex;
import std.math;
import std.mathspecial : Γ = gamma, regγ = gammaIncomplete, regΓ = gammaIncompleteCompl, ψ0 = digamma, regβ = betaIncomplete, β = beta, invRegΓ = gammaIncompleteComplInverse, invRegβ = betaIncompleteInverse;

enum π = Pi;

alias φ = Phi;

real Π(real x) {return Γ(x + 1);}

real Γ(real a, real x) { return Γ(a, x) / Γ(a);}

real γ(real a, real x) { return γ(a, x) / Γ(a);}

real factorial(uint x) {
	return Π(x);
}

real β(real a, real b, real x) {
	return regβ(a, b, x) / β(a, b);
}

real invΓ(real a, real x) {
	return invRegΓ(a, x * Γ(a));
}

real invRegγ(real a, real x) {
	return invRegΓ(a, 1 - x);
}

real invγ(real a, real x) {
	return invRegγ(a, x * Γ(a));
}

real invβ(real a, real b, real x) {
	return invRegβ(a, b, x * β(a, b));
}

T inv(T)(const T x) if(is_floating_point!T) {return 1/x;}

real fibonacci(real x) {
	real p = pow(φ, x);
	return (p - std.math.cos(x * π) / p) / Sqrt5;
}
enum : real { α = .log(φ)}
real sFh(real x) { return 2/Sqrt5 * sinh(2 * x * α);}
real cFh(real x) { return 2/Sqrt5 * cosh((2 * x + 1) * α);}
real tFh(real x) { return sFh(x) / cFh(x);}

Complex!T fibonacci(T)(Complex!T ν) if (is_floating_point!T) {
	Complex!T p = pow(φ, ν);
	return (p - std.complex.cos(ν * π) / p) / Sqrt5;
}
real lucas(real x) {
	real p = pow(φ, x);
	return p - std.math.cos(x * π) / p;
}

Complex!T lucas(T)(Complex!T ν) if (is_floating_point!T){
	Complex!T p = φ ^^ ν;
	return p + std.complex.cos(ν * π) / p;
}

template smoothStep(uint a) {
	real smoothStep(uint a :1)(real x)
	in { assert(x <=1 && x >=0, "argument not in [0; 1]");}
	body {return x;}

	real smoothStep(uint a: 2)(real x)
	in { assert(x <=1 && x >=0, "argument not in [0; 1]");}
	body {return x*x*(3 - 2 * x);}

	real smoothStep(uint a: 3)(real x)
	in { assert(x <=1 && x >=0, "argument not in [0; 1]");}
	body {return x*x*x*(10 +x * (- 15 + 6*x));}

	real smoothStep(uint a: 4)(real x)
	in { assert(x <=1 && x >=0, "argument not in [0; 1]");}
	body {return pow(x, 4) * (35 + x *(-84 + x * (70 + x * -20)));}

    real smoothStep(uint a)(real x) if (a > 4) 
	in { assert(x <=1 && x >=0, "argument not in [0; 1]");}
	body {
		real result = 0;
		for (uint n = 0; n <= a - 1; ++n)
			result += pow_1(n) * binomial(a + n - 1, n) * binomial(2 * a - 1, a - n - 1) * pow(x, a + n);
		return result;
	}
}

T harmonic(T)(N n) if(is_UnsignedIntegral!N && is_floating_point!T) { return  Gamma + digamma(n + 1);}
T primeHarmonic(T)(size_t n) {primes.init; return sum(take(Primes, n).map!inv);}

T arg(T)(in T x) if(is_floating_point!T) { return x > 0 ? 0 : x < 0 ? T(Pi) : T.nan;}
T inv(T)(in T x) if(is_floating_point!T) { return x ? one!T/x : T.nan;}

T hypot(T)(in T x, in T y) if(is_floating_point!T) {return std.math.hypot(x, y);}
T sqrt(T)(in T x) if (is_floating_point!T) { return std.math.sqrt(x); }
T cbrt(T)(in T x) if (is_floating_point!T) {return std.math.cbrt(x);}
T sin(T)(in T x) if (is_floating_point!T) { return std.math.sin(x); }
T cos(T)(in T x) if (is_floating_point!T) { return std.math.cos(x); }
T tan(T)(in T x) if (is_floating_point!T) { return std.math.tan(x); }
T sinh(T)(in T x) if (is_floating_point!T) { return std.math.sinh(x); }
T cosh(T)(in T x) if (is_floating_point!T) { return std.math.cosh(x); }
T tanh(T)(in T x) if (is_floating_point!T) { return std.math.tanh(x); }
T asin(T)(in T x) if (is_floating_point!T) { return std.math.sin(x); }
T acos(T)(in T x) if (is_floating_point!T) { return std.math.cos(x); }
T atan(T)(in T x) if (is_floating_point!T) { return std.math.tan(x); }
T atan2(T)(in T y, in T x) if (is_floating_point!T) { return std.math.atan2(y, x); }
T asinh(T)(in T x) {return std.math.asinh(x);}
T acosh(T)(in T x) {return std.math.acosh(x);}
T atanh(T)(in T x) {return std.math.atanh(x);}
T pow(T)(in T x, in T power) if (is_floating_point!T) { return std.math.pow(x, power); }
T powi(T)(in T x, int power) if (is_floating_point!T) { return std.math.pow(x, power); }
T exp(T)(in T x) if (is_floating_point!T) { return std.math.exp(x); }
T log(T)(in T x) if (is_floating_point!T) { return std.math.log(x); }
T log2(T)(in T x) if (is_floating_point!T) { return std.math.log2(x); }
T log10(T)(in T x) if (is_floating_point!T) { return std.math.log10(x); }
T log(T, T base)(in T x) if (is_floating_point!T) { return std.math.log(x) / std.math.log(base);}
T abs(T)(in T x) if (is_floating_point!T) { return std.math.fabs(x); }
T floor(T)(in T x) if (is_floating_point!T) { return std.math.floor(x); }
T exp2(T)(in T x) if (is_floating_point!T) { return std.math.exp2(x); }
T ceil(T)(in T x) if (is_floating_point!T) { return std.math.ceil(x); }
T trunc(T)(in T x) if (is_floating_point!T) { return std.math.trunc(x); }
T rint(T)(in T x) if (is_floating_point!T) { return std.math.rint(x); }
T nearbyint(T)(in T x) if (is_floating_point!T) { return std.math.nearbyint(x); }
T copysign(T)(in T mag, in T sgn) if (is_floating_point!T) { return std.math.copysign(mag, sgn); }
T round(T)(in T x) if (is_floating_point!T) { return std.math.round(x); }
T muladd(T)(in T a, in T b, in T c) if (is_floating_point!T) { return a * b + c; }
T min(T)(in T x, in T y) if (is_floating_point!T) { return std.math.fmin(x, y); }
T max(T)(in T x, in T y) if (is_floating_point!T) { return std.math.fmax(x, y); }
T arg(T)(in T x) if(is_floating_point!T) {
	return x.isPositive!T ? zero!T : x.isNegative!T ? pi!T : T.nan;
}

Tuple!(T, "ρ", T, "θ") polar(T)(in Complex!T z) if(is_floating_point!T) { return tuple!("ρ", "θ")(abs(x), arg(x));}
Complex!T polar(T)(T ρ, T θ){ creal c = std.math.expi(θ); return Complex!T(c.re, c.im);}

/*
mixin template Functions(T) {

	auto log2(T)(const T x) {return log(x) / Ln2;}
	auto log10(T)(const T x) { return log(x) / Ln10;}
	auto sec(T)(const T x) {return inv(cos(x));}
	auto csc(T)(const T x) {return inv(sin(x));}
	auto cot(T)(const T x) {return cos(x) / sin(x);}
	auto sech(T)(const T x) {return inv(cosh(x));}
	auto csch(T)(const T x) {return inv(sinh(x));}
	auto coth(T)(const T x) {return atan(inv(x));}

	auto asec(T)(const T x) {return acos(inv(x));}
	auto acsc(T)(const T x) {return asin(inv(x));}
	auto acot(T)(const T x) {return atan(inv(x));}
	auto asech(T)(const T x) {return acosh(inv(x));}
	auto acsch(T)(const T x) {return asinh(inv(x));}
	auto acoth(T)(const T x) {return inv(atanh(x));}

	auto sinc(T)(const T x) {return (x) ? sin(x) / x : T(1);}
	auto sinhc(T)(const T x) {return (x) ? sinh(x) / x : T(1);}
	auto tanc(T)(const T x) {return (x) ? tan(x) / x : T(1);}
	auto tanhc(T)(const T x) {return (x) ? tanh(x) / x : T(1);}

	auto sigmoid(T)(const T x) {return inv(1 + exp(-x));}
	auto invSigmoid(T)(const T x) {return -log(inv(x) - 1);}
	auto gd(T)(const T x) {return atan(tanh(x / 2)) * 2;}
	auto invGd(T)(const T x) {return atanh(tan(x / 2)) * 2;}

	auto versin(T)(const T a) { return 1 - cos(a); }
	auto coversin(T)(const T a) { return 1 - sin(a); }
	auto vercos(T)(const T a) { return 1 + cos(a); }
	auto covercos(T)(const T a) { return 1 + sin(a); }
	auto haversin(T)(const T a) { return versin(a) / 2; }
	auto hacoversin(T)(const T a) { return coversin(a) / 2; }
	auto havercos(T)(const T a) { return vercos(a) / 2; }
	auto hacovercos(T)(const T a) { return covercos(a) / 2; }
	auto exsec(T)(const T a) { return sec(a) - 1; }
	auto excsc(T)(const T a) { return csc(a) - 1; }

	auto aversin(T)(const T a) { return acos(1 - a); }
	auto acoversin(T)(const T a) { return asin(1 - a); }
	auto avercos(T)(const T a) { return  acos(a - 1); }
	auto acovercos(T)(const T a) { return asin(a - 1); }
	auto ahaversin(T)(const T a) { return acos(1 - 2 * a); }
	auto ahacoversin(T)(const T a) { return asin(1 - 2 * a); }
	auto ahavercos(T)(const T a) { return acos(2 * a - 1); }
	auto ahacovercos(T)(const T a) { return asin(2 * a - 1); }
	auto aexsec(T)(const T a) { return acos(inv(a + 1)); }
	auto aexcsc(T)(const T a) { return asin(inv(a + 1)); }

	auto versinh(T)(const T a) { return 1 - cosh(a); }
	auto coversinh(T)(const T a) { return 1 - sinh(a); }
	auto vercosh(T)(const T a) { return 1 + cosh(a); }
	auto covercosh(T)(const T a) { return 1 + sinh(a); }
	auto haversinh(T)(const T a) { return versinh(a) / 2; }
	auto hacoversinh(T)(const T a) { return coversinh(a) / 2; }
	auto havercosh(T)(const T a) { return vercosh(a) / 2; }
	auto hacovercosh(T)(const T a) { return covercosh(a) / 2; }
	auto exsech(T)(const T a) { return sech(a) - 1; }
	auto excsch(T)(const T a) { return csch(a) - 1; }

	auto aversinh(T)(const T a) { return acosh(1 - a); }
	auto acoversinh(T)(const T a) { return asinh(1 - a); }
	auto avercosh(T)(const T a) { return  acosh(a - 1); }
	auto acovercosh(T)(const T a) { return asinh(a - 1); }
	auto ahaversinh(T)(const T a) { return acosh(1 - 2 * a); }
	auto ahacoversinh(T)(const T a) { return asinh(1 - 2 * a); }
	auto ahavercosh(T)(const T a) { return acosh(2 * a - 1); }
	auto ahacovercosh(T)(const T a) { return asinh(2 * a - 1); }
	auto aexsech(T)(const T a) { return acosh(inv(a + 1)); }
	auto aexcsch(T)(const T a) { return asinh(inv(a + 1)); }
}

mixin Functions!float; 
mixin Functions!double;
mixin Functions!real;

mixin template Function_to_Real(T) if (is_integral!T || is_quotient!T) {	
	real sqrt(T)(T x) {return std.math.sqrt(cast(real) x);}	
	real log(T)(T x) {return std.math.log(cast(real) x);}
	real exp(T)(T x) {return std.math.exp(cast(real) x);}
	real sin(T)(T x) { return std.math.sin(cast(real)(x));}
	real cos(T)(T x) { return std.math.cos(cast(real)(x));}
	real tan(T)(T x) { return std.math.tan(cast(real)(x));}
	real asin(T)(T x) { return std.math.asin(cast(real)(x));}
	real acos(T)(T x) { return std.math.acos(cast(real)(x));}
	real atan(T)(T x) { return std.math.atan(cast(real)(x));}
	real sinh(T)(T x) { return std.math.sinh(cast(real)(x));}
	real cosh(T)(T x) { return std.math.cosh(cast(real)(x));}
	real tanh(T)(T x) { return std.math.tanh(cast(real)(x));}
	real asinh(T)(T x) {return std.math.log(cast(real)(x) + std.math.sqrt(1 + sqr!real(cast(real) x)));}
	real acosh(T)(T x) {return std.math.log(cast(real)(x) + std.math.sqrt(cast(real) x + 1) * std.math.sqrt(cast(real) x - 1));}
	real atanh(T)(T x) {return (std.math.log(1 + cast(real)(x)) - std.math.log(1 - cast(real) x)) / 2;}
	real arg(T)(T x) {return isZero!T(x) ? real.nan : x.isPositive ? zero!real : Pi;}
	Imaginary!real i(T)(T x) {return ireal(cast(real)x);}
}
*/
real binomial(const real n, const real p) {
	return Π(n) / Π(n - p) / Π(p);
}

real arrangement(const real n, const real p) {
	return Π(n) / Π(n - p);
}

real subFactorial(const real n) {
	return Γ(n + 1, -1) / numerics.E;
}

alias derangement = subFactorial;

T F(T)(T[] num, T[] den, const T x) { 
	size_t m = num.length;
	size_t n = den.length;
	T s = sum(num, T(0)) - sum(den, T(0));
	assert(m <= n || (abs(x) < 1) && (n == m -1) || !x || (n == m - 1) && (abs(x) == 1 || s.re < 0));
	T r, r1;
	r = r1 = T(1);
	T k = T(1);
	T c = product(num, T(1)) / product(den, T(1)) * x;
	r = r + c;
	while(r != r1) {
		r1 = r;
		++num[]; ++den[];
		c = x * c * product(num, T(1)) / product(den, T(1)) / ++k;
		r = r + c;
	}
	return r;
}

T besselJ(T, T v)(T t) if (is_floating_point!T) {
	return inv(Γ(v + 1)) * pow((t / 2), v) * F([], [v + 1], - sqr(t) / 4);
}

T besselJ(T, int n)(T t) if(is_floating_point!T) {
	if (n < 0) return (n & 1) ? -besselJ!(-n)(t) : besselJ!(-n)(t);
	T z = t / 2;
	T term = 1;
	for(int i= 1; i <= n; ++i) term *= z / i;
	T z2 = sqr(z);
	T result = term;
	T result_old = term;
	for (T i = 1; ; i+=2.0) {
		term *= z2 / (i * (n + 1));
		result -= term;
		trem *= z2 / ((i + 1) * (n + i + 1));
		result += term;
		if (result_old == result) return result;
		result_old = result;
	}
}

T Ei(T)(const T x) {
	return x * F([T(1), T(1)], [T(2), T(2)], x) + log(x) + Gamma;
}

T E(T, T v)(const T x) {
	return pow(x, v - 1) * Γ(1-v) - F([1 - v],[2 - v], -x) / (1 - v);
}
T E(T, T t)(T ν, T a) {
	return pow(a, -ν) * exp(a * t) * γ(ν, a * t)/Γ(ν);
}	

T li(T)(const T x) {
	return Ei(log(x));
}

enum : real {
	// li(2)
	Li2 = 1.04516378011749278484458888919461313652261557815120157583290914407501320521035953017271740562638335630602, 
	// Soldner's constant : li(μ) = 0
	μ   = 1.45136923488338105028396848589202744949303228364801586309300455766242559575451783565953135771108682884
}

T Li(T)(const T x) {
	return li(x) - Li2;
}

//Polylog
T Li(T, uint n)(const T z) {
	return Φ(z, n, 1) * z;
}

T digamma(T)(const T x) {
	if (x > 1) enforce(x <= 1, "diLog is not define in ]1, +∞]");
	if (x == 0) return T(0);
	if (x == 1) return T(π * π / 6);
	if (x == -1) return T(- π * π / 12);
	if (abs(x) < 1) return x * F([T(1), T(1), T(1)], [T(2), T(2)], x);
	return T(π * π / 6 - sqr(log(-x))/2) - diLog(inv(x));
}

T polygamma(T, uint n)(T z) {
	return -pow_1!m * factorial(m) * Hurwitz.ζ(n + 1, z);
}

T Ci(T)(const T x) {
	T s = -sqr(x) / 4;
	return s *= F([T(1), T(1)], [T(2), T(2), T(1.5)], s) + log(x) + Gamma;
}

T Chi(T)(const T x) {
	T s = sqr(x) / 4;
	return s *= F([T(1), T(1)],[T(2), T(2), T(1.5)], s) + log(x) + Gamma;
}

T Si(T)(const T x) {
	return x * F([T(0.5)], [T(1.5), T(1.5)], - sqr(x)/4);
}

T Shi(T)(const T x) {
	return x * F([T(0.5)], [T(1.5), T(1.5)], sqr(x)/4);
}

T S(T)(const T x) {
	return PI * pow(x, 3) / 6 * F([T(0.75)],[T(1.5), T(1.75)], -sqrt(π * sqr(x)/4));
}

T C(T)(const T x) {
	return x * F([T(0.25)], [T(0.5), T(1.25)], -sqr(PI * sqr(x) / 4));
}

T cos(T, uint v)(const T x) {
	T k, r, r0;
	r0 = 0;
	r = cos(x);
	k = 2;
	while(r0 != r) {
		r0 = r;
		r = cos(k * x) / pow(k, v);
		++k;
	}
	return r;
}

T sin(T, uint v)(const T x) {
	T k, r, r0;
	r0 = 0;
	r = sin(x);
	k = 2;
	while(r0 != r) {
		r0 = r;
		r = sin(k * x) / pow(k, v);
		++k;
	}
	return r;
}

T exp(T, uint v)(const T x) {
	T e, k; e = k = 1;
	for(uint i = 1; i <= v; i++) e+=(k*=x/i);
	return e;
}

static struct Whittaker {
	T M(uint k, uint m)(T x) {
		return exp(-x/2) * pow(x, m + 0.5) * F([T(m - k + 0.5)], [T(1 + 2 * m)], x);
	}
}
//Dirichelet eta
T η(T)(T z) {
	ulong n = 1;
	T result = T(0);
	T r_1 = T(1);
	for(ulong n = 1; r_1 != result; ++n) {
		r_1 = result;
		result += pow_1(n - 1) / pow(n, z);
	}
	return result;
}
// Riemann zeta
T ζ(T)(T z) {
	return η(z) / (1 - pow(2, 1 - z));
}

T ζ(T)(const T z, const T a) {
	T  r_1 = T(1);
	T result = 0;
	T zn = 1;
	T k = 0;
	while(r_1 != result) {
		r_1 = result;
		result += pow(sqr((a + k)), -z / 2);
	}
	return result;
}

// Direchet lambda
T λ(T)(T z) {
	return η(z) * (pow(2, z) - 1) / (pow(2, z) - 2);
}
// Lerch phi
T Φ(T)(T z, T s, T a) {
	T  r_1 = T(1);
	T result = 0;
	T zn = 1;
	T k = 0;
	while(r_1 != result) {
		r_1 = result;
		result += pow(z, k) / pow(sqr((a + k)), s / 2);
	}
	return result;
}
// Dirichlet beta
T β(T)(T s) {
	return pow(2, - s) * Φ(-1, s, 0.5);
}

static struct Hurwitz {
	T ζ(T)(const T z, const T a) {
		T  r_1 = T(1);
		T result = 0;
		T zn = 1;
		T k = 0;
		while(r_1 != result) {
			r_1 = result;
			result += pow((a + k), -z);
		}
		return result;
	}
}


