module numerics.complex;

import numerics;
import numerics.functions;
import std.typecons : Tuple, tuple;

struct Complex(T) if(is_floating_point!T) {
	union {
		static if(is(T==float)) cfloat value;
		static if(is(T==double)) cdouble value;
		static if(is(T==real)) creal value;
		struct { T x, y;}
		T[2] t;
	}
	alias value this;
	this(T r, T i) { re = r; im = i;}
	this(T r) { this(r, T(0));}
	enum zero = Complex(T(0));
	enum one = Complex(T(1));
	enum nan = Complex(T.nan, T.nan);
	bool opEqual()(const Complex z) { return t == z.t;}
	bool opEqual()(const T x) { return re == x && im.isZero!T;}
	bool opEqual()(const Imaginary!T y) { return re.isZero!T && im == y.im;}
	bool isZero() const @property { return (re == 0) && (im == 0);}
	bool isNaN() const @property { return isNaN!T(re) || isNaN!T(im);}
	Complex opUnary(string op : "+")() {return this;}
	Complex opUnary(string op : "-")() {return Complex(-re, -im);}
	Complex opUnary(string op : "++")() {++re; return this;}
	Complex opUnary(string op : "--")() {--re; return this;}

	Complex opBinary(string op : "+")(const Complex z) {return Complex(re + z.re, im + z.im);}
	Complex opBinary(string op : "-")(const Complex z) {return Complex(re - z.re, im - z.im);}
	Complex opBinary(string op : "*")(const Complex z) {return Complex(re * z.re - im * z.im, im * z.re + re * z.im);}
	Complex opBinary(string op : "/")(const Complex z) {return this * conj(z) / abs(z);}
	Complex opBinary(string op : "^^")(const Complex z) { 
		if (isZero && z.isZero) return nan;
		else if(isZero) return zero;
		else if(z.isZero) return one;
		else return exp(log(this) * z);
	}
	Complex opBinary(string op : "^^")(const int n) {
		if((isZero) && (n == 0)) return nan;
		if(isZero) return zero;
		if(n == 0) return one;
		int m = n > 0 ? n : -n;
		Complex base = n > 0 ? this : inv(z);
		Complex result = one;
		while(m > 0) {
			if (m % 2) res *= base;
			m /= 2;
			if (m > 0) base *= base;
		}
		return result;
	}
	Complex opBinary(string op : "+")(const T x) {return Complex(re + x, im);}
	Complex opBinary(string op : "-")(const T x) {return Complex(re - x, im);}
	Complex opBinary(string op : "*")(const T x) {return Complex(re * x, im * x);}
	Complex opBinary(string op : "/")(const T x) {return Complex(re / x, im / x);}
	Complex opBinary(string op : "^^")(const T x) { 
		if (isZero && x.isZero!T) return nan;
		else if(isZero) return zero;
		else if(z.isZero) return one;
		else return exp(log(this) * x);
	}
	Complex opBinaryRight(string op : "+")(const T x) {return Complex(x + re, im);}
	Complex opBinaryRight(string op : "-")(const T x) {return Complex(x - re, -im);}
	Complex opBinaryRight(string op : "*")(const T x) {return Complex(x * re, x * im);}
	Complex opBinaryRight(string op : "/")(const T x) {return x * conj(z) / sqAbs(z);}
	Complex opBinaryRight(string op : "^^")(const T x) {
		if(x.isZero!T && isZero) return nan;
		else if(x.isZero!T) return zero;
		else if(isZero) return one;
		else return exp(Complex(abs(x), arg(x)) * this);
	}


	Complex!U opBinary(string op : "+", F, U : CommonType!(T, F))(const Complex!F z) {return Complex!U(re + z.re, im + z.im);}	
	Complex!U opBinary(string op : "-", F, U : CommonType!(T, F))(const Complex!F z) {return Complex!U(re - z.re, im - z.im);}
	Complex!U opBinary(string op : "*", F, U : CommonType!(T, F))(const Complex!F z) {return Complex!U(re * z.re - im * z.im, im * z.re + re * z.im);}
	Complex!U opBinary(string op : "/", F, U : CommonType!(T, F))(const Complex!F z) {return this * conj(z) / abs(z);}
	Complex!U opBinary(string op : "^^", F, U : CommonType!(T, F))(const Complex!F z) { 
		if (isZero && z.isZero) return Complex!U.nan;
		else if(isZero) return Complex!U.zero;
		else if(z.isZero) return Complex!U.one;
		else return exp(log(this) * z);
	}

	Complex!U opBinary(string op : "+", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(re + x, im);}
	Complex!U opBinary(string op : "-", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(re - x, im);}
	Complex!U opBinary(string op : "*", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(re * x, im * x);}
	Complex!U opBinary(string op : "/", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(re / x, im / x);}
	Complex!U opBinary(string op : "^^", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) { 
		if (isZero && x.isZero!T) return Complex!U.nan;
		else if(isZero) return Complex!U.zero;
		else if(x.isZero!T) return Complex!U.one;
		else return exp(log(this) * x);
	}

	Complex!U opBinaryRight(string op : "+", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(x + re, im);}
	Complex!U opBinaryRight(string op : "-", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(x - re, -im);}
	Complex!U opBinaryRight(string op : "*", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return Complex!U(x * re, x * im);}
	Complex!U opBinaryRight(string op : "/", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) {return x / sqAbs(this) * conj(this);}
	Complex!U opBinaryRight(string op : "^^", F, U : CommonType!(T, F))(const F x) if(!is(T == F)) { 
		if (isZero && x.isZero!T) return Complex!U.nan;
		else if(isZero) return Complex!U.one;
		else if(x.isZero!T) return Complex!U.zero;
		else return exp(Complex!F.log(x) * this);
	}

    Tuple!(T, "ρ", T, "θ") polar() @property { return tuple(abs!T(this), arg!T(this));}

	static Complex!T sqrt(const T x) {
		return (x < 0) ? Complex(0, sqrt(-x)) : (x > 0) ? Complex(sqrt(x), 0) : zero;
	}

	static Complex!T log(const T x) {
		return (x < 0) ? Complex(log(abs(x)), T(Pi)) : (x > 0) ? Complex(log(x), 0) : nan;
	}	

	static Complex!T log2(const T x) {
		return Complex.log(x) / T(Ln2);
	}	

	static Complex!T log10(const T x) {
		return Complex.log(x) / T(Ln10);
	}

	static Complex!U log(U : CommunType!(T, F), F)(const T x, const F base) {
		return Complex.log(x) / Complex!F.log(base);
	}

	static Complex!U pow(U : CommunType!(T, F), F)(const T x, const F y) {
		if (x.isZero!T && y.isZero!T) return Complex!U.nan;
		else if(x.isZero!T) return Complex!U.zero;
		else if(y.isZero!T) return Complex!U.one;
		else return exp!U(Complex.log(x) * y);
	}

}

T norm(T, int m : -2)(const Complex!T z) {return inv(z.re.inv.sqr + z.im.inv.sqr);}
T norm(T, int m : -1)(const Complex!T z) {return inv(z.re.abs.inv + z.im.abs.inv);}
T norm(T, int m : 0)(const Complex!T z) {return sqrt(abs(z.re * z.im));}
T norm(T, int m : 1)(const Complex!T z) {return abs(z.re) + abs(z.im);}
T norm(T, int m : 2)(const Complex!T z) {return sqrt(sqr(z.re) + sqr(z.im));}
T norm(T, int m)(const Complex!T z) if(abs(m) > 2 && (m % 2)) {	return pow!(-m)(pow!m(z.re.abs) + pow!m(z.im.abs));}
T norm(T, int m)(const Complex!T z) if(abs(m) > 2 && !(m % 2)) {return pow!(-m)(pow!m(z.re) + pow!m(z.im));}
T normMax(T)(const Complex!T z) {return max(z.re.abs, z.im.abs);}
T normMin(T)(const Complex!T z) {return min(z.re.abs, z.im.abs);}

Complex!T conj(T)(const Complex!T z) {
	return Complex!T(z.re, -z.im);
}

T sqAbs(T)(const Complex!T z) {
	return numerics.sqr!T(z.re) + numerics.sqr!T(z.im);
}

T abs(T)(const Complex!T z) {
	return sqrt!T(sqAbs(z));
}

Complex!T sqr(T)(const Complex!T z) { return Complex!T(sqr(z.re) - sqr(z.im), 2 * z.re * z.im);}

Complex!T expi(T)(const T x) {return Complex!T(cos(x), sin(x));}

Complex!T expi(T)(const Complex!T z) { return expi(z.re) * exp(-z.im);}

Complex!T exp(T)(const Complex!T z) { return exp(z.re) * expi(z.im);}

Complex!T log(T)(const Complex!T z) { return z.isZero ? Complex!T.nan : Complex!T(z.abs.log, z.arg);}

Complex!T log(T)(const Complex!T z, T base) {
	return log(z) / Complex.log(base);
}

Complex!T log(U : CommonType!(T, F), T, F)(const Complex!T z, const F base) {
	return log(z) / Complex!F.log(base);
}

Complex!T log(T)(const Complex!T z, const Complex!T base) {
	return log(z) / log(base);
}

Complex!U log(U : CommonType!(T, F), T, F)(const Complex!T z, const Complex!F base) {
	return log(z) / log(base);
}

Complex!T inv(T)(const Complex!T z) {return conj(z) / sqAbs(z);}

auto polar(F1, F2)(const F1 ρ, const F2 θ) {
	return ρ * expi(θ);
}

Complex!T complex(F1, F2, T : CommonType!(F1, F2))(const F1 r, const F2 i) {
	return Complex!T(T(r), T(i));
}

T arg(T)(const Complex!T z) { return atan2!T(z.im, z.re);}

Complex!T sqrt(T)(const Complex!T z) {
	if(z.isZero) return z;
	else if(im.isZero!T) return re > 0 ? Complex!T(sqrt(z.re), 0) : Complex!T(0, sqrt(-z.re));
	else if(re.isZero!T) return Complex!T(1, sign(z.im)) * z.im.abs.sqrt / Sqrt2;
	else {
		T a = abs(z);
		return Complex!T(sqrt(a + z.re), sqrt(a - z.re) * sign(z.im)) / Sqrt2;
	}
}

Complex!T cbrt(T)(const Complex!T z) { return polar(cbrt(abs(z)), arg(z) / 3);}

Complex!T sin(T)(const Complex!T z) {
	Complex!T expiz = expi(z);
	return (sqr(expiz) - 1) / (2 * expiz).i;
}

Complex!T cos(T)(const Complex!T z) {
	Complex!T expiz = expi(z);
	return (sqr(expiz) + 1) / (2 * expiz);
}

Complex!T tan(T)(const Complex!T z) {
	Complex!T exp2iz = expi(2 * z);
	return (exp2iz - 1)/(exp2iz + 1).i;
}

Complex!T sinh(T)(const Complex!T z) {
	Complex!T expz = exp(z);
	return (sqr(expz) - 1) / (2 * expz);
}

Complex!T cosh(T)(const Complex!T z) {
	Complex!T expz = exp(z);
	return (sqr(expz) + 1) / (2 * expz);
}

Complex!T tanh(T)(const Complex!T z) {
	Complex!T exp2z = exp(2 * z);
	return (exp2z - 1)/(exp2z + 1);
}

Complex!T asin(T)(const Complex!T z) {
	return -log(z.i + sqrt(1 - sqr(z))).i;
}

Complex!T acos(T)(const Complex!T z) {
	return T(Pi) / 2 + log(z.i + sqrt(1 - sqr(z))).i;
}

Complex!T atan(T)(const Complex!T z) {
	return (log(1 - z.i) - log(1 + z.i)).i / 2;
}

Complex!T asinh(T)(const Complex!T z) {
	return log(z + sqrt(sqr(z) + 1));
}

Complex!T acosh(T)(const Complex!T z) {
	return log(z + sqrt(z - 1) * sqrt(z + 1));
}

Complex!T atanh(T)(const Complex!T z) {
	return (log(1 + z) - log(1 - z)) / 2;
}


mixin Functions!cfloat;
mixin Functions!cdouble;
mixin Functions!creal;
