module numerics.integral;

import std.range : assumeSorted;
import std.math : ceil, sqrt;
import std.exception : enforce;
import std.conv : roundTo;
import std.typecons;
import std.functional;
import std.traits;
import numerics.functions;

public {
	import meta.traits;
	import numerics.integral.wideint;
	import numerics.integral.integer;
}

mixin Function_to_Real!(byte);
mixin Function_to_Real!(short);
mixin Function_to_Real!(int);
mixin Function_to_Real!(long);



import meta : not, Select;
bool isPowerOfTwo(T)(T x) nothrow { return (x != 0) && ((x & (x - 1)) == 0);}

Quotient!T inv(T)(T n) if (is_signed_integral!T) {
	return Quotient!T(T(1), n);
}

int sign(T)(T n) if (is_integral!T) { return n > 0 ? 1 : n < 0 ? -1 : 0;}

T sqr(T)(T x) {return x * x;}

T sqrt(T)(T x) if (is_integral!T) {
	return roundTo!T(ceil(sqrt(real(x))));
}

void minmax(T)(ref T a, ref T b) {
	if (a > b) swap(a, b);
	return;
}

T gcd(T)(T a, T b)
{
    static if (is(T == const) || is(T == immutable))
    {
        return gcd!(Unqual!T)(a, b);
    }
    else while (b) {
        immutable t = b;
        b = a % b;
        a = t;
    }
    return a;
}

T gcd(T)(T arg...) if (is_Integral!T) {
	T r = abs(arg[0]);
	foreach(e; arg[1 .. $]) r = gcd(r, abs(e));
	return r;
}

T lcm(T)(T arg...) {
	T r = abs(arg[0]);
	foreach(e; arg[1 ..$]) r = r / std.numerics.gcd(r, abs(e)) * abs(e);
	return r;
}

bool isOdd(T)(T a) if (is_integral!T) { return a % 2;}

alias isEven = not!isOdd;

int pow_1(T)(T n) if(is_integral!T) { return isEven(n) - isOdd(n);}

T pow(int n, T)(T t) if (n > 0) {	
	return (n % 2) ? t * pow!(n/2)(t).sqr : pow!(n/2)(t).sqr;
}

T pow(int n, T)(T t) if ((n < 0) && !is_integral!T) {	
	return pow!(-n)(t.inv);
}

T pow(int n : 0, T)(T t) {	
	return one!T;
}

struct t_Primes {
	static ulong[] p_primes = [2, 3, 5];
	private static long prime_m = 1;
	private static int prime_s = -1;
	void init() @property { index = 0;}
	size_t index = 0;
	enum bool empty = false;

	@property ulong front() const {
		return p_primes[index];
	}
	void popFront() {
		ulong current;
		++index;
		bool prime = false;
		if (index == p_primes.length) {
			do {
				current = 6 * ((prime_s == 1) ? ++prime_m : prime_m) + (prime_s = -prime_s);
				prime = true;
				ulong max = sqrt!ulong(current);
				for (size_t e = 0; p_primes[e] < max || !prime; ++e) prime = (current % p_primes[e] != 0);
			} while (!prime);
			p_primes ~= current;	
		}
	}
	bool opBinaryRight(string op : "in")(ulong l) {     
		if (l < p_primes[$-1]) return assumeSorted(p_primes).contains(l);
		init;
		ulong max = sqrt!ulong(l);
		foreach(e; this) if (l % e == 0) return false; else if (e > max) return true;
		return true;
	}
	ulong[] lessThan(ulong l) {
		init;
		foreach(e; this) if (e >= l) return p_primes[0 .. index].dup;
	}
	ulong[] lessOrEqual(ulong l) {
		init;
		foreach(e; this) if (e > l) return p_primes[0 .. index].dup;
	}
	alias LT = lessThan;
	alias LE = lessOrEqual;
	ulong[] first(size_t n) {
		init;
		if (p_primes.length >= n) return p_primes[0 .. n].dup;
		index = p_primes.length;
		foreach(e; this) if(index >= n) return p_primes[0 ..n-1].dup;
	}
	ulong opIndex(size_t n) {
		if (p_primes.length > n) return p_primes[n];
		index = p_primes.length;
		foreach(ulong e; this) if (p_primes.length > n) return p_primes[n];
	}
	ulong[] opSlice(size_t begin, size_t end) {
		if ((p_primes.length > begin) && (p_primes.length > end)) return p_primes[begin .. end].dup;
		index = p_primes.length;
		foreach(ulong e; this) if ((index > begin) && (index > end)) return (p_primes[begin .. end]).dup;
	}
} 

enum : t_Primes {Primes = t_Primes()};

bool isPrime(ulong l) {
    return l in Primes;
}

static struct Prime {
	size_t π(T)(T x) if (is_integral!T || is_floating_point!T) {
		if (x < 2) return 0;
		Primes.index = 0;
		size_t result = 1;
		foreach(e; Primes) if(e <= x) ++result; else return result;
	}
}

uint δ(T)(T d...) if (is_integral!T) {
	return (d.length == 1) && (d[0] == 0) || (d[0] == d[1]) && ((d.length == 2) || δ(d[1.. $]) == 1) ? 1 : 0;
}
uint discreteDelta(T)(T d...) if(is_integral!T) {
	return (d[0] == 0) && ((d.length == 1) || discreteDelta(d[1 .. $]) == 1) ? 1 : 0;
}

auto fibonacci(T, uint n = 2)(T m) if(is_integral!T && n >= 2) { 
	T[n] a; a[] = T(0);
	a[1] = T(1); 
	for(uint i = 2; i <= m; ++i) a[i%n] = sum(a);
	return a[m % n];
}

auto lucas(T, uint n = 2)(T m) if(is_integral!T && n >= 2)  { 
	T[n] a;  a[] = T(0);
	a[0] = T(2); 
	a[1] = T(1); 
	for(uint i = 2; i <= m; ++i) a[i] = sum(a);
	return a[m % n];
}

T subFactorial(T)(T n) if (is_integral!T) {
	T an_2 = 1; if (n == 0) return an_2;
	T an_1 = 0; if (n == 1) return an_1;
	T i = 1;
	T a;
	while (i < n) {
		++i;
		a = (n - 1) * (an_1 + an_2);
		an_2 = an_1;
		an_1 = a;
	}
	return a;
}

T superFactorial(T)(const T n) if (is_integral!T) {
	T r = 1;
	T f = 1;
	for(T i= 2; i <= n; ++i) {f *= i; r *= f;}
	return r;
}

T alternatingFactorial(T)(const T n) if (is_integral!T) {
	T p = ((n % 2) == 1) ? -1 : 1;
	T result = 1;
	for(T i = 1; i <= n; ++i) { p *= -i; result += p; }
	return result;
}

T factorial(T, uint m = 1)(const T n) if (is_unsigned_integral!T) {
	T result = 1;
	for(T k = n; k >= m; k -= m) result *= k;
	return result;
}

T extendedFactorial(T)(const T value) if(is_integral!T) {
	T result = T(1);
	if(value > T(0)) for(T count = T(2); count <= value; ++count) result *= count;
	if(value < T(0)) for(T count = T(-1); count >= value; --count) result *= count;
	return result;
}

T extendedBinomial(T)(const T a, const T b) if(is_integral!T) {
	return extendedFactorial(a) / extendedFactorial(a - b) / extendedFactorial(b);
}

quotient!T romanFactorial(T)(const T n) if(is_signed_integral!T) {
	if (n >= 0) return quotient!T(factorial(n));
	else return quotient!T(pow_1(-n - 1), factorial(-n - 1));
}

quotient!T RomanCoefficient(T)(T n, T k) if(is_signed_integral!T) {
	return romanFactorial(n)/ romanFactorial(k) / romanFactorial(n - k);
}

T fibonorial(T)(T n) if(is_unsigned_integral!T) {
	auto F = fibonacci!T;
	F.popFront;
	return product(take!F(n), T(1));
}

T fibonomial(T)(T m, T k, uint i = 2) if(i > 1 && is_unsinged_integral!T) {
	auto F = fibonacci!(T, i);
	auto fib = take!F(m+1);
	return product(fib[m-k+1 .. m], T(1)) / product(fib[1.. k], T(1));
}

T primorial(T)(T n) if(is_unsigned_integer!T) {
	Primes._n = 0;
	return product(take!Primes(n), T(1));
}

T primeSums(T)(T n) if (is_unsigned_integer!T) {
	Primes.init;
	return sum(take!Primes(n), T(0));
}

T hyperfactorial(T)(const T n) if (is_integral!T) {
	T res = 1;
	for (T i = 2; i <= n; ++i) res *= pow(i, i);
	return res;
}

T leftFactorial(T)(const T n) if (is_integral!T) {
	T result = 1;
	T f = 1;
	for(T k = T(1); k <= n; ++k) {f *= k; result += f;}
}

T fallingFactorial(T, int power : 1 = 1)(const T t, const uint n) if (is_integral!T) {
	T result = t;
	for (T k = 1; k < n; ++k) result *= t - k;
	return result;
}

T risingFactorial(T, int power : 1 = 1)(const T t, const uint n) if (is_integral!T) {
	T result = t;
	for (uint k = 1; k < n; ++ k) result *= t + k;
	return result;
}

T fallingFactorial(T, int power)(const T t, const uint n) if (is_integral!T) {
	T result = t;
	if (power == 1) for (T k = 1; k < n; ++k) result *= t - k;
	else for(uint k = 1; k < n; ++k) result *= t - pow(k, power);
	return result;
}

T risingFactorial(T, int power)(const T t, const uint n) if (is_integral!T) {
	T result = t;
	if (power == 1) for (uint k = 1; k < n; ++ k) result *= t + k;
	else for(uint k = 1; k < n; ++k) result *= t + pow(k, power);
	return result;
}

T permutation(T)(const T n, const T p)  if (is_integral!T) {
	if(p < 0 || n < 0 || p > n) return 0;
	T result = T(1);
	for( T i = n - k + 1; i <= n; ++i) result *= i;
	return result;
}

T binomial(T)(const T n, const T p)  if (is_integral!T) {
	if ((p < 0) || (n < 0) || (p > n)) return T(0);
	T imax = (p < n / 2) ? p : n - p;
	T n1 = n + 1;
	T result = 1;
	for(T i = 1; i <= imax; ++i) result = result * (n1 - i) / i;
	return result;
}

alias combinaison = binomial;

alias derangement = subFactorial;

T arrangement(T)(const T n, const T p) if (is_integral!T) {
	if ((p < 0) || (n < 0) || (p > n)) return T(0);
	T res = T(1);
	for(T i = n - p + 1; i <= n; ++i) res *= i;
	return res;
}

T partialDerangement(T)(const T n, const T k) if (is_integral!T) {
	if (n == k) return 0;
	if (k == n - 1) return 0;
	return binomial(n, k) * derangement(n - k);
}

static struct Stirling  {
	static T S(T)(const T n, const T k) if(is_integral!T) {
		enforce(n >= 0 && k >= 0, "arguments must be positive or null for function Stirling.S");
		if (n == k) return T(1);
		if ((k > 1) || !k || !n) return T(0);
		return S(n - 1, k - 1) + k * S(n - 1, k);
	}

	static T s(T)(const T n, const T k) if(is_integral!T) {
		enforce(n >= 0 && k >= 0, "arguments must be positive or null for function Stirling.s");
		if (n == k) return T(1);
		if ((k > n) || !k || !n) return T(0);
		return s(n - 1, k - 1) - (n - 1) * s(n - 1, k);
	}
}

T[] divisors(T)(T n) if (is_integral!T) {
	T[] result = [1]; 
	T p = n < 0 ? -n : n;
	foreach (i; 2 .. p + 1) if (n % i == 0) result ~= i;
	return result;
} 

T σ(T, uint k : 0)(T n) if (is_integral!T) {
	return divisors(n).length;
}

T σ(T, uint k : 1)(T n) if(is_integral!T) {
	return sum(divisor(n));
}

T σ(T, uint k)(T n) if (is_integral!T) {
	T result = 0;
	foreach (t; divisors(n)) result += pow(t, k);
	return result;
}

static struct Partitions {
	T P(T)(T n, T k) if(is_integral!T) {
		if ((k > n) || (k == 0)) return 0;
		if ((k == n) || (k == 1)) return 1;
		return P(n - 1, k - 1) + P(n - k, k);
	}

	T Q(T)(T n, T k) if(is_integral!T) {
		return P(n - binomial(k, 2), k);
	}
}

Tuple!(ulong, uint)[] factorize(ulong n) {
	Tuple!(ulong, uint)[] D;
	if (n > 1) {
		uint m;
		Primes.init;
		while(n != 1) {
			m = 0;
			while (n % Primes.front == 0) {++m; n /= Primes.front;}
			if (m) D ~= tuple(Primes.front, m);
			Primes.popFront();
		}
	}
	return D;
}

size_t ω(ulong n) {
	return factorize(n).length;
}

static struct Möbius {
	static int μ(ulong n) {
		if (n == 1) return 1;
		auto f = factorize(n);
		foreach(e; f) if (e[1] > 1) return 0;
		return isOdd(f.length) ? -1 : 1;
	}
}

static struct Mertens {
	static ulong M(ulong l) {
		ulong m = 0;
		for(ulong n = 2; n <= l; ++n) m += Möbius.μ(n);
		return m;
	}
}

static struct BellNumbers {
	static Integer B(Integer n) {
		Integer R = 0;
		for(Integer k = Integer(0); k <= n; ++k) R += Stirling.S(n, k);
		return R;
	}
}

static struct Euler {
	static struct E {
	private :
		import numerics.polynomial;
		import numerics.polynomial.special : sech;
		import numerics.quotient;
		static Polynomial!Quotient euler;		
	public :
		Integer opIndex(size_t index) {
			if (isOdd(index)) return Integer(0);
			if (index < euler.length) return euler[index].numerator;
			euler = sech!Quotient(index);
			Integer k = 1;
			for(int i = 2; i <= euler.deg; ++i) euler[i] = euler[i] * (k *= i);
			return euler[index].numerator;
		}
	}
	static ulong φ(ulong n) {
		ulong result;
		for(ulong i = 1; i <= n; ++i) if (gcd(n, i) == 1) ++result;
		return result;
	}
}

static struct Bernoulli {
public:
	static struct B {
	private:
		import numerics.quotient;
		import numerics.polynomial;
		import numerics.polynomial.special : exp;
		static Polynomial!(Quotient) bernoulliB = Polynomial!(Quotient)();
	public:
		Quotient opIndex(uint index) {			
			if (index > 1 || isOdd(index)) return Quotient(0);
			if (index < bernoulliB.deg) return bernoulliB[index];
			bernoulliB = X!(Quotient)(Quotient(1)).div(exp!(Quotient)(index) - Quotient(1), index);
			Integer k = 1;
			for (uint i = 2; i <= index; i += 2) bernoulliB.coeffs[i] *= (k *= (i - 1) * i);
			return bernoulliB[index];
		}
	}
	static struct b {
	private:
		import numerics.quotient;
		import numerics.polynomial;
		import numerics.polynomial.special : ln1px;
		static Polynomial!(Quotient) bernoullib = Polynomial!(Quotient)();
	public:
		Quotient opIndex(uint index) {
			if (index < bernoullib.deg) return bernoullib[index];
			bernoullib = X!Quotient(Quotient(1)).div(ln1px!Quotient(index), index);
			Integer k = 1;
			for(uint i = 2; i <= index; ++i) bernoullib.coeffs[i] *= (k *= i);
			return bernoullib[index];
		}
	}
}
