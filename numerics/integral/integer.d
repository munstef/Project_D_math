module numerics.integral.integer;

import std.conv : ConvException, parse;
import std.exception;
import numerics.functions;

private import numerics.integral.biguintcore;
private import std.format : FormatSpec, FormatException;
private import meta;



alias Natural = integer!false;
alias Integer = integer!true;

struct integer(bool signed) {
private:

    BigUint data;     // integer adds signed arithmetic to BigUint.
    bool sign = false;

    static if(!signed) pure invariant() {
        if(sign) throw new Error("Underflow");
    }

	void negate() @safe pure nothrow @nogc {
        if (!data.isZero())
            sign = !sign;
    }

    bool isZero() pure const nothrow @nogc @safe {
        return data.isZero();
    }

    bool isNegative() pure const nothrow @nogc @safe {
        return sign;
    }

    void checkDivByZero() pure const nothrow @safe {
        if (isZero()) throw new Error("integer division by zero");
    }

public:

	T opCast(T : float)() {
		T res = 0.0f;
		for(size_t i = data.ulongLength() - 1; i >= 0; --i) res = res * ulong.max + data.peekUlong(i);
		return res * (-int(sign));
	}

	T opCast(T : double)() {
		T res = 0.0;
		for(size_t i = data.ulongLength() - 1; i >= 0; --i) res = res * ulong.max + data.peekUlong(i);
		return res * (-int(sign));
	}

	T opCast(T : real)() {
		T res = 0.0L;
		for(size_t i = data.ulongLength() - 1; i >= 0; --i) res = res * ulong.max + data.peekUlong(i);
		return res * (-int(sign));
	}

	alias self = typeof(this);

    this(T : const(char)[])(T s, string file = __FILE__, size_t line = __LINE__) pure {
		if (s.length == 0)
			throw new ConvException("Can't initialize integer with empty string", file, line);

		static if(signed) {
			bool neg = false;
			if (s[0] == '-') {
				neg = true;
				s = s[1..$];
			}
		}
		if (s[0] == '+') s = s[1..$];
		data = 0UL;
		bool ok;
		assert(isZero());
		if (s.length > 2 && (s[0..2] == "0x" || s[0..2] == "0X"))
		{
			ok = data.fromHexString(s[2..$]);
		} else {
			ok = data.fromDecimalString(s);
		}
		if (!ok)
			throw new ConvException("Invalid digit string", file, line);

		if (isZero())
			neg = false;
		static if(signed) sign = neg;
	}

    this(T)(T x) pure nothrow if (is_integral!T) {
        data = data.init; // @@@: Workaround for compiler bug
        opAssign(x);
    }

    this(integer x) pure nothrow  {
        opAssign(x);
    }
	this(integer!(!signed) x) pure nothrow {
        opAssign(x);
    }

    integer opAssign(T)(T x) pure nothrow if (is_integral!T) {
        data = cast(ulong)absUnsign(x);
        sign = (x < 0);
        return this;
    }

	integer opAssign(integer x) pure @nogc {
        data = x.data;
        sign = x.sign;
        return this;
    }

    integer opAssign(integer!(!signed) x) pure @nogc {
        data = x.data;
        sign = x.sign;
        return this;
    }

    integer opOpAssign(string op, T)(T y) pure nothrow
        if ((op=="+" || op=="-" || op=="*" || op=="/" || op=="%"
			 || op==">>" || op=="<<" || op=="^^" || op=="|" || op=="&" || op=="^") && is_integral!T) {
				 ulong u = absUnsign(y);

				 static if (op=="+")
				 {
					 data = BigUint.addOrSubInt(data, u, sign != (y<0), sign);
				 }
				 else static if (op=="-")
				 {
					 data = BigUint.addOrSubInt(data, u, sign == (y<0), sign);
				 }
				 else static if (op=="*")
				 {
					 if (y == 0) {
						 sign = false;
						 data = 0UL;
					 } else {
						 sign = ( sign != (y<0) );
						 data = BigUint.mulInt(data, u);
					 }
				 }
				 else static if (op=="/")
				 {
					 assert(y!=0, "Division by zero");
					 static if (T.sizeof <= uint.sizeof)
					 {
						 data = BigUint.divInt(data, cast(uint)u);
					 }
					 else
					 {
						 data = BigUint.divInt(data, u);
					 }
					 sign = data.isZero() ? false : sign ^ (y < 0);
				 }
				 else static if (op=="%")
				 {
					 assert(y!=0, "Division by zero");
					 static if (is(immutable(T) == immutable(long)) || is( immutable(T) == immutable(ulong) ))
					 {
						 this %= integer(y);
					 }
					 else
					 {
						 data = cast(ulong)BigUint.modInt(data, cast(uint)u);
						 if (data.isZero())
							 sign = false;
					 }
				 }
				 else static if (op==">>" || op=="<<")
				 {
					 if (y == 0)
						 return this;
					 else if ((y > 0) == (op=="<<"))
					 {
						 data = data.opShl(u);
					 } else
					 {
						 data = data.opShr(u);
						 if (data.isZero())
							 sign = false;
					 }
				 }
				 else static if (op=="^^")
				 {
					 sign = (y & 1) ? sign : false;
					 data = BigUint.pow(data, u);
				 }
				 else static if (op=="|" || op=="&" || op=="^")
				 {
					 integer b = y;
					 opOpAssign!op(b);
				 }
				 else static assert(0, "integer " ~ op[0..$-1] ~ "= " ~ T.stringof ~ " is not supported");
				 return this;
			 }

    integer opOpAssign(string op, bool s)(integer!s y) pure nothrow
        if (op=="+" || op== "-" || op=="*" || op=="|" || op=="&" || op=="^" || op=="/" || op=="%") {
			static if (op == "+")
			{
				data = BigUint.addOrSub(data, y.data, sign != y.sign, &sign);
			}
			else static if (op == "-")
			{
				data = BigUint.addOrSub(data, y.data, sign == y.sign, &sign);
			}
			else static if (op == "*")
			{
				data = BigUint.mul(data, y.data);
				sign = isZero() ? false : sign ^ y.sign;
			}
			else static if (op == "/")
			{
				y.checkDivByZero();
				if (!isZero())
				{
					data = BigUint.div(data, y.data);
					sign = isZero() ? false : sign ^ y.sign;
				}
			}
			else static if (op == "%")
			{
				y.checkDivByZero();
				if (!isZero())
				{
					data = BigUint.mod(data, y.data);
					if (isZero())
						sign = false;
				}
			}
			else static if (op == "|" || op == "&" || op == "^")
			{
				data = BigUint.bitwiseOp!op(data, y.data, sign, y.sign, sign);
			}
			else static assert(0, "integer " ~ op[0..$-1] ~ "= " ~
							   T.stringof ~ " is not supported");
			return this;
		}

    integer opBinary(string op, bool s)(integer!s y) pure nothrow const
        if (op=="+" || op == "*" || op=="-" || op=="|" || op=="&" || op=="^" || op=="/" || op=="%") {
			integer r = this;
			return r.opOpAssign!(op)(y);
		}

	integer opBinaryRight(string op)(integer!(!signed) y) pure nothrow const
        if (op=="+" || op == "*" || op=="-" || op=="|" || op=="&" || op=="^" || op=="/" || op=="%") {
			static if (op=="+" || op == "*" || "|" || "&" || "^") return opBinary(y);
			static if(op == "-") return y - this;
			static if(op == "/") return y / this;
			static if(op == "%") return y % this;
		}

    integer opBinary(string op, T)(T y) pure nothrow const
        if ((op=="+" || op == "*" || op=="-" || op=="/" || op=="|" || op=="&" ||
			 op=="^"|| op==">>" || op=="<<" || op=="^^")
            && is_integral!T) {
				integer r = this;
				return r.opOpAssign!(op)(y);
			}

    auto opBinary(string op, T)(T y) pure nothrow const if (op == "%" && is_integral!T) {
		assert(y!=0);
		static if (is(Unqual!T == long) || is(Unqual!T == ulong)) {
			auto r = this % integer(y);
			static if (is(Unqual!T == long)) {
				return r.toLong();
			} else return r;
		} else {
			uint u = absUnsign(y);
			int rem = BigUint.modInt(data, u);
			return sign ? -rem : rem;
		}
	}

    integer opBinaryRight(string op, T)(T y) pure nothrow const
        if ((op=="+" || op=="*" || op=="|" || op=="&" || op=="^") && is_integral!T) {
			return opBinary!op(y);
		}

    integer opBinaryRight(string op, T)(T y) pure nothrow const
        if (op == "-" && is_integral!T) {
			ulong u = absUnsign(y);
			integer r;
			static if (op == "-")
			{
				r.sign = sign;
				r.data = BigUint.addOrSubInt(data, u, sign == (y<0), r.sign);
				r.negate();
			}
			return r;
		}

    T opBinaryRight(string op, T)(T x) pure nothrow const
        if ((op=="%" || op=="/") && is_integral!T) {
			checkDivByZero();
			static if (op == "%") {
				if (data.ulongLength > 1)
					return x;
				ulong u = absUnsign(x);
				ulong rem = u % data.peekUlong(0);
				return cast(T)((x<0) ? -rem : rem);
			} else static if (op == "/") {
				if (data.ulongLength > 1) return 0;
				return cast(T)(x / data.peekUlong(0));
			}
		}

    integer opUnary(string op)() pure nothrow const if (op=="+" || op=="-" || op=="~") {
		static if (op == "-") {
            integer r = this;
            r.negate();
            return r;
        } else static if (op == "~" && signed) {
            return -(this+1);
        } else static if(op == "~") {
			auto r = this;
			r.data = ~r.data;
			return r;
		}
        else static if (op == "+") return this;
    }

    integer opUnary(string op)() pure nothrow if (op=="++" || op=="--") {
        static if (op == "++") {
            data = BigUint.addOrSubInt(data, 1UL, sign, sign);
            return this;
        } else static if (op == "--") {
            data = BigUint.addOrSubInt(data, 1UL, !sign, sign);
            return this;
        }
    }

    bool opEquals()(auto ref const integer y) const pure @nogc {
		return sign == y.sign && y.data == data;
    }

    bool opEquals(T)(T y) const pure nothrow @nogc if (is_integral!T) {
        if (sign != (y<0)) return 0;
        return data.opEquals(cast(ulong)absUnsign(y));
    }

    T opCast(T:bool)() pure nothrow @nogc const {
        return !isZero();
    }

	T opCast(T : integer!(!signed))(){
		T result = this;
		return result;
	}		

	T opCast(T : self)() pure nothrow @nogc const {
        return this;
    }

	T opCast(T:ulong)() /*pure*/ const {
		import std.string : format; import std.conv : ConvOverflowException;
		if (isUnsigned!T && sign) throw ConOverfowException("%d cannot be represented as a %s".format(this, T.stringof));
		else
			if (data.ulongLength == 1)
			{
				ulong l = data.peekUlong(0);
				if (isUnsigned!T || !sign)
				{
					if (l <= T.max)
						return cast(T)l;
				}
				else
				{
					if (l <= ulong(T.max)+1)
						return cast(T)-long(l); // -long.min==long.min
				}
			}	
		throw new ConvOverflowException("BigInt(%d) cannot be represented as a %s".format(this, T.stringof));
	}

	T opCast(T:double)() if(is_floating_point!T) {
		return parse!double(toDecimalString(this));
	}

    int opCmp(T)(T y) pure nothrow @nogc const if (is_integral!T)  {
        if (sign != (y<0)) return sign ? -1 : 1;
        int cmp = data.opCmp(cast(ulong)absUnsign(y));
        return sign? -cmp: cmp;
    }

    int opCmp(const integer y) pure nothrow @nogc const {
        if (sign!=y.sign) return sign ? -1 : 1;
        int cmp = data.opCmp(y.data);
        return sign? -cmp: cmp;
    }

    int opCmp(const integer!(!signed) y) pure nothrow @nogc const {
        if (sign!=y.sign) return sign ? -1 : 1;
        int cmp = data.opCmp(y.data);
        return sign? -cmp: cmp;
    }

    long toLong() @safe pure nothrow const @nogc {
        return (sign ? -1 : 1) *
			(data.ulongLength == 1  && (data.peekUlong(0) <= sign+cast(ulong)(long.max)) // 1+long.max = |long.min|
			 ? cast(long)(data.peekUlong(0))
				 : long.max);
    }

    int toInt() @safe pure nothrow @nogc const {
        return (sign ? -1 : 1) *
			(data.uintLength == 1  && (data.peekUint(0) <= sign+cast(uint)(int.max)) // 1+int.max = |int.min|
			 ? cast(int)(data.peekUint(0))
				 : int.max);
    }

    @property size_t uintLength() @safe pure nothrow @nogc const {
        return data.uintLength;
    }

    @property size_t ulongLength() @safe pure nothrow @nogc const {
        return data.ulongLength;
    }

    void toString(scope void delegate(const (char)[]) sink, string formatString) const  {
        auto f = FormatSpec!char(formatString);
        f.writeUpToNextSpec(sink);
        toString(sink, f);
    }

    void toString(scope void delegate(const(char)[]) sink, ref FormatSpec!char f) const {
        auto hex = (f.spec == 'x' || f.spec == 'X');
        if (!(f.spec == 's' || f.spec == 'd' || hex))
            throw new FormatException("Format specifier not understood: %" ~ f.spec);
        char[] buff;
        if (f.spec == 'X') {
            buff = data.toHexString(0, '_', 0, f.flZero ? '0' : ' ', LetterCase.upper);
        } else if (f.spec == 'x') {
            buff = data.toHexString(0, '_', 0, f.flZero ? '0' : ' ', LetterCase.lower);
        }
        else  {
            buff = data.toDecimalString(0);
        }
        assert(buff.length > 0);
        char signChar = isNegative() ? '-' : 0;
        auto minw = buff.length + (signChar ? 1 : 0);
        if (!hex && !signChar && (f.width == 0 || minw < f.width)) {
            if (f.flPlus) {
                signChar = '+';
                ++minw;
            } else if (f.flSpace) {
                signChar = ' ';
                ++minw;
            }
        }

        auto maxw = minw < f.width ? f.width : minw;
        auto difw = maxw - minw;

        if (!f.flDash && !f.flZero)
            foreach (i; 0 .. difw)
                sink(" ");

        if (signChar) sink((&signChar)[0..1]);

        if (!f.flDash && f.flZero)
            foreach (i; 0 .. difw)
                sink("0");

        sink(buff);
        if (f.flDash)
            foreach (i; 0 .. difw)
                sink(" ");
    }

    size_t toHash() const @safe nothrow {
        return data.toHash() + sign;
    }

	static if(!signed) enum integer!false min = 0;

}

Integer!signed abs(bool signed)(integer!signed n) {
	static if(!signed) return n;
	return (n < 0) ? -n : n;
}


string toDecimalString(bool s)(const(integer!s) x) {
    string outbuff="";
    void sink(const(char)[] x) { outbuff ~= x; }
    x.toString(&sink, "%d");
    return outbuff;
}

string toHex(bool s)(const(integer!s) x) {
    string outbuff="";
    void sink(const(char)[] s) { outbuff ~= s; }
    x.toString(&sink, "%X");
    return outbuff;
}


Unsigned!T absUnsign(T)(T x) if (is_integral!T) {
    static if (is_signed!T) {
        import std.conv;
        return unsigned((x < 0) ? -x : x);
    } else return x;
}

mixin Function_to_Real!(Integer);

