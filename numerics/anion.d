module numerics.anion;
import numerics;

/**
les anions sont représenté par leur degré 'n'
pour n = 0 => réels  ℝ^(2^0)
pour n = 1 => complexes ℝ^(2^1)
pour n = 2 => quaternion = ℝ^(2^2)
pour n = 4 => octonions = ℝ^(2^3)
...
n = 15 
**/

struct Anion(uint n : 0, T) if (is_floating_point!T) {

	T re;

	alias re this;

	T im() const @property {return zero!T;}

	size_t opDollar()() const {
		return 1;
	}

	T opIndex()(size_t index) const {
		assert(index < opDollar(), "index must be 0");
		return re;
	}

	T[] opIndex()() const {
		return [re];
	}

	ref T opIndexAssign()(T r, size_t index) const {
		assert(!index, "index must be 0");
		return re = r;
	}

	ref Anion opIndexAssign()(T r) const {
		assert(!index, "index must be 0");
		re = r;
		return this;
	}

	T opIndexOpAssign(string op)(T r, size_t index) if(op == "+" || op == "-" || op == "*" || op == "/"){
		assert(index < opDollar(), "index must be 0");
		static if(op == "+") return re += r;
		static if(op == "-") return re -= r;
		static if(op == "*") return re *= r;
		static if(op == "/") return re /= r;
	}

	T opIndexUnary(string op)(size_t index) if(op == "++" || op == "--" || op == "+" || op == "-"){
		assert(index < opDollar(), "index must be 0");
		static if(op == "++") return ++re;
		static if(op == "--") return --re;
		static if(op == "-") return -re;
		static if(op == "+") return re;
	}

	bool opIndexUnary(string op : "!")(size_t index) {
		assert(index < opDollar(), "index must be 0");
		return !re;
	}

	ref Anion opIndexOpAssign(string op)(T r) if(op == "+" || op == "-" || op == "*" || op == "/"){
		assert(index < opDollar(), "index must be 0");
		static if(op == "+") re += r;
		static if(op == "-") re -= r;
		static if(op == "*") re *= r;
		static if(op == "/") re /= r;
		return this;
	}

	T opIndexUnary(string op)() if(op == "++" || op == "--" || op == "+" || op == "-"){
		assert(index < opDollar(), "index must be 0");
		static if(op == "++") return ++re;
		static if(op == "--") return --re;
		static if(op == "-") return -re;
		static if(op == "+") return re;
	}

	bool opIndexUnary(string op : "!")() {
		assert(index < opDollar(), "index must be 0");
		return !re;
	}


	this(F)(const F x) if(is_floating_point!F) {
		re = T(x);
	}

	this(T[] arg...){ assert(arg.length <= 1); if(arg.length != 0) re = arg[0];}

	this(const Anion z) { re = z.re;}

	this(N)(const N x) if(is_integral!N) {
		re = cast(T)x;
	}
	this(F)(const Real!F x) if(is_floating_point!F) {
		re = cast(T) x.re;
	}

	Anion opUnary(string op = "-")() const {
		return Anion(-re);
	}

	Anion opUnary(string op = "+")() const {
		return this;
	}

	bool opEqual(T x) const {
		return re == x;
	}
	bool opEqual(const Anion a) {
		return re == a.re;
	}

	bool opEqual(n)(const Anion!(n, T) a) if(n) { return a.left == re && a.right == 0;}

	int opComp(T x) const {
		return (re > x) - (re < x);
	}

	int opComp(const Anion a) const {
		return (re > a.re) - (re < a.re);
	}

	Anion opBinary(string op)(const T x) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion(re + x);
		static if(op == "-") return Anion(re - x);
		static if(op == "*") return Anion(re * x);
		static if(op == "/") return Anion(re / x);
	}

	Anion opBinary(string op)(const Anion z) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion(re + z.re);
		static if(op == "-") return Anion(re - z.re);
		static if(op == "*") return Anion(re * z.re);
		static if(op == "/") return Anion(re / z.re);
	}

	Anion!(1, T) opBinary(string op)(const T x) const if(op == "^^") {
		return exp(log(this) * x);
	}

	Anion!(1, T) opBinary(string op)(const Anion a) const if(op == "^^") {
		return exp(log(a) * this);
	}

	Anion opBinaryRight(string op)(const T x) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion(x + re);
		static if(op == "-") return Anion(x - re);
		static if(op == "*") return Anion(x * re);
		static if(op == "/") return Anion(x / re);
	}

	Complex!T opBinaryRight(string op :"^^")(const T x) const {
		return exp(log(Real!T(x)) * this);
	}

	ref Anion opAssign()(const T x) {
		re = x;
		return this;
	}

	ref Anion opAssign()(const Anion a) {
		re = a.re;
		return this;
	}

	ref Anion opOpAssign(string op)(const T x) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") re += x;
		static if(op == "-") re -= x;
		static if(op == "*") {re *= x;}
		static if(op == "/") {re /= x;}
		return this;
	}

	ref Anion opOpAssign(string op)(const Anion z) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") re += z.re;
		static if(op == "-") re -= z.re;
		static if(op == "*") re *= z.re;
		static if(op == "/") re /= z.re;
		return this;
	}

	Anion!(1, T) i() const @property { return Anion!(1, T)(zero!T, re);}

	T dot(const Anion!(0, T) z) { return re * z.re;}

	B opCast(B : bool)() { return re != 0;}

	T opCast(T)() {return re;}

	T dot(const Anion z) const {
		return re * z.re;
	}

	Anion e(size_t index) {
		if(size_t == 0) return this;
		assert(index < 1 << n);
	}
}

alias Real(T) = Anion!(0, T);

T norm2(U : Anion!(0, T), T, int m)(const U a) if(m) { return a.re * a.re;}
T norm2(U : Anion!(0, T), T, int m : 2 = 2)(const U a) {return pow!m(abs(a.re));}
T norm(U : Anion!(0, T), T, int m)(const U a) { return pow(norm2!m(a), one!T / m);}
T norm(U : Anion!(0, T), T, int m : 2 = 2)(const U a) { return abs(a.re);}

Real!T conj(U : Real!T, T)(const U a) {
	return a;
}

T sqAbs(U : Anion!(0, T), T)(const U a) {
	return norm2(a);
}

T abs(U : Anion!(0, T), T)(const U a) {
	return abs(a.re);
}

Real!T sign(U : Real!T, T)(const U a) {
	return sign!T(a.re);
}

Real!T arg(U : Real!T, T)(const U a) {
	return (a > 0) ? zero!T : (a < 0) ? cast(T) Pi : T.nan;
}

Anion!(1, T) argx(U : Real!T, T)(const U a) {
	return Anion!(1, T)(zero!T, arg(a));
}

Anion!(1, T) sqrt(U : Real!T, T)(const U a) {
	if(z.re > 0) return Anion!(1, T)(std.math.sqrt(z.re), zero!T);
	if(z.re < 0) return Anion!(1, T)(zero!T, std.math.sqrt(-z.re));
	return zero!(Anion!(1, T));
}

Real!T sqr(U : Real!T, T)(const U a) {
	return a.re * a.re;
}

Real!T exp(U : Real!T, T)(const U a) {
	return exp(a.re);
}

Anion!(1, T) log(U : Real!T, T)(const U a) {
	return !a.re ? Anion!(1, T)(log(a.re), arg(a)) : Anion!(1, T)(T.nan, T.nan);
}

Real!T sin(U : Real!T, T)(const U a) {
	return sin(a.re);
}

Real!T cos(U : Real!T, T)(const U a) {
	return cos(x.re);
}

Real!T tan(U : Real!T, T)(const U a) {
	return tan(x.re);
}

Real!T asin(U : Real!T, T)(const U a) {
	return asin(a.re);
}

Real!T acos(U : Real!T, T)(const U a) {
	return acos(x.re);
}

Real!T atan(U : Real!T, T)(const U a) {
	return atan(x.re);
}

Real!T sinh(U : Real!T, T)(const U a) {
	return sinh(a.re);
}

Real!T cosh(U : Real!T, T)(const U a) {
	return cosh(x.re);
}

Real!T tanh(U : Real!T, T)(const U a) {
	return tanh(x.re);
}

Real!T asinh(U : Real!T, T)(const U a) {
	return asinh(a.re);
}

Real!T acosh(U : Real!T, T)(const U a) {
	return acosh(x.re);
}

Real!T atanh(U : Real!T, T)(const U a) {
	return atanh(x.re);
}

T dot(T)(const Real!T a, const Real!T b) {return a.dot(b);}

string format(T)(in char[] fmt, const Real!T a) {return format(fmt, a.re);}

//--------------------------------------------------------------------------------------------

struct Anion(uint n : 1, T) if (is_floating_point!T) {	

	numerics.Complex!T value;

	alias value this;

	this(T x, T y = zero!T) { re = x; im = y;}	

	this(const Anion!(0, T) x, const Anion!(0, T) y = zero!(Anion!(0, T))) { re = x.re; im = y.re;}

	this(const typeof(value) z) { value = z;} // Complex!T

	this(const typeof(value.value) z) { value.value = z;} // cfloat, cdouble or creal

	U opCast(U : typeof(value.value))() const { // cfloat, cdouble or creal
		return value.value;
	}

	U opCast(U : typeof(value))() const { // Complex!T
		return value;
	}

	U opCast(U : bool)() const {
		return re != 0 || im != 0;
	}

	size_t opDollar()() const {
		return 2;
	}

	T opIndex()(size_t index) { assert(index < 2, "index is out of range");
		return (index) ? im : re;
	}

	T[] opIndex()() const {
		return [re, im];
	}

	ref T opIndexAssign()(T r, size_t index) { assert(index < 2, "index is out of range");
		return (index) ? re = r : im = r;
	}

	ref Anion opIndexAssign()(T r) {
		re = r; im = r;
		return this;
	}

	ref T opIndexOpAssign(string op)(T r, size_t index) if(op == "+" || op == "-" || op == "*" || op == "/") { assert(index < 2, "index is out of range");
		static if(op =="+") return (index) ? im += r : re += r;
		static if(op =="-") return (index) ? im -= r : re -= r;
		static if(op =="*") return (index) ? im *= r : re *= r;
		static if(op =="/") return (index) ? im /= r : re /= r;
	}

	ref T opIndexUnary(string op)(size_t index) if(op == "++" || op == "--") { assert(index < 2, "index is out of range");
		static if(op == "++") return (index) ? ++im: ++re;
		static if(op == "--") return (index) ? --im : --re;
	}

	T opIndexUnary(string op)(size_t index) if(op == "-" || op == "+") { assert(index < 2, "index is out of range");
		static if(op == "+") return (index) ? im : re;
		static if(op == "-") return (index) ? -im : -re;
	}

	bool opIndexUnary(string op : "!")(size_t index) { assert(index < 2, "index is out of range");
		return (index) ? !im : !re;
	}

	ref Anion opIndexOpAssign(string op)(T r) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op =="+") { re += r; im += r;}
		static if(op =="-") { re -= r; im -= r;}
		static if(op =="*") { re *= r; im *= r;}
		static if(op =="/") { re /= r; im /= r;}
		return this;
	}

	ref Anion opIndexUnary(string op)() if(op == "++" || op == "--") {
		static if(op == "++") {++re; ++im;}
		static if(op == "--") {--re; --im;}
		return this;
	}
	Anion opIndexUnary(string op)() if(op == "-" || op == "+") { 
		return opUnary!op();
	}

	bool opIndexUnary(string op : "!")() {
		return !re && !im;
	}

	// left & right

	Anion!(0, T) left() @property {return Real!T(re);}
	Anion!(0, T) right() @property {return Real!T(im);}

	void left(Real!T v) @property {re = v.re;}
	void right(Real!T v) @property {im = v.re;}



	Anion e(size_t index) 
	in { assert(index <= 1);}
	body {
		return (index) ? this.i : this;
	}
	Anion i() @property { return Anion(-im, re);}

	Anion opUnary(string op :"+")(const Anion a) const {
		return a;
	}
	Anion opUnary(string op : "-")(const Anion a) const {
		return Anion(-a.re, -a.im);
	}

	ref Anion opUnary(string op)() if(op == "++" || op == "--") {
		if(op == "++") ++re; else --re;
		return this;
	}

	Anion opBinary(string op)(const T x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion!(1, T)(a.re + x, a.im);
		static if(op == "-") return Anion!(1, T)(a.re - x, a.im);
		static if(op == "*") return Anion!(1, T)(a.re * x, a.im * x);
		static if(op == "/") return Anion!(1, T)(a.re / x, a.im / x);
	}

	Anion opBinaryRight(string op)(const T x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion!(1, T)(x + a.re, a.im);
		static if(op == "-") return Anion!(1, T)(x - a.re, -a.im);
		static if(op == "*") return Anion!(1, T)(x * a.re, x * a.im);
		static if(op == "/") return  x * conj(this) / norm2;
	}

	ref Anion opOpAssign(string op)(const T x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") re += x;
		static if(op == "-") re -= x;
		static if(op == "*") { re *= x; im *= x;}
		static if(op == "/") {a.re /= x;  a.im /= x;}
		return this;
	}

	Anion opBinary(string op)(const Real!T x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion!(1, T)(a.re + x.re, a.im);
		static if(op == "-") return Anion!(1, T)(a.re - x.re, a.im);
		static if(op == "*") return Anion!(1, T)(a.re * x.re, a.im * x.re);
		static if(op == "/") return Anion!(1, T)(a.re / x.re, a.im / x.re);
	}

	Anion opBinaryRight(string op)(const Real!T x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion!(1, T)(x.re + a.re, a.im);
		static if(op == "-") return Anion!(1, T)(x.re - a.re, -a.im);
		static if(op == "*") return Anion!(1, T)(x.re * a.re, x * a.im);
		static if(op == "/") return  x * conj(this) / norm2;
	}

	ref Anion opOpAssign(string op)(const Real!T x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") re += x.re;
		static if(op == "-") re -= x.re;
		static if(op == "*") { re *= x.re; im *= xre;}
		static if(op == "/") {a.re /= x.re;  a.im /= x.re;}
		return this;
	}

	Anion opBinary(string op)(const Anion x) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion!(1, T)(re + x.re, im + x.im);
		static if(op == "-") return Anion!(1, T)(re - x.re, im - x.im);
		static if(op == "*") return Anion!(1, T)(re * x.re - im * x.im, im * x.re + re * x.im);
		static if(op == "/") return this * conj(this) / sqAbs(x);
	}

	ref Anion opOpAssign(string op)(const Anion x) const if (op == "+" || op == "-" || op == "*" || op == "/") {
		Anion t = this;
		return this = t.opBinary!op(x);
	}

	bool opEqual(const T x) {
		return left == x && right == zero!T;
	}

	bool opEqual(const Anion!(0, T) a) {
		return re == a.re && im == zero!T;
	}

	bool opEqual(const Anion a) {
		return left == a.left && right == a.right;
	}

	bool opEqual(uint m)(const Anion!(m, T) a) if(m > 1){
		return a.left == this && a.right == zero!T;
	}

	Tuple!(T, "ρ", T, "θ") polar() const @property { return tupe(abs(this), arg(this));}

	T dot(const Anion z) {
		return re * z.re + im * z.im;
	}

}

Anion!(1, T) polar(T)(T ρ, T θ) { return  ρ * Anion!(1, T)(cos(θ), sin(θ));}

T dot(U : Anion!(1, T), T)(const U x, const U y) {
	return x.dot(y);
}

U conj(U : Anion!(1, T), T)(const U a) {
	return U(re, -im);
}

T sqAbs(U : Anion!(1, T), T)(const U a) { return norm2(a);}

T abs(U : Anion!(1, T), T)(const U a) { return norm(a);}

T norm2(U : Anion!(1, T), T, int m)(const U a) if(m) { return pow!m(abs(re)) + norm2!m(abs(im));}
T norm2(U : Anion!(1, T), T, int m : 2 = 2)(const U a) {return re * re + im * im;}
T norm(U : Anion!(1, T), T, int m)(const U a) if(m) { return pow(norm2!m(a), one!T / m);}
T norm(U : Anion!(1, T), T, int m : 2 = 2)(const U a) { return sqrt(norm2(a));}

T normMax(U : Anion!(1, T), uint n, T)(const U a) { return max(abs(re), abs(im));}

T normMin(U : Anion!(1, T), uint n, T)(const U a) { return min(abs(re), abs(im));}

T arg(U : Anion!(1, T), T)(const U a) {
	return atan2(a.im, a.re);
}

U argx(U : Anion!(1, T), T)(const U a) {
	return U(zero!T, atan2(a.im, a.re));
}

U sign(U : Anion!(1, T), T)(const U a) {
	return isZero!(Anion!(1, T)) ? zero!(Anion!(1, T)) : value / abs(a);
}

U sqrt(U : Anion!(1, T), T)(const U a) {
	return sqrt(a.value);
}

U sqr(U : Anion!(1, T), T)(const U a) {
	return sqr!T(a.value);
}

U log(U : Anion!(1, T), T)(const U a) {
	return U(log(abs(a)), arg(a));
}

U exp(U : Anion!(1, T), T)(const U a) {
	return exp(re) * U(cos(im), sin(im)); 
}

U sin(U : Anion!(1, T), T)(const U a) {
	return sin(a.value);
}

U cos(U : Anion!(1, T), T)(const U a) {
	return cos(a.value);
}

U tan(U : Anion!(1, T), T)(const U a) {
	return tan(a.value);
}

U asin(U : Anion!(1, T), T)(const U a) {
	return asin(a.value);
}

U acos(U : Anion!(1, T), T)(const U a) {
	return acos(a.value);
}

U atan(U : Anion!(1, T), T)(const U a) {
	return atan(a.value);
}

U sinh(U : Anion!(1, T), T)(const U a) {
	return sinh(a.value);
}

U cosh(U : Anion!(1, T), T)(const U a) {
	return cosh(a.value);
}

U tanh(U : Anion!(1, T), T)(const U a) {
	return tanh(a.value);
}

U asinh(U : Anion!(1, T), T)(const U a) {
	return asinh(a.value);
}

U acosh(U : Anion!(1, T), T)(const U a) {
	return acosh(a.value);
}

U atanh(U : Anion!(1, T), T)(const U a) {
	return atanh(a.value);
}

string format(T)(in char[] fmt, const Anion!(1, T) a) {return "(" ~ format(fmt, a.left) ~ ", " ~ format(fmt, a.right) ~ ")";}
//--------------------------------------------------------------------------------------------

struct Anion(uint n, T) if(is_floating_point!T) {

	Anion!(n - 1) left, right;

	this(const T val) { left = val; right = zero!T;}

	this(T[] args...){
		assert(args.length <= (1<<n), " too many arguments");
		foreach(i, r; args) opIndexAssign()(r, i);
	}

	this(uint m)(const Anion!(m, T) a) if(m < n) {left = a; right = zero!T;}
	this(const Anion!(n - 1, T) l, const Anion!(n - 1, T) r = zero!(Anion!(n-1, T))) {
		left = l; right = r;
	}

	this(const Anion a) {left = a.left; right = a.right;}

	// re et im
	T re() const @property {
		return left.re;
	}

	void re(T value) @property {
		left.re = value;
	}

	Anion im() @property const {
		Anion a = this;
		a.re = zero!T;
		return a;
	}

	void im(const Anion imag) @property { 
		T r = re; this = imag; re = r;
	}

	U opCast(U : bool)() const {
		return cast(bool) left || cast(bool) right;
	}

	Tuple!(T, "ρ", Anion, "θ") polar() @property { return tuple(abs(this), argx(this));}

	ref Anion opAssign(T t) {
		left = t; right = zero!T;
		return this;
	}

	ref Anion opAssign(uint m)(const Anion!(m, T) a) if (m < n) {
		left = a; right = zero!T;
		return this;
	}

	ref Anion opAssign(const Anion a) {
		left = a.left; right = a.right;
		return this;
	}

	size_t opDollar()() const { return 1 << n;}

	T opIndex()(size_t index) { assert(index < 1<< n, "index is out of range");
		size_t maxi = 1 << (n - 1);
		return (index / maxi) ? right[index % maxi] : left[index % maxi];
	}

	T[] opIndex()() const {
		return left[] ~ right[];
	}

	ref T opIndexAssign()(T r, size_t index) { assert(index < 1<< n, "index is out of range");
		size_t maxi = 1 << (n - 1);
		return (index / maxi) ? right[index % maxi] = r : left[index % maxi] = r;
	}

	ref Anion opIndexAssign()(T r) {
		left[] = r; right[] = r;
		return this;
	}

	ref T opIndexOpAssign(string op)(T r, size_t index) if(op == "+" || op == "-" || op == "*" || op == "/") { assert(index < 1<< n, "index is out of range");
		size_t maxi = 1 << (n - 1);
		static if(op =="+") return (index / maxi) ? right[index % maxi] += r : left[index % maxi] += r;
		static if(op =="-") return (index / maxi) ? right[index % maxi] -= r : left[index % maxi] -= r;
		static if(op =="*") return (index / maxi) ? right[index % maxi] *= r : left[index % maxi] *= r;
		static if(op =="/") return (index / maxi) ? right[index % maxi] /= r : left[index % maxi] /= r;
	}

	ref T opIndexUnary(string op)(size_t index) if(op == "++" || op == "--") { assert(index < 1<< n, "index is out of range");
		size_t maxi = 1 << (n - 1);
		static if(op == "++") return (index / maxi) ? ++right[index % maxi] : ++left[index % maxi];
		static if(op == "--") return (index / maxi) ? --right[index % maxi] : --left[index % maxi];
	}

	T opIndexUnary(string op)(size_t index) if(op == "-" || op == "+") { assert(index < 1<< n, "index is out of range");
		static if(op == "+") return opIndex()(index);
		static if(op == "-") return (index / maxi) ? -right[index % maxi] : -left[index % maxi];
	}

	bool opIndexUnary(string op : "!")(size_t index) { assert(index < 1<< n, "index is out of range");
		size_t maxi = 1 << (n - 1);
		return (index / maxi) ? !right[index % maxi] : !left[index % maxi];
	}

	ref Anion opIndexOpAssign(string op)(T r) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op =="+") { left[] += r; right[] += r;}
		static if(op =="-") { left[] -= r; right[] -= r;}
		static if(op =="*") { left[] *= r; right[] *= r;}
		static if(op =="/") { left[] /= r; right[] /= r;}
		return this;
	}

	ref Anion opIndexUnary(string op)() if(op == "++" || op == "--") {
		static if(op == "++") {++left[]; ++right[];}
		static if(op == "--") {--left[]; --right[];}
		return this;
	}

	Anion opIndexUnary(string op)() if(op == "-" || op == "+") { 
		return opUnary!op();
	}

	bool opIndexUnary(string op : "!")() {
		return opUnary!"!"();
	}

	ref Anion opUnary(string op)() if(op == "++" || op == "--") {
		static if(op == "++") ++left; else --left;
		return this;
	}

	Anion opUnary(string op)() if(op == "+" || op == "-") {
		static if(op == "-") return Anion(-left, -right);
		return this;
	}

	bool opUnary(string op : "!")() const {
		return !left && !right;
	}

	T dot(const Anion a) const { return left.dot(a.left) + right.dot(a.right);}

	Anion opBinary(string op)(const T a) const if(op == "+" || op == "-" || op == "*" || op == "/" || op == "^^") {
		static if(op == "+") return Anion(left + a, right);
		else static if(op == "-") return Anion(left - a, right);
		else static if(op == "*") return Anion(left * a, right * a);
		else static if(op == "/") return Anion(left / a, right / a);
		else return exp(log(this) * a);
	}
	Anion opBinaryRight(string op)(const T a) const if(op == "+" || op == "-" || op == "*" || op == "/" || op == "^^") {
		static if(op == "+") return Anion(a + left, right);
		else static if(op == "-") return Anion(a - left, -right);
		else static if(op == "*") return Anion(a * left, a * right);
		else static if(op == "/") return a * inv(this);
		else return exp(log(this) * a);

	}
	Anion opBinary(string op)(const Anion!(0, T) a) const if(op == "+" || op == "-" || op == "*" || op == "/") { return opBinary!op(a.re);} 
	Anion opBinaryRight(string op)(const Anion!(0, T) a) const if(op == "+" || op == "-" || op == "*" || op == "/" || op == "^^") { return opBinaryRight!op(a.re);}

	Anion opBinary(string op, uint m)(const Anion!(m, T) a) const if((op == "+" || op == "-" || op == "*" || op == "/") && (m) && (m < n - 1)) {
		static if(op == "+") return Anion(left + a, right);
		else static if(op == "-") return (left - a, right);
		else static if(op == "*") return opBinary!op(Anion!(m+1, T)(a));
		else return opBinary!"*"(inv(Anion!(m + 1, T)(a)));
	}

	Anion opBinaryRight(string op, uint m)(const Anion!(m, T) a) const if((op == "+" || op == "-" || op == "*" || op == "/") && (m) && (m < n - 1)) {
		static if(op == "+") return Anion(a + left, right);
		else static if(op == "-") return (a - left, right);
		else static if(op == "*") return opBinaryRight!op(Anion!(m+1, T)(a));
		else return opBinaryRight!"*"(inv(Anion!(m + 1, T)(a)));
	}

	Anion opBinary(string op)(const Anion!(n - 1, T) a) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion(left + a, right);
		else static if(op == "-") return (left - a, right);
		else static if(op == "*") return Anion(left * a,  - right * conj(a));
		else return opBinary!"*"(inv(a));
	}

	Anion opBinaryRight(string op)(const Anion!(n - 1, T) a) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion(a + left, right);
		else static if(op == "-") return (a - left, right);
		else static if(op == "*") return Anion(a * left, a * right);
		else return opBinaryRight!"*"(inv(a));
	}

	Anion opBinary(string op)(const Anion a) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Anion(left + a.left, right + a.right);
		else static if(op == "-") return (left - a.left, right - a.right);
		else static if(op == "*") return Anion(left * a.left - conj(a.right) * right, a.right * left - right * conj(a.left));
		else return opBinary!"*"(inv(a));
	}

	Anion opBinary(string op : "^^", uint m)(const Anion!(m, T) a) if(m <= n) {
		return exp(log(this) * a);
	}
	Anion opBinaryRight(string op : "^^", uint m)(const Anion!(m, T) a) if(m < n) {
		return exp(log(a) * this);
	}

	ref Anion opOpAssign(string op)(const T a) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") left += a;
		else static if(op == "-") left -= a;
		else static if(op == "*") {left *= a; right *= a;}
		else {left /= a; right /= a;}
		return this;
	}

	ref Anion opOpAssign(string op)(const Anion!(0, T) a) if(op == "+" || op == "-" || op == "*" || op == "/") { return opOpAssign!op(a.re);}

	ref Anion opOpAssign(string op)(const Anion!(n-1, T) a) if(op == "+" || op == "-" || op == "*" || op == "/") { 
		static if(op == "+") left += a;
		else static if(op == "-") left -= a;
		else static if(op == "*") {left *= a; right = -right * conj(a);}
		else return opOpAssign!"*"(inv(a));
		return this;
	}

	ref Anion opOpAssign(string op, uint m)(const Anion!(m, T) a) if ((op == "+" || op == "-" || op == "*" || op == "/") && m && (m < n - 1)) {
		static if(op == "+") left += a;
		else static if(op == "-") left -= a;
		else static if(op == "*") return opOpAssign!"*"(Anion!(m+1)(a));
		else  return opOpAssign!"*"(inv(Anion!(m+1)(a)));
		return this;
	}

	ref Anion opOpAssign(string op)(const Anion a) if (op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") { left += a.left; right += a.right;}
		else static if(op == "-") { left -= a.left; right -= a.right;}
		else static if(op == "*") return this = opBinary!"*"(a);
		else return opOpAssign!"*"(inv(a));
		return this;
	}

	ref Anion opOpAssign(string op : "^^", uint m)(const Anion!(m, T) a) if (m <= n) {
		return this = exp(log(this) * a);
	}

	ref Anion opOpAssign(string op : "^^")(const T a) {
		return this = exp(log(this) * a);
	}

	ref Anion opOpAssign(string op, N)(N m) if(op ="^^" && isIntegral!N) {return this = pow(m, this);}

	bool opEqual(const T t) const { return left == t && right == zero!T; }

	bool opEqual(uint m)(const Anion!(m, T) a) const if(m < n) { return left == a && right == zero!T; }

	bool opEqual(const Anion a) const { return left == a.left && right == a.right;}

	bool opEqual(uint m)(const Anion!(m, T) a) const if (m > n) { return a.left == this && a.right == zero!T;}

	string toString() const { return "(" ~ a.left.tostring() ~ ", " ~ a.right.toString() ~ ")";}

	Anion e(size_t index)() @property if(index < (1 << n)) {
		if(!index) return this;
		Anion a(Zero!T);
		a[index] = 1;
		return this * a;
	}	

	alias i = e!1;
	alias j = e!2;
	alias k = e!3;
}

U conj(U : Anion!(n, T), uint n, T)(const U a) { return U(conj(a.left), -right);}

T dot(U : Anion!(n, T), uint n, T)(const U x, const U y) {
	return x.dot(y);
}

T sqAbs(U : Anion!(n, T), T)(const U a) { return norm2(a);}
T abs(U : Anion!(n, T), T)(const U a) { return norm(a);}
T norm2(U : Anion!(n, T), T, int m)(const U a) if(m) { return norm2!m(left) + norm2!m(left);}
T norm2(U : Anion!(n, T), T, int m : 2 = 2)(const U a) {return norm2(left) + norm2(right);}
T norm(U : Anion!(n, T), T, int m)(const U a) { return pow(norm2!m(a), one!T / m);}
T norm(U : Anion!(n, T), T, int m : 2 = 2)(const U a) { return sqrt(norm2(a));}
T normMax(U : Anion!(n, T), uint n, T)(const U a) { return max(normMax(left), normMax(right));}
T normMin(U : Anion!(n, T), uint n, T)(const U a) { return min(normMin(left), normMin(right));}

U sign(U : Anion!(n, T), uint n, T)(const U a) {
	T s = abs(a); return (s == zero!T) ? zero!U : a / s;
}

U inv(U : Anion!(n, T), uint n, T)(const U a) {return conj(a) / sqAbs(a);}
T arg(U : Anion!(n, T), uint n, T)(const  U a) { return atan2(abs(im), re);}
U argx(U : Anion!(n, T), uint n, T)(const U a) { return (a.isZero) ? T.nan.i : im.isZero ? arg(a).i : sign(im) * arg(a);}
U log(U = Anion!(n, T), uint n, T)(const U a) { return log(abs(a)) + argx(a);}

U exp(U = Anion!(n, T), uint n, T)(const U a) { T u = arg(a); return exp(abs(a)) * (cos(u) + sign(im) * sin(u));}
U sqrt(U = Anion!(n, T), uint n, T)(const U a) {
	if (a.im == 0) {
		if (a.re < 0) { return U(0, sqrt(abs(a)));}
		else U(sqrt(a));
	}
	else return exp(log(a)/2);
}

U sin(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return sin(a.re); z = sin(Complex!T(a.re, abs(a.im))); return z.re + sign(a.im) * z.im;}
U cos(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return cos(a.re); z = cos(Complex!T(a.re, abs(a.im))); return z.re + sign(a.im) * z.im;}
U tan(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return tan(a.re); z = tan(Anion!(1,T)(a.re, abs(a.im))); return z.re + sign(a.im) * z.im;}
U sinh(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return sinh(a.re); z = sinh(Anion!(1,T)(a.re, abs(a.im))); return z.re + sign(a.im) * z.im;}
U cosh(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return cosh(a.re); z = cosh(Anion!(1,T)(a.re, abs(a.im))); return z.re + sign(a.im) * z.im;}
U tanh(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return tanh(a.re); z = tanh(Anion!(1,T)(a.re, abs(a.im))); return z.re + sign(a.im) * z.im;}
U asin(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return asin(a.re); U sim = sign(a.im); return -sim * asinh(a * sim);}
U acos(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return acos(a.re); return -sign(a.im) * acosh(a);}
U atan(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return atan(a.re); U sim = sign(a.im); return -sim * atanh(a * sim);}
U asinh(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return asinh(a.re); U sim = im; z = asinh(Complex!T(re, abs(im))); return z.re * sign(sim) * z.im;}
U acosh(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return acosh(a.re); U sim = im; z = acosh(Complex!T(re, abs(im))); return z.re * sign(sim) * z.im;}
U atanh(U : Anion!(n, T), uint n, T)(const U a) { if (a.im == 0) return atanh(a.re); U sim = im; z = atanh(Complex!T(re, abs(im))); return z.re * sign(sim) * z.im;}

static if(is(U == Anion!(n, T), uint n, T)) mixin Functions!U;

Anion!(n, T) bessel(uint n, int m, T)(const Anion!(n, T) a) {
	T absIm = a.im.magnitude;
	numerics.complex.Complex!T z = besselJ!(n, T, m)(Anion!(1, T)(r, absIm));
	return z.re + a.im.sign * z.im;
}
T dot(uint n, T)(const  Anion!(n, T) a, const Anion!(n, T) b) {return a.dot(b);}
string format(uint n, T)(in char[] fmt, const Anion!(n, T) a) {return "(" ~ format(fmt, a.left) ~ ", " ~ format(fmt, a.right) ~ ")";}
//--------------------------------------------------------------------------------------------

auto e(T)(size_t index) {
    i = 0;
	while(index >= (1<<i)) ++i;
	Anion!(i, T) a(zero!T); a[index] = one!T;
	return a;
}

alias Quaternion(T) = Anion!(2, T);

alias Octonion(T) = Anion!(3, T);

alias Sedenion(T) = Anion!(4, T);

//--------------------------------------------------------------------------------------------

struct Slerp(uint n, T) {
private:
	Anion!(n, T) left, imag;
	T ln, atan;
	bool equal;
public:
	this(const ref Anion!(n, T) q1 = Anion!(n, T).e(1), const ref Anion!(n, T) q2 = Anion!(n, T).e(1)) {
		left = sign(q1);
		Anion!(n, T) right = sign(q2);
		equal = (q1 == q2);
		if (equal) return;
		right = conj(left) * right;
		imag = right.im;
		abs_imag = abs(imag);
		ln = 0.5 * log(right.re * right.re + abs_imag * abs_imag);
		atan = atan2(abs_imag, right.re);
	}
	Anion!(n, T) opCall(T t) {
		if (equal) return left;
		T expln = exp(t * ln);
		return (left * (exptlnb * cos(t * atan) + imag * (exptln * sin(t * atan)))).im;
	}
}

struct Bezier(uint n, T) {
private:
	Slerp!(n, T)[] slerps;
	this(Anion!(n, T)[] q...) {
		slerps = new Slerp[q..length - 1];
		for(size_t i = 0; i < slerps.length; ++i) slerps[i] = Slerp!(n, T)(q[i], q[i + 1]);
	}
	Anion!(n, T) opCall(T t) const {
		Anion!(n, T)[slerps.length] pts;
		for (size_t i = 0; i < slerps.length; ++i) pts[i] = slerps[i](t);
		for (size_t j = slerps.length - 1; j >= 0; --j)
			for (size_t i = 0; i < j; ++j) pts[i] = Slerps!(n, T)(pts[i], pts[i + 1])(t);
		return pts[0];
	}
}

struct Splin(uint n, T) {
private:
	Slerp!(n, T)[] slerpa;
	Slerp!(n, T)[] slerpq;
	Anion!(n, T) q0, qn;
	this(Anion!(n, T)[] q...) {
		slerpa = Slerp!(n, T)[q.length - 1];
		slerpq = Slerp!(n, T)[q.length - 1];
		Anion!(n, T)[q.legnth] a;
		q0 = q[0];
		qn = q[$ - 1];
		a[0] = q[0];
		for(zize_t i = 1; i < q.length - 1; ++i) {
			Anion!(n, T) invqi = inv(q[i]);
			a[i] = q[i] * exp(T(0.25) * (log(invqi * q[i + 1]) + log(invqi * q[i - 1])));
		}
		a[n -1] = q[n - 1];
		for(size_t i = 0; i < q.length - 1; ++i) {
			slerpq[i] = Slerp!(n, T)(q[i], q[i + 1]);
			slerpa[i] = Slerp!(n, T)(a[i], a[i + 1]);
		}
	}
	Anion!(n, T) opCall(T t) {
		if (t <= 0) return q0;
		if (t >= n - 1) return qn;
		size_t m = size_t(std.math.floor(t));
		T ft = t - m;
		return Slerp!(n, T)(slerpq[m](ft), slerpa[m](ft))(2 * ft * (1 - ft));
	}
}

auto anion(T, uint m)(const T[] list...) {
	T[] array = list.dup;
	if(array.length > 1) {
		if (array.length.isOdd) list ~= zero!T;
		Anion!(m + 1, T)[] a;
		for(size_t i = 0; i < length; i += 2) a ~= Anion!(m+1, T)(array[i], array[i + 1]);
		return anion(a);
	} else if(list.length == 1) return Anion!(0, T)(list[0]);
	else return zero!(Anion!(0, T));
}

auto anion(T, uint m)(const Anion!(m, T)[] list...) {
	if(array.length > 1) {	
		Anion!(m, T)[] array = list.dup;
		if (array.length.isOdd) list ~= Anion!(m, T)();
		Anion!(m + 1, T)[] a;
		for(size_t i = 0; i < length; i += 2) a ~= Anion!(m+1, T)(array[i], array[i + 1]);
		return anion(a);
	} else if(array.length == 1) return list[0];
	else return zero!(Anion!T(0, T));
}


