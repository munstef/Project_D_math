module numerics.algorithm;
import std.algorithm;
import std.math;
import std.range;
import std.traits : CommonType;
import std.typecons;
import meta;

auto product(R)(R r) if (isInputRange!R && !isInfinite!R && is(typeof(r.front * r.front))) {
	return reduce!((a, b) => a * b)(r);
}
auto product(R, S)(R r, S seed) if (isInputRange!R && !isInfinite!R && is(typeof(seed * r.front))) {
	return reduce!((a, b) => a * b)(seed, r);
}

ElementType!R max(R)(R r) if (IsInputRange!R && !isInfinite!R && is_comparable!(ElementType!R)) {
	return reduce!((a, b) => (a >= b ? a : b))(r);
}

ElementType!R min(R)(R r) if (IsInputRange!R && !isInfinite!R && is_comparable!(ElementType!R)) {
	return reduce!((a, b) => (a <= b ? a : b))(r);
}

Tuple!(ElementType!R, "min", ElementType!R, "max") minmax(R)(R r) if(IsInputRange!R && !isInfinite!R && is_comparable!(ElementType!R)) {
	return tuple(reduce!((a, b) => (a <= b ? a : b))(r), reduce!((a, b) => (a >= b ? a : b))(r));
}

struct Recurrence(alias fun, StateType, size_t stateSize)
{
    import std.functional : binaryFun;

    StateType[stateSize] _state;

    size_t _n;

	StateType[stateSize] initState;

	this(StateType[stateSize] initial) {_state = initState = initial;}

    void popFront() {
        static auto trustedCycle(ref typeof(_state) s) @trusted { return cycle(s);}
        _state[_n % stateSize] = cast(StateType) binaryFun!(fun, "a", "n")
			(trustedCycle(_state), _n + stateSize);
        ++_n;
    }

    @property StateType front() { return _state[_n % stateSize];}

    @property typeof(this) save() { return this;}

    enum bool empty = false;


	StateType opIndex(size_t index)() {
		_n = _n > index ? (_state = initState, 0) : _n;
		foreach(i; _n .. index) popFront();
		return front;
	}

	StateType[] opSlice(size_t lower, size_t upper) {
		StateType[] r =[];
		if (lower > upper) return r;
		_n = _n > lower ? (_state = initState, 0) : _n;
		foreach(i; _n .. lower) popFront();
		foreach(i; lower .. upper) {popFront(); r ~= front;}
		return r;
	}
}

Recurrence!(fun, CommonType!(State), State.length)
recurrence(alias fun, State...)(State initial) {
    CommonType!(State)[State.length] state;
    foreach (i, Unused; State) state[i] = initial[i];
    return typeof(return)(state);
}

struct Permutations(bool doCopy = true, T) if (isMutable!T) {
    private immutable size_t num;
    private T[] items;
    private uint[31] indexes;
    private ulong tot;

    this (T[] items) pure nothrow @safe @nogc
		in {
			static enum string L = indexes.length.text;
			assert(items.length >= 0 && items.length <= indexes.length,
				   "Permutations: items.length must be >= 0 && < " ~ L);
		} body {
			static ulong factorial(in size_t n) pure nothrow @safe @nogc {
				ulong result = 1;
				foreach (immutable i; 2 .. n + 1)
					result *= i;
				return result;
			}

			this.num = items.length;
			this.items = items;
			foreach (immutable i; 0 .. cast(typeof(indexes[0]))this.num)
				this.indexes[i] = i;
			this.tot = factorial(this.num);
		}

		@property T[] front() pure nothrow @safe {
			static if (doCopy) {
				return items.dup;
			} else
				return items;
		}

		@property bool empty() const pure nothrow @safe @nogc {
			return tot == 0;
		}

		@property size_t length() const pure nothrow @safe @nogc {
			// Not cached to keep the function pure.
			typeof(return) result = 1;
			foreach (immutable x; 1 .. items.length + 1)
				result *= x;
			return result;
		}

		void popFront() pure nothrow @safe @nogc {
			tot--;
			if (tot > 0) {
				size_t j = num - 2;

				while (indexes[j] > indexes[j + 1])
					j--;
				size_t k = num - 1;
				while (indexes[j] > indexes[k])
					k--;
				swap(indexes[k], indexes[j]);
				swap(items[k], items[j]);

				size_t r = num - 1;
				size_t s = j + 1;
				while (r > s) {
					swap(indexes[s], indexes[r]);
					swap(items[s], items[r]);
					r--;
					s++;
				}
			}
		}
}

Permutations!(doCopy,T) permutations(bool doCopy=true, T)(T[] items) pure nothrow if (isMutable!T) {
    return Permutations!(doCopy, T)(items);
}


struct Spermutations(bool doCopy=true) {
    private immutable uint n;
    alias TResult = Tuple!(int[], int);

    int opApply(in int delegate(in ref TResult) nothrow dg) nothrow {
        int result;

        int sign = 1;
        alias Int2 = Tuple!(int, int);
        auto p = n.iota.map!(i => Int2(i, i ? -1 : 0)).array;
        TResult aux;

        aux[0] = p.map!(pi => pi[0]).array;
        aux[1] = sign;
        result = dg(aux);
        if (result) return result;

        while (p.any!q{ a[1] }) {
            // Failed to use std.algorithm here, too much complex.
            auto largest = Int2(-100, -100);
            int i1 = -1;
            foreach (immutable i, immutable pi; p)
                if (pi[1])
                    if (pi[0] > largest[0]) {
                        i1 = i;
                        largest = pi;
                    }
            immutable n1 = largest[0],
				d1 = largest[1];

            sign *= -1;
            int i2;
            if (d1 == -1) {
                i2 = i1 - 1;
                p[i1].swap(p[i2]);
                if (i2 == 0 || p[i2 - 1][0] > n1)
                    p[i2][1] = 0;
            } else if (d1 == 1) {
                i2 = i1 + 1;
                p[i1].swap(p[i2]);
                if (i2 == n - 1 || p[i2 + 1][0] > n1)
                    p[i2][1] = 0;
            }

            if (doCopy) {
                aux[0] = p.map!(pi => pi[0]).array;
            } else {
                foreach (immutable i, immutable pi; p)
                    aux[0][i] = pi[0];
            }
            aux[1] = sign;
            result = dg(aux);
            if (result) return result;;

            foreach (immutable i3, ref pi; p) {
                immutable n3 = pi[0],
					d3 = pi[1];
                if (n3 > n1)
                    pi[1] = (i3 < i2) ? 1 : -1;
            }
        }

	    return result;
    }
}

Spermutations!doCopy spermutations(bool doCopy=true)(in uint n) {
    return typeof(return)(n);
}


T median(T)(T[] nums) pure nothrow {
	nums.sort();
    if (nums.length & 1)
        return nums[$ / 2];
    else
        return (nums[$ / 2 - 1] + nums[$ / 2]) / 2.0;
}

Tuple!(ElementType!R, "mean", ElementType!R, "StdDev") meanStdDev(R)(R numbers) /*nothrow*/ @safe /*@nogc*/ {
    if (numbers.empty)
        return tuple(0.0L, 0.0L);

    real sx = 0.0, sxx = 0.0;
    ulong n;
    foreach (x; numbers) {
        sx += x;
        sxx += sqr(x);
        n++;
    }
    return tuple(sx / n, sqrt(n * sxx - sx ^^ 2) / n);
}

auto powMean(T, int n)(T data) pure if(n && n % 2){
	return (data.map!(x => abs(x)^^n).sum / data.length) ^^ (-n);
}

auto powMean(T, int n : 0)(T data) pure {
	return abs(data.product) ^^ (-data.length);
}

auto powMean(T, int n)(T data) pure if(n && !(n%2)) {
	return (data.map!(x => x ^^ n).sum / data.length) ^^ (-n);
}

auto powMean(T, int n : 1)(T data) pure {
	return data!map(a => abs(a)).sum / data.length;
}

auto powMean(T, int n : 2)(T data) pure {
	return data!map(a => sqr(a)).sum / data.length;
}

auto powMeanMax(T)(T data) pure {
	return reduce(a, b => a >= b ? a : b)(r!map(a => abs(a)));
}

auto powMeanMin(T)(T data) pure {
	return reduce(a, b => a <= b ? a : b)(r!map(a => abs(a)));
}

alias aMean(T) = powMean!(T, 1);

alias hMean(T) = powMean!(T, -1);

alias gMean(T) = powMean!(T, 0);

alias norm(T, int n) = powMeans!(T, n);

Tuple!(Signed!size_t, "index", T, "max") indexMax(T)(T[] arg...) {
	if(arg.length == 0) return tuple(-1, zero!T);
	T max = arg[0]; Signed!size_t index = 0;
	foreach(v, i; arg[1 ..$]) if(v > max) { index = i; max = v;}
	return tuple(index, max);
}

Tuple!(Signed!size_t, "index", T, "min") indexMin(T)(T[] arg...) {
	if(arg.length == 0) return tuple(-1, zero!T);
	T min = arg[0]; Signed!size_t index = 0;
	foreach(v, i; arg[1 ..$]) if(v < min) { index = i; min = v;}
	return tuple(index, min);
}

Tuple!(Signed!size_t, "indexMin", T, "min", Signed!size_t, "indexMax", T, "max") indexMinMax(T)(T[] arg...) {
	if(arg.length == 0) return tuple(-1, zero!T, -1, zero!T);
	T min, max; Signed!size_t indexMin, indexMax;
	min = max = arg[0]; indexMin = indexMax = 0;
	foreach(v, i; arg[1 ..$]) {
		if(v < min) { indexMin = i; min = v;}
		if (v > max) { indexMax = i; max = v;}
	}
	return tuple(indexMin, min, indexMax, max);
}

T[] knuthShuffle(T)(const T[] arg...) {
	size_t n = arg.length;
	T[n] arr = arg.dup;
	auto gen = Random(unpredictableSeed);
	for(size_t = 0; i < n; ++i) {
		r = uniform!(i, n, gen);
		T temp = a[r];
		a[r] = a[i];
		a[i] = temp;
	}
	return arr;
}
