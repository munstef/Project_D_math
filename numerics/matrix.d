module numerics.matrix;

import numerics.polynomial;
import std.math : pow;

struct Matrix(T, size_t nbrows : 1, size_t nbcols : 1) {
	private union {
		T[1][1] coeffs;
		T a11;
	}
	T opIndex(size_t index1, size_t index2) {
		assert(index1 < nbrows && index2 < nbcols);
		return a11;
	}
	ref T opIndexAssign(size_t index1, size_t index2) {
		assert(index1 < nbrows && index2 < nbcols);
		return a11;
	}
	T det() @property  {return a11;}
	T tr() @property {return a11;}
	Matrix T() @property {return this;}
	Matrix tr() @property {return this;}

	enum  bool isSquare = true;
}
T det(T)(Matrix!(T, 1, 1) M) { return M.coeffs;}
T trace(T)(Matrix!(T, 1, 1) M) {return M.coeffs;}
Matrix!(T, 1, 1) inv(T)(Matrix!(T, 1, 1) M) { Matrix R; R.coeffs = inv(coeffs); return R;}
enum size_t size = 1;

struct Matrix(T, size_t nbrows, size_t nbcols) {

	enum bool isSquare = nbrows == nbcols;
	enum size_t size = nbrows * nbcols;

	private static string elements(string letter) @property {
		string res;
		foreach (x; 0..nbrows)
			foreach (y; 0..nbcols) res ~= "T " ~ letter ~ to!string(y+1) ~ to!string(x+1) ~ ";";
		return res;
	}

	private union {
		T[nbrows][nbcols] coeffs;
		struct { mixin(elements("a")); }
		T[size] _coeffs;
	}

	this(T a = T(0)) { _coeffs[] = a;}

	T opIndex(size_t index1, size_t index2) {
		assert(index1 < nbrows && index2 < nbcols);
		return coeffs[index1][index2];
	}
	ref T opIndexAssign(T value, size_t index1, size_t index2) {
		assert(index1 < nbrows && index2 < ncols);
		return coeffs[index1][index2] = value;
	}
	ref T opIndexOpAssign(string op)(T value, size_t index1, size_t index2) {
		assert(index1 < nbrows && index2 < nbcols);
		mixin("return coeffs[index1][index2] " ~ op ~ "= value;");
	}
	ref Matrix opAssign(Matrix M) {
		coeffs = M.coeffs.dup;
		return this;
	}
	ref Matrix opAssign(T t) {
		size_t m = min(nbrows, nbcols);
		_coeffs[] = 0;
		foreach(row, i; coeffs) row[i] = t;
		return this;
	}

	Matrix!(T,nbrows, nbols + cols) opBinary(size_t cols, string op = "|")(const Matrix!(T, nbrows, cols) I) const if(cols) {
		Matrix!(T, nbrows, nbcols + cols) R;
		foreach(i, row; A.coeffs) R.coeffs[i] = row ~ I.coeffs[i];
		return R;
	}

	ref Matrix opOpAssign(string op)(T t) if (op == "*" || op == "/") {
		for(size_t i = 0; i < size; ++i) _coeffs[i].opOpAssign(op)(t);
		return this;
	}
	ref Matrix opOpAssign(string op)(T t) if (op =="+" || op == "-") {
		size_t  m = min(nbrows, nbcols);
		foreach(i; 0 .. m) coeffs[i][i].opOpAssign(op)(t);
		return this;
	}
	ref Matrix opOpAssign(string op)(Matrix M) if (op == "+" || op == "-") {
		foreach(x, i; _coeffs) x.opOpAssign(op)(M._coeffs[i]);
		return this;
	}

	Matrix!(T, nbcols, nbrows) T() const @property {
		Matrix!(T, nbcols, nbrows) M;
		foreach(i; 0 .. nbrows) foreach(j; 0 .. nbcols) M.coeffs[j][i] = coeffs[i][j];
		return M;
	}

	Tuple!(Matrix, T) GaussJordan() const @property {
		Matrix A = tihs;
		size_t r = 0;
		size_t p = 0;
		T t = zero!T;
		T det = T(1);
		foreach(j; 0 .. nbcols) {
			foreach(i; r+1 .. nbrows) k = max(k, abs(A.coeffs[i, j]));
			++r;
			A.coeeffs[k] /= A.coeffs[k][j];
			if (k /= r) { p++, swap(A.coeffs[k], A.coeffs[r]);}
			foreach(i; 0 .. nbrow) {
				if(i/=r) A.coeffs[i] -= A.coeefs[r] * A.coeffs[i, j];
			}
		}
		return tuple(A, p);
	}


	static if(isSquare) {

		enum Matrix {zero = T(0), identity = T(1)}

		ref Matrix opOpAssign(string op)(Matrix M) if(op == "*" || op ="/") {
			Matrix N = this; Matrix m;
			if (op == "/") m = inv(M); else m = M;
			for(size_t i = 0; i < nbrows; ++i) for(size_t j = 0; j < nbrows; ++j) {
				coeffs[i][j] = 0;
				for(size_t r = 0; r < nbrows; ++r) coeffs[i][j] += N.coeffs[i][r] * m.coeffs[r][j];
			}
			return this;
		}

		Matrix!(T, nbrows - 1, nbrows -1) minor(size_t r, size_t c) {
			assert( r < nbrows && c < nbrows);
			Matrix!(T, nbrows - 1) M;
			for(size_t i = 0; i < nbrows -1; i++) for(size_t j = 0; i < nbrows - 1; ++j) {
				M.coeffs[i][j] = coeffs[i + (i >= r)][j + (j >= c)];
			}
			return M;
		}

		T det() {
			if (nbrow == 1) return a11;
			else if (nbrows == 2) return a11 * a22 - a21 * a12;
			T result = zero!T;
			foreach(i, m; coeffs[0]) result += coeffs[0][i] * pow_1(i) * minor(0, i).det;
		}

		T perm() @property nothrow {
			auto r = nbrows.iota;
			T tot = 0;
			foreach(const sigma; r.array.permutations)
				tot += r.map!(i => coeffs[i][sigma[i]]).prod;
			return tot;
		}


		T tr() @property {
			T t = 0;
			for(size_t i = 0; i < nbrows; ++i) t += coeffs[i][i];
			return t;
		}

		Matrix opBinary(string op)(Matrix M) if(op == "*" || op == "/") {
			if(op == "/") M = inv(M);
			Matrix R;
			for(size_t i = 0; i < nbrows; ++i) for(size_t j = 0; j < nbrows; ++j) {
				R.coeffs[i][j] = 0;
				for(size_t r = 0; r < nbrows; ++r) R.coeffs[i][j] += coeffs[i][r] * M.coeffs[r][j];
			}
			return M;
		}

		Matrix opBinaryRight(string op)(T, t) if(op == "*" || op == "/") {
			Matrix M = (op == "*") ? this : inv(this);
			for(size_t i; i < nbrows; ++i) for(size_t j = 0; j < nbcolq; j++) M.coeffs[i][j] = t * M.coeffs[i][j];
			return M;
		}

		T norm(uint n : 1)() @property {
			return transversal(M.coeffs, 0).map!q{abs(a)}.sum;
		}

		T norm(uint n : 2)() @property {
			return transversal(M.coeffs, 0).map!q{a * a}.sum.sqrt;
		}

		alias abs = norm!2;

		T norm(uint n)() @property if(n > 2 && n % 2) {
			return transversal(M.coeffs, 0).map!q{pow(abs(a), n)}.sum.pow(1.0L/n);
		}

		T norm(uint n)() @property if(n > 2 && n % 2 == 0) {
			return transversal(M.coeffs, 0).map!q{pow(a, n)}.sum.pow(1.0L/n);
		}
		
		T normInf() @property {
			return reduce((a, b) => max(abs(a), abs(b)), _coeffs, zero!T);
		}

		Tuple!(Matrix, "L", Matrix, "U", Matrix, "P") decomposeLUP() @property {
			Matrix C = this;
			Matrix P = Matrix.identity;
			for (size_t i = 0; i < nbrows; i++) {
				T pivotValue = 0.0;
				size_t pivot;
				for (size_t row = i; row < nbrows; row++) {
					if (abs(C[row, i]) > pivotValue) {
						pivotValue = abs(C[row, i]);
						pivot = row;
					}
				}
				assert(pivotValue != 0.0);
				P.swapRows(pivot, i);
				C.swapRows(pivot, i);
				for (size_t j = i + 1; j < nbrows; j++) {
					C[j, i] = C[j, i] / C[i, i];
					for (size_t k = i + 1; k < nbrows; k++) C[j, k] = C[j, k] - C[j, i] * C[i, k];
				}
			}
			Matrix L = Matrix.identity;
			Matrix U = Matrix.zero;
			for (size_t i = 0; i < nbrows; i++) {
				for (size_t j = 0; j < nbrows-1-i; j++) L[nbrows-1-i, j] = C[nbrows-1-i, j];
				for (size_t j = i; j < nbrows; j++) U[i, j] = C[i, j];
			}
			return tuple(L, U, P);
		}	
		
		Matrix cholesky() @property pure nothrow {
			Matrix!(T, nbrows, nbcols) L;
			foreach(immutable r, row; L) row[r + 1 .. $] = 0;
			foreach(immutable i; 0 .. nbrows)
				foreach(immutable j; 0 .. i + 1) {
					auto t = dotProduct(L.coeffs[i][0 .. j], L.coeffs[j][0 .. j]);
					L.coeffs[i][j] = (i == j) ? (coeffs[i][i] - t) ^^ 0.5 : (1.0 / L.coeffs[j][j] * (coeffs[i][j] - t));
				}
			return L;
		}

		Tuple!(Matrix, "inv", T, "det") GaussJordan() const @property {
			auto M = this|identity;
			auto result = M.GaussJordan;
			Matrix inv = result[inv].coeffs[][$/2 .. $];
		}
	}



	Matrix opBinary(string op)(Matrix M) if(op == "+" || op == "-") {
		Matrix R;
		if(op == "+") R.coeffs = coeffs + M.coeffs; else R.coeffs = coeffs - M.coeffs;
		return R;
	}

	Matrix opBinary(string op)(T t) if(op == "*" || op == "/") {
		Matrix M; M.coeffs = coeffs.dup;
		M.opOpAssign(op)(t);
		return M;
	}
	Matrix opBinary(string op)(T t) if(op == "+" || op == "-") {
		Matrix M = this;
		t = (op == "+") ? t : -t;
		for(size_t i = 0; i < nbrows; ++i) M.coeffs[i][i] = coeffs[i][i] + t;
		return M ;
	}
	Matrix opBinaryRight(string op)(T t) if(op == "+" || op == "-") {
		Matrix M = (op == "+") ? this : -this;
		for(size_t i = 0; i < nbrows; ++i) M.coeffs[i][i] = t + M.coeffs[i][i];
		return M ;
	}
	Matrix opUnary(string op)() if (op == "+" || op == "-") {
		if (op == "+") return this;
		else {
			Matrix M = this;
			M.coeffs[][] = -M.coeffs[][];
			return M;
		}
	}
	Matrix t() @property {
		Matrix M;
		for(size_t i=0; i < nbrows; ++i)
			for(size_t j = 0; j < nbrows; ++j) M.coeffs[i][j] = coeffs[j][i];
		return M;
	}

	ref Matrix opOpAssign(string op)(Matrix!(T, nbcols, nbcols) M) if(op = "*" || op = "/") {
		if (op == "/") return M = inv(M);
		Matrix R = this;
		for(size_t i = 0; i < nbrows; ++i)
			for(size_t j = 0; j < nbcols; ++j) {
				coeffs[i][j] = 0;
				for(size_t k = 0; k < nbcols; ++k) coeffs[i][j] += R.coeffs[i][k] * M.coeffs[k][i];
			}
		return this;
	}

	Matrix!(T, nbrows, n) opBinary(size_t n, string op)(Matrix!(T, nbcols, n) M) if (op == "*") {
		Matrix!(T, nbrows, n) R;
		for (size_t i = 0; i < nbcols; ++i)
			for(size_t k = 0; k < n; ++k) {
				R.coeffs[i][k] = 0;
				for(size_t j = 0; j < nbcols; ++j) R.coeffs[i][k] += coeffs[i, j] * M.coeffs[j, k];
			}
		return R;
	}

	Tuple!(Matrix, "L", T[], "D") LDLTdecomposition() const @property {
		T[nbrows] D = Zero!T;
		Matrix L;
		D[0] = coeffs[0, 0];
		L.coeffs[0][0] = T(1);
		T r;
		for(size_t j = 1; j < nbrows; ++j) {
			D[j] = coeffs[j][j];
			r = zero!T;
			for(size_t k = 0; k < j; ++j)
				r += L.coeffs[j][k] * conj(L.coeffs[j][k]) * D[k];
			D[j] -= r;
			for(size_t i = j + 1; i < nbrows; ++i) {
				L.coeffs[i][j] = coeffs[i][j];
				T r = zero!T;
				for(size_t k = 0; k < j; ++k) r += L.coeffs[i][k] * conj(L.coeffs[j][k]) * D[k];
				L.coeffs[i][j] -= r;
				L.coeffs[i][j] /= D[j];
			}
		}
		return tuple(L, D);
	}



}
	
T det(T, size_t n)(Matrix!(T, n, n) M) { return M.det;}

T tr(T, size_t n)(Matrix!(T, n, n) M) { return M.tr;}

T perm(T, size_t n)(Matrix!(T, n, n) M) { return M.perm;}

Matrix!(T, n, n) cholersky(T, size_t n)(Matrix!(T, n, n) A) {
	return A.chokesky();
}

Matrix!(T, n, n) inv(T, size_t n)(Matrix!(T, n, n) M) {
	T d = M.det;
	assert(d != 0);
	Matrix R;
	for (size_t = 0; i < nbrows; ++i)
		for(size_t j = 0; j < nbrows; ++j)
			R.coeffs[j][i] = (isEven(i+j) ? 1 : -1) * M.minor(i, j).det / d;
	return M;
}

Matrix!(T, n, n) adj(T, size_t n)(Matrix!(T, n, n) M) {
	Matrix R;
	for (size_t = 0; i < nbrows; ++i)
		for(size_t j = 0; j < nbrows; ++j)
			R.coeffs[j][i] = (isEven(i+j) ? 1 : -1) * M.minor(i, j).det;
	return M;
}

Polymonial!T characteristic(T, size_t n)(Matrix!(T, n, n) M) {
	Matrix!(Polymonial!T, nbrows) m;
	for(size_t i = 0; i < nbrows; ++i)
		for(size_t j = 0; j < nbrows; ++j)
			if (i == j) m.coeffs[i][j] = M.coeffs[i][j] - X!T(1); else m.coeffs[i][j] = M.coeffs[i][j];
	return det(m);
}

auto diagonal(T)(T[] arg ...) {
	Matrix!(T, arg.length, arg.length) R;
	R.coeffs[][] = T(0);
	foreach(x, i; arg) R[i, i] = x;
	return R;
}


