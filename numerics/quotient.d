module numerics.quotient;

import meta;
import numerics;
import std.format;

string format(T)(in char[] fmt, const quotient!T a) {return format(fmt, a.num) ~ " / " ~ format(fmt, a.right);}

struct quotient(T : Integer) {
private:
	T num = zero!T;
	T den = one!T;
protected:

	void reduce() {
		if(numerics.isZero!T(den)) num = numerics.sign!T(num);
		else {
			if (isNegative!T(den)) {num = -num; den = -den;}
			T g = gcd!T(num, den) * numerics.sign!T(num);
			if (g > 1) {num /= g; den /= g;}
		}
	}

public:

	T numerator() const @property { return num;}

	T denominator() const @property { return den;}

	string toString() const {return std.format.format("%d / %d", num, den);}

	this(Z)(const Z n) if(is_integral!Z) {
		num = n;
		den = one!T;
		reduce();
	}

	this(Z1, Z2)(const Z1 n, const Z2 d) if(is_integral!Z1 && is_integral!Z2) {
		num = n;
		den = d;
		reduce();
	}


	this(F)(F x) if(is_floating_point!F) {
		F u = x;
		T pm2 = 1;
		T qm2 = 0;
		T num = T(x);
		den = 1;
		T last = num;
		T a, p, q;
		while (F(this) != u) {
			u = inv(u - F(last));
			a = T(u);
			p = a * num + pm2;
			q = a * den + qm2;
			pm2 = num;
			num = p;
			qm2 = den;
			den = q;
			last = a;
		}
		reduce();
	}

	static struct fraction {
		quotient opcall()(T[] n...) {
			if (n.length == 0) return quotient(zero!T);
			else if (n.length == 1) {return quotient(n[0]);}
			else {
				T pm2 = 1;
				T qm2 = 0;
				T num = n[0];
				T den = 1;
				T p, q;
				for(size_t i = 1; i < n.length; ++i) {
				    alias a = n[i];
					p = a * num + pm2;
					q = a * den + qm2;
					pm2 = num;
					num = p;
					qm2 = den;
					den = q;
				}
			}
			return quotient(num, den);
		}

		quotient opCall()(quotient[] r...) {
		    if(!r.length) return quotient(zero!T);
		    else if(r.length == 1) return r[0];
			T pm2 = 1;
			T qm2 = 0;
			num = r[0].num;
			den = r[0].den;
			T p, q;
			for(size_t i = 1; i< n.length; ++i) {
			    alias a = r[i];
				p = a.den * num + a.num * pm2;
				q = a.den * den + a.num * qm2;
				pm2 = num;
				num = p;
				qm2 = den;
				den = q;
			}
			return quotient(num, den);
		}
	}

	enum : quotient {nan = quotient(), infinity = quotient(one!T, zero!T)};

	bool isNaN() const @property { return numerics.isZero!T(num) && numerics.isZero!T(den);}
	bool isInfinity() const @property {return numerics.isZero(den) && !numerics.isZero(num);}
	bool isPositiveInfinity() const @property {return numerics.isZero!T(den) && num > zero!T;}
	bool isNegativeInfinity() const @property {return numerics.isZero!T(den) && num < zero!T;}
	bool isZero() const @property {return numerics.isZero!T(num) && !numerics.isZero!T(den);}

	ref quotient opUnary(string op)() if (op == "++" || op == "--") {
		if (op == "++") num += den; else num -= den;
		return this;
	}

	quotient opUnary(string op)() const if (op== "+") {
		return this;
	}

	quotient opUnary(string op)() const if (op == "-") {
		return quotient(-num, den);
	}

	ref quotient opAssign(quotient q) {
		num = q.num;
		den = q.den;
		return this;
	}
	ref quotient opAssign(Z)(quotient!Z q) if(is_signed_integral!Z) {
		num = q.num;
		den = q.den;
		return this;
	}

	ref quotient opAssign(T t) {
		num = t;
		den = 1;
		return this;
	}

	ref quotient opAssign(Z)(Z z) if(is_signed_integral!Z) {
		num = z;
		den = 1;
		return this;
	}

	ref quotient opOpAssign(string op) (const quotient q) if (op == "+" || op =="-") {
		if (op == "+") num = num * q.den + q.num * den; else num = num * q.den - q.num * den;
		den *= q.den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z) (const quotient!Z q) if ((op == "+" || op =="-") && (is_signed_integral!Z)) {
		if (op == "+") num = num * q.den + q.num * den; else num = num * q.den - q.num * den;
		den *= q.den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op) (const quotient q) if(op == "*" || op =="/" || op == "%") {
		T n = num;
		if (op == "*") { num *= q.num; den *= q.den;} else {num *= q.den; den *= q.num;}
		if (op == "%") { num = remainder;}
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z) (const quotient!Z q) if((op == "*" || op =="/" || op == "%")  && is_signed_integral!Z){
		T n = num;
		if (op == "*") { num *= q.num; den *= q.den;} else {num *= q.den; den *= q.num;}
		if (op == "%") { num = remainder;}
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op)(const T t) if((op == "+") || (op == "-")) {
		if(op == "+") num += t * den; else num -= t * den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op)(const T t) if((op == "*") || (op == "/") || (op == "%")) {
		if(op == "*") num *= t; else den *= t;
		if(op == "%") num = remainder;	
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z)(const Z t) if(((op == "+") || (op == "-")) && (is_signed_integral!Z )) {
		if(op == "+") num += t * den; else num -= t * den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z)(const Z t) if(((op == "*") || (op == "/") || (op == "%")) && (is_signed_integral!Z)) {
		if(op == "*") num *= t; else den *= t;
		if(op == "%") num = remainder;	
		reduce();
		return this;
	}

	quotient opBinary(string op)(const quotient q) const if (op== "+" || op == "-" || op == "*" || op == "/" || op == "%") {
		quotient r = this; return r.opOpAssign!(op)(q);
	}

	quotient opBinary(string op)(const T t) const if (op== "+" || op == "-" || op == "*" || op == "/" || op == "%") {
		quotient r = this; return r.opOpAssign!(op)(t);
	}

	quotient opBinary(string op, Z)(const quotient!Z q) const if ((op== "+" || op == "-" || op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)){
		quotient r = this; return r.opOpAssign!(op, Z)(q);
	}

	quotient opBinary(string op, Z)(const Z t) const if ((op== "+" || op == "-" || op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)) {
		quotient r = this; return r.opOpAssign!(op, Z)(t);
	}

	quotient opBinaryRight(string op, Z)(const Z t) const if ((op == "+" || op == "-") && (is_signed_integral!Z)) {
		quotient r = this;
		return r.opOpAssign!(op, Z)(t);
	}

	quotient opBinaryRight(string op, Z)(const Z t) const if ((op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)) {
		quotient r;
		if(op == "*")  r = quotient(num * t, den); else r = quotient(den * t, num);
		if (op == "%") return quotient(r.remainder, r.den);
		r.reduce();
		return r;
	}
	quotient opBinaryRight(string op, Z)(const quotient!Z q) const if ((op == "+" || op == "-") && (is_signed_integral!Z)) {
		return (op == "+") ? quotient(num * q.den + den * q.num, den * q.den) : Quotiernt(den * q.num - num * q.den, den * q.den);
	}

	quotient opBinaryRight(string op, Z)(const quotient!Z q) const if ((op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)) {
		quotient r;
		if(op == "*")  r = quotient(num * q.num, den * q.den); else r = quotient(den * q.num, num * q.den);
		if (op == "%") r.num = r.remainder;
		r.reduce();
		return r;
	}
	T integer() const @property { return num / den;}
	T remainder() const @property { return num % den;}

	quotient magnitude() const @property { return (num < 0) ? quotient(-num, den) : this;}

	bool opEquals(const quotient q) const {
		return (num == q.num) && (den == q.den);
	}

	bool opEquals(Z)(const quotient!Z q) const if(is_signed_integral!Z){
		return (num == q.num) && (den == q.den);
	}

	bool opEquals(const T n) const { return (num == n) && (den == 1);}

	bool opEquals(Z)(const Z n) const if (is_signed_integral!Z) { return (num == n) && (den == 1);}

	int sign() const @property {
		return (num < 0) ? -1 : (num > 0) ? 1 : 0;
	}

	int opCmp(Z)(const Z z) const if(is_integral!Z || is_quotient!Z){
		return (this - z).sign;
	}

	bool opCast(B : bool) () const {
		return num != T(0);
	}

	F opCast(F)() if(is_floating_point!F){
		return cast(F)(num) / cast(F)(den);
	}

	static quotient inv(const T x) { return quotient(one!T, x);}
}

alias  quotient!Integer Quotient;

struct quotient(T) if (is_signed_integral!T && !is(T == Integer)) {
private:
	T num = 0;
	T den = 0;
protected:
	void reduce() {
		if(den == 0) num = numerics.sign!T(num);
		else {
			if (den < 0) {num = -num; den = -den;}
			T g = gcd(num, den);
			if (g > 1) {num /= g; den /= g;}
		}
	}

public:
	T numerator() const @property { return num;}

	T denominator() const @property { return den;}

	string toString() const {return std.format.format("%d / %d", num, den);}

	this(Z)(const Z n) if(is_signed_integral!Z) {
		num = n;
		den = 1;
	}

	this(Z1, Z2)(const Z1 n, const Z2 d) if(is_signed_integral!Z1 && is_signed_integral!Z2) {
		num = n;
		den = d;
		reduce();
	}

	this(Z)(const quotient!Z q) if(is_signed_integral!Z) {
		num = q.num; 
		den = q.den;
	}

	enum : quotient {nan = quotient(), infinity = quotient(one!T, zero!T), max = quotient(T.max, one!T), min = quotient(T.min, one!T)};

	bool isNaN() const @property { return numerics.isZero!T(num) && numerics.isZero!T(den);}
	bool isInfinity() const @property {return numerics.isZero(den) && !numerics.isZero(num);}
	bool isPositiveInfinity() const @property {return numerics.isZero!T(den) && num > zero!T;}
	bool isNegativeInfinity() const @property {return numerics.isZero!T(den) && num < zero!T;}
	bool isZero() const @property {return numerics.isZero!T(num) && !numerics.isZero!T(den);}

	ref quotient opUnary(string op)() if (op == "++" || op == "--") {
		if (op == "++") num += den; else num -= den;
		return this;
	}

	quotient opUnary(string op)() const if (op== "+") {
		return this;
	}

	quotient opUnary(string op)() const if (op == "-") {
		return quotient(-num, den);
	}

	ref quotient opAssign(Z)(quotient!Z q) if(is_signed_integral!Z) {
		num = q.num;
		den = q.den;
		return this;
	}

	ref quotient opAssign(T t) {
		num = t;
		den = 1;
		return this;
	}

	ref quotient opAssign(Z)(Z z) if(is_signed_integral!Z) {
		num = z;
		den = 1;
		return this;
	}

	ref quotient opOpAssign(string op) (const quotient q) if (op == "+" || op =="-") {
		if (op == "+") num = num * q.den + q.num * den; else num = num * q.den - q.num * den;
		den *= q.den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z) (const quotient!Z q) if ((op == "+" || op =="-") && (is_signed_integral!Z)) {
		if (op == "+") num = num * q.den + q.num * den; else num = num * q.den - q.num * den;
		den *= q.den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z) (const quotient!Z q) if((op == "*" || op =="/" || op == "%") && is_signed_integral!Z) {
		T n = num;
		if (op == "*") { num *= q.num; den *= q.den;} else {num *= q.den; den *= q.num;}
		if (op == "%") num = remainder;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op)(const T t) if((op == "+") || (op == "-")) {
		if(op == "+") num += t * den; else num -= t * den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op)(const T t) if((op == "*") || (op == "/") || (op == "%")) {
		if(op == "*") num *= t; else den *= t;
		if(op == "%") num = remainder;	
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z)(const Z t) if(((op == "+") || (op == "-")) && (is_signed_integral!Z )) {
		if(op == "+") num += t * den; else num -= t * den;
		reduce();
		return this;
	}

	ref quotient opOpAssign(string op, Z)(const Z t) if(((op == "*") || (op == "/") || (op == "%")) && (is_signed_integral!Z)) {
		if(op == "*") num *= t; else den *= t;
		if(op == "%") num = remainder;	
		reduce();
		return this;
	}

	quotient opBinary(string op)(const quotient q) const if (op== "+" || op == "-" || op == "*" || op == "/" || op == "%") {
		quotient r = this; return r.opOpAssign!(op)(q);
	}

	quotient opBinary(string op)(const T t) const if (op== "+" || op == "-" || op == "*" || op == "/" || op == "%") {
		quotient r = this; return r.opOpAssign!(op)(t);
	}

	quotient opBinary(string op, Z)(const quotient!Z q) const if ((op== "+" || op == "-" || op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)){
		quotient r = this; return r.opOpAssign!(op, Z)(q);
	}

	quotient opBinary(string op, Z)(const Z t) const if ((op== "+" || op == "-" || op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)) {
		quotient r = this; return r.opOpAssign!(op, Z)(t);
	}

	quotient opBinaryRight(string op, Z)(const Z t) const if ((op == "+" || op == "-") && (is_signed_integral!Z)) {
		quotient r = this;
		return r.opOpAssign!(op, Z)(t);
	}

	quotient opBinaryRight(string op, Z)(const Z t) const if ((op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)) {
		quotient r;
		if(op == "*")  r = quotient(num * t, den); else r = quotient(den * t, num);
		if (op == "%") return r = r.remainder;
		return r;
	}
	quotient opBinaryRight(string op, Z)(const quotient!Z q) const if ((op == "+" || op == "-") && (is_signed_integral!Z)) {
		return (op == "+") ? quotient(num * q.den + den * q.num, den * q.den) : Quotiernt(den * q.num - num * q.den, den * q.den);
	}

	quotient opBinaryRight(string op, Z)(const quotient!Z q) const if ((op == "*" || op == "/" || op == "%") && (is_signed_integral!Z)) {
		quotient r;
		if(op == "*")  r = quotient(num * q.num, den * q.den); else r = quotient(den * q.num, num * q.den);
		if (op == "%") return r.num = r.remainder;
		r.reduce();
		return r;
	}

	T integer() const @property { return num / den;}

	T remainder() const @property { return num % den;}

	quotient magnitude() const @property { return (num < 0) ? quotient(-num, den) : this;}

	bool opEquals(const quotient q) const {
		return (num == q.num) && (den == q.den);
	}

	bool opEquals(Z)(const quotient!Z q) const if(is_signed_integral!Z){
		if (isNan(this) || isNan(q)) return false;
		return (num == q.num) && (den == q.den);
	}

	bool opEquals(const T n) const { return (num == n) && (den == 1);}

	bool opEquals(Z)(const Z n) const if (is_Integral!Z) { return (num == n) && (den == 1);}

	int opCmp()(const quotient q) {
		return (this - q).sign;
	}
	int opComp()(const T n) {
		return (this - n).sign;
	}
	int opCmp(Z)(const quotient!Z q) const if(is_signed_integral!Z) {
		return (this - q).sign;
	}
	int opComp(Z)(const Z n) const if(is_signed_integral!Z) {
		return (this - n).sign;
	}

	bool opCast(B : bool) () const {
		return num;
	}

	F opCast(F)() const if(is_floating_point!F) {
		return to!F(num) / to!F(den);
	}

	static inv(const T x) {return quotient(one!T, x);}
}

T abs(T : quotient!N, N)(const T q) if(is_integer!N) {
	return (q.num < zero!N) ? -q : q;
}

int sign(T : quotient!N, N)(const quotient!N q) {
	return sign!N(q.num);
}		

alias Quot(uint n) = quotient!(Int!n);

auto quot(N, D)(N n, D d) if(is(N == Integer) || is(D == Integer)) {
	return Quotient(n, d);
}

auto quot(N, D)(N n, D d) if(is_signed_integral!N && is_signed_integral!D) {
	return quotient!(CommonType!(N, D))(n, d);
}

mixin Function_to_Real!Quotient;
