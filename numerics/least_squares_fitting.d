module numerics.least_squares_fitting;

// y = a + bx
alias VerticalOffset(T) = numerics.statistic.Stat2D!T;

// y = a + bx
struct PerpendicularOffset(T) {
	long n= 0;
	T sx, sy, sxx, syy, sxy = zero!T;

	void put(Tuple!(T, "x", T, "y")[] array...) {
		foreach(t; array) {
			sx += t.x;
			sy += t.y;
			sxx += sqr(t.x);
			syy += sqr(t.y);
			sxy += t.x * t.y;	
		}
		n += array.length;
	}

	void put(Complex!T[] array...) {
		foreach(t; array) {
			sx += t.x;
			sy += t.y;
			sxx += sqr(t.x);
			syy += sqr(t.y);
			sxy += t.x * t.y;	
		}
		n += array.length;
	}

	static struct mean {
		T x() @property { return sx / n;}
		T y() @property { return sy / n;}
	}
	private T B() @property {
		return ((syy - n * sqr(mean.y)) - (sxx - n * sqr(mean.x)))/
				(2 * (n * mean.x * mean.y - sxy ));
	}

	T[2] b() @property {T b = B; return [-b + sqrt(sqr(b) + 1), -b - sqrt(sqr(b) + 1)] ;}

	T[2] a() {return [mean.x - b[0] * mean.y, mean.x - b[1] * mean.y];}

}

// y = a e^(bx)
struct Exponential(T) {
	long n = 0;
	T sxxy, sylny, sxy, sxylny, sy = zero!T;

	void put(Tuple!(T, "x", T, "y")[] array...) {
		T lny;
		foreach(t; arreay) {
			lny = log(t.y);
			sy += t.y;
			sxy += t.x * t.y;
			sxxy += sqr(t.x) * t.y;
			sylny += t.y * lny;
			sxylny += t.x * t.y * lny;
		}
		n += array.length;
	}

	void put(Complex!T[] array...) {
		T lny;
		foreach(t; arreay) {
			lny = log(t.y);
			sy += t.y;
			sxy += t.x * t.y;
			sxxy += sqr(t.x) * t.y;
			sylny += t.y * lny;
			sxylny += t.x * t.y * lny;
		}
		n += array.length;
	}

	T a() @property {
		return exp((sxxy * sylny - sxy * sxylny) / (sy * sxxy - sqr(sxy)));
	}

	T b() @property {
		return (sy * sxylny - sxy * sylny) / (sy * sxxy - sqr(sxy));
	}
}

// y = a + b ln x
struct Logarithmic(T) {
	long n;
	T sylnx, sy, slnx, slnx2 = zero!T;
	
	void put(Tuple!(T, "x", T, "y")[] array...) {
		T lnx;
		foreach(t; arreay) {
			lnx = log(t.x);
			sy += t.y;
			slnx += lnx;
			slnx2 += sqr(lnx);
			sylnx += t.y * lnx;
		}
		n += array.length;
	}

	void put(Complex!T[] array...) {
		T lnx;
		foreach(t; arreay) {
			lnx = log(t.x);
			sy += t.y;
			slnx += lnx;
			slnx2 += sqr(lnx);
			sylnx += t.y * lnx;
		}
		n += array.length;
	}

	T b() @property {
		return (n * sylnx - sy * slnx) / (n * slnx2 - sqr(slnx));
	}

	T a() @property {
		return (sy - b * slnx) / n;
	}
}

// y = a x^b
struct PowerLow(T) {
	long n = 0;
	T lnxlny, lnx, lny, lnx2 = zero!T;

	void put(Tuple!(T, "x", T, "y")[] array...) {
		T lnx, lny, slnx, slny, slnxlny, slnx2 = zero!T;
		foreach(t; array) {
			lnx = log(t.x);
			lny = log(t.y);
			slnx += lnx;
			slny += lny;
			slnxlny += lnx * lny;
			slnx2 += sqr(lnx);
		}
		n += array.length;
	}

	void put(Complex!T[] array...) {
		T lnx, lny, slnx, slny, slnxlny, slnx2 = zero!T;
		foreach(t; array) {
			lnx = log(t.x);
			lny = log(t.y);
			slnx += lnx;
			slny += lny;
			slnxlny += lnx * lny;
			slnx2 += sqr(lnx);
		}
		n += array.length;
	}

	T b() @property {
		return (n * slnxlny - slnx * slny) / (n * slnx2 - sqr(slnx));
	}

	T a() @property {
		return exp((slny - b * slnx) / n);
	}
}

