module numerics.statistic;

struct Stat1D(T) {
	ulong n = 0;

	T sx, sxx = zero!T;

	void put(T[] list...) {
		foreach(x; list) {
			sx += x;
			sxx += sqr(x);
		}
		n += list.length;
	}

	T mean() @property { return sx / n;}

	T ssXX() @property { return sxx - n * sqr(meanX);}

	T variance() @property { return ssXX / n;}
	
}

struct Stat2D(T) {
	ulong n = 0;

	T sx, sy, sxx, syy, sxy = zero!T;

	void put(Tuple!(T, "x", T, "y")[] array...) {
		foreach(t; array) {
			sx += t.x;
			sxx += sqr(t.x);
			sxy += t.x * t.y;
			syy += sqr(t.y);
			sy += t.y;
		}
		n += array.length;
	}

	void put(Complex!T[] array...) {
		foreach(t; array) {
			sx += t.x;
			sxx += sqr(t.x);
			sxy += t.x * t.y;
			syy += sqr(t.y);
			sy += t.y;
		}
		n += array.length;
	}

	static struct mean {
		T x() @property { return sx / n;}
		T y() @property { return sy / n;}
	}

	T ssXX() @property { return sxx - n * sqr(mean.x);} // Σ(x - mx)²

	T ssYY() @property { return syy - n * sqr(mean.x);} // Σ(y - my)²

	T ssXY() @property { return sxy - n * mean.x * mean.y;} // Σ(x -mx)(y - my)

	static struct n {
		static struct variance {
			T x() @property { return ssXX / n;}
			T y() @property { return ssYY / n;}
		}
		T covariance() @property { return  ssXY / n;}
		static struct standardDeviation {
			T x() @property { return sqrt(variance.x);}
			T y() @property { return sqrt(variance.y);}
		}
	}

	static struct n_1 {
		static struct variance {
			T x() @property { return ssXX / (n - 1);}
			T y() @property { return ssYY / (n - 1);}
		}
		T covariance() @property { return  ssXY / (n - 1);}
		static struct standardDeviation {
			T x() @property { return sqrt(variance.x);}
			T y() @property { return sqrt(variance.y);}
		}
	}

	T b() @property { return ssXY / ssXX;}

	T a() @property { return mean.y - b * mean.x;}

	// statistical squared correlation
	T r2() @property { return sqr(ssxy) / ssXX / ssYY;}

	// statistical correlation
	T r() @property { return sqrt(r2);} 
	// Yi ≡ a + b mx
	// ei ≡ y -Yi
	// s² = Σ(ei / (n - 2))
	T s() @property { return sqrt((ssyy - b * ssxy) / (n - 2));}
	// standard error for a and b
	static struct StdError {
		T a() @property { return s * sqrt(sqr(mean.x)/ ssXX + 1.0/n);}
		T b() @property { return s / sqrt(ssXX);}
	}
	// sum squared error
	T ssE() @property { return sqr(b) * ssXX;}
	// sum squared residual
	T ssR() @property { return ssYY * (1 - r2);}
}

