module numerics.dual;
import numerics;
import meta;
import std.traits : isInstanceOf;
import std.typecons : Tuple, tuple;

private {
	import meta : InitialType;
}


struct Dual(T) if(is_floating_point!T || is_complex!T) {

	T re;
	T du;

	this(T r) {
		re = r;
		du = zero!T;
	}

	this(T r, T d) {
		re = r;
		du = d;
	}

	this(N)(in N e) if(is_integral!N) {
		re = cast(T)e;
		du = 0.0;
	}

	static Dual polar(T a, T m) { return Dual!T(a, a * m);}

	T opCast(T) { return re;}

	Dual opAssign(in T x)  {
		re = x;
		du = 0.0;
		return this;
	}

	Dual opBinary(string op)(in T x) const if(op == "+" || op == "-" || op == "*" || op == "/" || op == "^^") { 
		static if(op == "+") return Dual!T(re.x + x, du);
		static if(op == "-") return Dual!T(re.x - x, du);
		static if(op == "*") return Dual!T(re * x, du * x);
		static if(op == "/") return Dual!T(re / x, -du * x / (x * x));
		static if(op == "^^") return Dual(re ^^ x, du * x * re ^^ (x-1));
	}

	Dual opBinaryRight(string op)(in T x) const if(op == "+" || op == "-" || op == "*" || op == "/") { 
		static if(op == "+") return Dual!(T)(x + re, du);
		static if(op == "-") return Dual!(T)(x - re, -du);
		static if(op == "*") return Dual!(T)(x * re, x * du);
		static if(op == "/") return Dual!(T)(x / re, x * du / (re * re));
	}

	Dual opUnary(string op)() const if (op == "-") { return Dual!V(-re, -du);}

	Dual opBinary(string op)(in Dual x) const if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") return Dual!V(re + x.re, du + x.du);
		static if(op == "-") return Dual!V(re - x.re, du - x.du);
		static if(op == "*") return Dual!V(re * x.re, re * x.du + du * x.re);
		static if(op == "/") return Dual!V(re / x.re, (re * x.du - du * x.re) / (x.re * x.re));
	}
	ref Dual opOpAssign(string op)(in Dual x) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") { re += x.re; du += x.du;}
		static if(op == "-") { re -= x.re; du -= x.du;}
		static if(op == "*") { re *= x.re; du = re * x.du + du * x.re;}
		static if(op == "/") { re /= x.re; du = (du * x.re - re * x.du) / (x.re * x.re);}
		return this;
	}

	ref Dual opOpAssign(string op)(in T x) if(op == "+" || op == "-" || op == "*" || op == "/") {
		static if(op == "+") { re += x;}
		static if(op == "-") { re -= x;}
		static if(op == "*") { re *= x; du *= x;}
		static if(op == "/") { re /= x; du /= x;}
		return this;
	}

	Dual conj(const Dual x) { return Dual!T(x.re, - x.du);}

	int opCmp(in double s) const { return (re > s) - (re < s);}

	int opEquals(const Dual x) const {return re == x.re && du == x.du;}

	Dual opBinary(string op : "^^")(T k) const {
		T temp = pow(re, k - 1);
		return Dual!(T)(re * temp, du * k * temp);
	}

	Dual opBinary(string op : "^^")(const Dual x) {
		T temp = pow(re, x.re);
		return Dual!V(temp, (du * x.re + log(re) * x.du) * temp);
	}

	Tuple!(T, T) polar() @property { return tuple(re, du / re);}

	T arg() @property { return du / re;}

	Dual ε() const @property {return Dual(0, re);}
}

U exp(U : Dual!T, T)(const Dual!T x) { return U(exp(x.re), x.du * exp(x.re));}

U log(U : Dual!T, T)(const Dual!T x) {return U(log(x.re), x.du / x.re);}

U inv(U : Dual!T, T)(const Dual!T x) { return U(1/ x.re, -x.du / x.re / x.re);}

U sqrt(U : Dual!T, T)(const Dual!T x) {T temp = std.math.sqrt(x.re); return U(temp, x.du / 2 / temp);}

U abs(U : Dual!T, T)(const T x) if(is(T == Dual!U, U)) {return U(std.math.abs(x.re), du * x.re / std.math.abs(x.re));}

U sign(U : Dual!T, T)(const Dual!T x) { return U(std.math.sgn(x.re), du * (1 - x.re / x.re));}

U conj(U : Dual!T, T)(const Dual!T x) { return U(x.conj, du.nan);}

U sqr(U : Dual!T, T)(const Dual!T x) {return U(x.re * x.re, 2 * du * x.re);}

T arg(U : Dual!T, T)(const Dual!T x) {return x.du / x.re;}

U sin(U : Dual!T, T)(const U x) { return U(sin(x.re), x.du * cos(x.re));}

U cos(U : Dual!T, T)(const Dual!T x) {  return U(cos(x.re), -x.du * sin(x.re));}

U tan(U : Dual!T, T)(const Dual!T x) { return U(tan(x.re), x.du / sqr(cos(x.re)));}

U cot(U : Dual!T, T)(const Dual!T x) { return U(cot(x.re), -x.du / sqr(sin(x.re)));}

U asin(U : Dual!T, T)(const Dual!T x) { return U(asin(x.re), x.du / sqrt(1 - x.re * x.re));}

U acos(U : Dual!T, T)(const Dual!T x) {  return U(acos(x.re), -x.du / sqrt(1 - x.re * x.re));}

U atan(U : Dual!T, T)(const Dual!T x) { return U(atan(x.re), x.du / (1 + x.re * x.re));}

U acot(U : Dual!T, T)(const Dual!T x) { return U(acot(x.re), -x.du / (1 + sqr(x.re)));}

U sinh(U : Dual!T, T)(const Dual!T x) { return U(sinh(x.re), x.du * cosh(x.re));}

U cosh(U : Dual!T, T)(const Dual!T x) {  return U(cosh(x.re), x.du * sinh(x.re));}

U tanh(U : Dual!T, T)(const Dual!T x) { return U(tanh(x.re), x.du / 2 * (log(1 + x.re) - log(1 - x.re)));}

U coth(U : Dual!T, T)(const Dual!T x) { return U(coth(x.re), x.du / sqr(sinh(x.re)));}

U asinh(U : Dual!T, T)(const Dual!T x) { return U(asinh(x.re), x.du / sqrt(1 + x.re * x.re));}

U acosh(U : Dual!T, T)(const Dual!T x) {  return U(acosh(x.re), x.du / sqrt(x.re - 1) / sqrt(x.re + 1));}

U atanh(U : Dual!T, T)(const Dual!T x) { return U(atanh(x.re), x.du / (1 - x.re * x.re));}

U acoth(U : Dual!T, T)(const Dual!T x) { return U(acoth(x.re), x.du / (1 - x.re * x.re));}

alias dfloat = Dual!float;
alias ddouble = Dual!double;
alias dreal = Dual!real;

mixin Functions!dfloat;
mixin Functions!ddouble;
mixin Functions!dreal;

