module collections.set;

import std.typecons : Tuple, tuple;
import numerics : isIn, isNotIn;

struct Set(V) { // Set of items of type V

	protected V[] array;

	this(const Set s) {
		array = s.array();
	}

	this(V[] arg...){
		foreach(v; arg) if (v.isNotIn(array)) array ~= v;
	}

enum : Set { empty = Set() }

	ref Set opAssign()(const Set s) {
		array = s.array.dup;
		return this;
	}

	bool opBinaryRight(string op : "in")(const V v) const {
		return v.isIn(array);
	}

	ref Set opOpAssign(string op)(const V v) if (op == "+" || op == "|") {
		if (v.isIn(array)) return this;
		array ~= v;
		return this;
	}

	ref Set opOpAssign(string op)(const Set s) if (op == "+" || op == "|") {
		foreach(x; s.array) if (x.isNotIn(array)) array ~= x;
		return this;
	}

	Set opBinary(string op)(const V v) const if (op == "+" || op == "|") {
		Set result = this;
		result += v;
		return result;
	}

	Set opBinaryRight(string op)(const V v) const if (op == "+" || op == "|") {
		Set result = this;
		result += v;
		return result;
	}

	Set opBinary(string op)(const Set s) const if (op == "+" || op == "|") {
		Set result = this;
		result += s;
		return result;
	}

	Set opBinary(string op : "&")(const Set s) const {
		Set result;
		foreach(x; array) if(x.isIn(s.array)) result += x;
		return result;
	}

	ref Set opOpAssign(string op : "&")(const Set s) {
		return this(this & s);
	}

	Set opBinary(string op : "^")(const Set s) const {
		Set result;
		foreach(x; array) if (x.isNotIn(s.array)) result += x;
		foreach(x; s.array) if(x.isNotIn(array)) result += x;
		return result;
	}

	ref opOpAssign(string op : "^")(const Set s) {
		return this = this ^ s;
	}

	Set opBinary(string op : "-")(const V v) const {
		foreach(i, a; array) if (a == v) return Set(array[0 .. i - 1] ~ array[i + 1 .. $]);
		return this;
	}

	Set opBinary(string op : "-")(const Set s) const {
		Set r;
		foreach(x; array) if(x.isNot(s.array)) r += x;
		return r;
	}

	ref opOpAssign(string op : "-")(const V v) const {
		foreach(i, a; array) if (a == v) { array = array[0 .. i - 1] ~ array[i + 1 .. $]; return this;}
		return this;
	}

	ref Set opOpAssign(string op : "-")(const Set s) {
		return this = this - s;
	}

	Set!(Tuple!(V, U)) opBinary(U, string op : "*")(const Set!U s) const {
		Set!(Tuple!(V, U)) r;
		foreach(x; array) foreach(y; s.array) r += tuple(x, y);
		return r;
	}

	Set!(Tuple!(V, U)) opBinary(U, string op : "*")(const U u) const {
		Set!(Tuple!(U, V)) result;
		foreach(a; array) result += tuple(a, u);
		return result;
	}

	Set!(Tuple!(U, V)) opBinaryRight(U, string op : "*")(const U u) const {
		Set!(Tuple!(V, U)) result;
		foreach(a; array) result += tuple(u, a);
		return result;
	}

	bool isEmpty() const { return !array.length;}

	bool opBinary(string op : "in")(const Set s) const {
		foreach(v; array) if(v.isNotIn(s.array)) return false;
		return true;
	}

	bool opEquals(const Set s) const {
		if (array.length != s.array.length) return false;
		return this in s;
	}

	T[] asArray() const { return array.dup;}

	U opCast(U : T[])() const {
		return asArray;
	}

	struct Range {
		ref Set set;
		size_t index;
		this(ref Set s) { set = s; index = 0;}
		V front() const 
			in {
				assert(frontIndex <= infs.max);
			}
		body {
			return array[index];
		}
		void popFront() const         
			in {
				assert(frontIndex <= infs.max);
			}
		body {
			++index;
		}
		bool isEmpty() const {
			return index >= s.array.length;
		}
	}

	auto range() const {
		return Range(this);
	}

}

Set!(Tuple!(T, T)) sqr(T)(const Set!T s) { return s * s; }

auto pow(T, uint n : 0)(const Set!T s) {
	return Set!T.empty;
}

auto pow(T, uint n : 1)(const Set!T s) {
	return s;
}

auto pow(T, uint n : 2)(const Set!T s) {
	return sqr!T(s);
}

auto pow(T, uint n)(const Set!T s) if(n % 2) {
	return s * sqr!T(pow!(T, n/2)(s));
}

auto pow(T, uint n)(const Set!T s) if(!(n % 2)) {
	return sqr!T(pow!(T, n/2)(s));
}

size_t Card(T)(const Set!T s) {return s.length; }

Set!(Set!T) power(T)(const Set!T s) {
	Set!(Set!T) ret;
	foreach(e; s.array) {
		Set!(Set!T) rs;
		foreach(x; ret.array) {
			x += e;
			rs += x;
		}
		ret += rs;
	}
	return ret;
}

