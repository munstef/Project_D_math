module collections.queue;

struct Queue(T) {
private:

	T[] array;

public:

	this(this) { array = array.dup;}
	this(const Queue Q) { array = Q.array.dup;}
	this(const T[] arg...) { array = arg.dup;}
	size_t length() const @property { return array.length;}
	bool isEmpty() const { return !array.length;}
	T head() const @property 
		in { assert(!empty);}
	body {
		return array[0];
	}
	void enqueue(T value) {
		array ~= value;
	}	

	T dequeue() 
	in {assert(!empty);}
	body {
		T result = array[0];
		array = array[1..$];
		return result;
	}
	T[] asArray() const @property {
		return array.dup;
	}
}
