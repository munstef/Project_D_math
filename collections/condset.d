module collections.condset;
import meta;

struct Set(T) {

    const pure nothrow bool delegate(in T) contains;

    bool opBinaryRight(string op : "in")(in T x) const pure nothrow {
        return contains(x);
    }

    Set opBinary(string op)(in Set set) const pure nothrow if (op == "+" || op == "|") {
		return Set(x => contains(x) || set.contains(x));
	}

	Set opBinary(string op : "-")(in Set set) const pure nothrow {
		return Set(x => contains(x) && !set.contains(x));
	}

	Set opBinary(string op : "&")(in Set set) const pure nothrow {
		return Set(x => contains(x) && set.contains(x));
	}

	Set opBinary(string op : "^")(in Set set) const pure nothrow {
		return Set(x => contains(x) != set.contains(x));
	}

	Set opUnary(string op)() const pure nothrow if(op == "!" || op == "-" || op == "~") {
		return Set(x => !contains(x));
	}

	/*	** to do **
	Set!(Tuple!(T, U)) opbinary(string op : "*")(const Set!(U) u) const {

	} 
	*/

}
/*
Set!(Tuple!(T, T)) sqr(const Set!(T) s) {
return Set(All!(s.contains)(x));
}

Set!(Repeat!(n, T)) pow(uint n)(const Set!T s) if(n > 0) {
return Set(x => All!(s.contains)(x));
}
*/