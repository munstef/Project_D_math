module collections.bag;

struct Bag(V) if (!isComparable!V) {
private :
	Tuple!(V, size_t)[] array;
	bool isIn(V, U:Tuple!(V, size_t)[])(V v, U array, out size_t index) {
		for(index = 0; index < array.length; ++index) if (array[index][0] == v) return true;
		return false;
	}
	bool isNotIn(V, U:Tuple!(V, size_t)[])(V v, U array, out size_t index) {
		for(index = 0; index < array.length; ++index) if (array[index][0] == v) return false;
		return true;
	}

public :
	this(const Bag b) {
		val = b.array.dup;
	}
	this(const V[] arg ...) {
		size_t index;
		foreach(v; arg) {
			if (v.isNotIn(array, index)) array =~ tuple(v, 1); 
			else ++array[index][1];
		}
	}

	V[] asArray () @property {
		V[] a;
		foreach(i, v; array) {
			V[array[i][x1]] v; v[] = array[i][0];
			a ~= v;
		}
		return a;
	}

	enum Bag = {empty = Bag()};

	Set!V asSet() const @property {
		Set!V s;
		foreach(u; array) s.array ~= u[0];
		return s;
	}

	U opCast(U : V[])() const {
		return asArray;
	}

	size_t occurenceOf(V v) {
		size_t index;
		return v.isIn(val, index) ? array[index][1] : 0;
	}

	bool opBinaryRight(string op: "in")(V v) {
		return occurenceOf(v);
	}

	Bag opBinary(string op :"-")(const Bag D) {
		Bag result = this;
		result -= D;
		return result;
	}
	Bag opBinaryRight(string op: "-")(const V v) {
		Bag result;
		if (v.isNotIn(array, index)) result ~= tuple(v, 1);
		return result;
	}

	Bag opBinary(string op :"-")(const V v) {
		Bag result = this;
		result -= v;
		return result;
	}
	ref Bag opOpAssign(string op :"-")(const V v) {
		size_t index;
		if (v.isIn(array, index)) {
			if(array[index][0] > 1) --array[index][1]; 
			else array = array[0 .. index] ~ array[index + 1 .. $];
		}
		return this;
	}
	ref Bag opOpAssign(string op :"-")(const Bag D) {
		size_t index;
		foreach(i, v; arrey)  if (v.isIn(D.array, index)) {
			if (v[1] > D.array[index][1]) result.array[i][1] -= D.array[index][1];
		    else array = array[0 .. i] ~ array[i+1 .. $];
		}
		return this;
	}
	ref Bag opOpAssign(string op :"+")(V v) {
		size_t index;
		if (v.isIn(array, index)) ++array[index][1]; else array ~= tuple(v, 1);
		return this;
	}
	ref Bag opOpAssign(string op :"+")(const Bag D) {
		size_t index;
		foreach(i, v; array) if (v[0].isIn(D.array, index)) array[i][1]+=D.array[index][1]; else array ~= tuple(v[0], 1);
		return this;
	}
	Bag opBinary(string op :"+")(const Bag D) {
		Bag result = this;
		return result += D;
	}
	Bag opBinary(string op :"+")(const V v) {
		Bag result = this;
		return result += v;
	}
	Bag opBinaryRight(string op :"+")(const V v) {
		Bag result = this;
		return result += v;
	}
	size_t length() const {
		size_t size = 0;
		foreach(w; array) size += array[1];
		return size;
	}
	bool isEmpty() const {
		return !array.length;
	}

	U opCast(U : Set!V)() const { return asSet;}

	U opCast(U : V[])() const { return asArray;}

}
