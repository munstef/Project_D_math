module collections.stack;

struct Stack(T) {
private:

	T[] array;

public:

	this(this) { array = array.dup;}

	this(const Stack S) { array = S.array.dup;}

	this(const T[] arg...) { array = arg.dup;}

	size_t length() const @property { return array.length;}

	bool isEmpty() const { return !array.length;}

	T top() const @property 
		in { assert(length);}
	body {
		return array[$-1];
	}

	T pop()
	in { assert(length);}
	body {
		r = array[$-1];
		--array.length;
		return r;
	}

	void push(T value) { array ~=value; }

	T[] asArray() @property const {
		return array.dup.reverse;
	}
}

