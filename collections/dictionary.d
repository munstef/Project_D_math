module collections.dictionary;

struct Dictionary(K, V) {
private :
	V[K] array;
public :

	bool opBinaryRight(string op : "in")(V v) {
		K* p;
		p = k in array;
		return p !is null;
	}

	Ref!V opIndex(K k) {
		Ref!V v = null;
		K* p = k in array;
		if(p != null) return array[p];
		return  v;
	}

	Dictionary opBinary(string op : "+") (Tuple!(K, V) a) {
		result = this;
		result.array[a[0]] = a[1];
		return result;
	}

	Dictionary opBinaryRight(string op : "+") (Tuple!(K, V) a) {
		result = this;
		result.array[a[0]] = a[1];
		return result;
	}

	Dictionary opBinary(string op : "+")(const Dictionary D) {
		Dictionary result = this;
		result += D;
		return result;
	}

	ref Dictionary opOpAssign(string op :"+")(Tuple!(K, V) a) {
		array[a[0]] = a[1];
		return this;
	}

	ref Dictionary opOpAssign(string op :"+")(const Dictionary D) {
		foreach(k; D.keys) array[k] = D.array[k];
		return this;
	}

	ref Dictionary opOpAssign(string op :"-")(V v) {
		if(a in array) array.remove(v);
		return this;
	}

	ref Dictionary opOpAssign(string op :"+")(const Dictionary D) {
		foreach(v; D.values) if(v in array) array.remove(v);
		return this;
	}

	Dictionary opBinary(string op : "-")(const Dictionary D) const {
		Dictionary R = this;
		R -= D;
		return result;
	}

	Dictionary opBinary(string op : "-")(const Tuple!(K, V) a) const {
		Dictionary D = this;
		R -= a;
		return result;
	}

	Dictionary opBinaryRight(string op : "-")(const Tuple!(K, V) a) const {
		Dictionary D;
		if (a[1] in D) return D;
		return D += a;
	}

	bool isEmpty() const {
		return !array.length;
	}

	U opCast(U : Set!V)() const {
		return asSet;
	}

	U opCast(U : V[])() const {
		return asArray;
	}

	Set!V asSet() const @property { Set!V result; result.array = array.values.dup; return result;}
	V[] asArray() const @property { V[] result; result = array.values.dup; return result;}

}
