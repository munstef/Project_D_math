module collections.array;
import iz.containers : Array;

template Array(T) {
	alias Array(T) = iz.containers.Array!T;
}
